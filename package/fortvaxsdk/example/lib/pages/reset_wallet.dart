import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:sdk_example/constant.dart';
import 'package:sdk_example/json_rpc.dart';

class ResetWallet extends StatefulWidget {
  const ResetWallet({Key? key}) : super(key: key);

  @override
  State<ResetWallet> createState() => _ResetWalletState();
}

class _ResetWalletState extends State<ResetWallet> {
  final _sdkPlugin = FortVaxSdk();

  String description = "";

  void callResetWallet() async {
    try {
      final rpc = JSONRPCRequest("reset_wallet", "1", {"caPublicKey": caKey});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);

      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          setState(() {
            description = "重置完成，請重新創建錢包或恢復錢包";
          });
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "重置失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    } finally {
      closeNFC();
    }
  }

  void closeNFC() async {
    final rpc = JSONRPCRequest("stop_scan", "1", {"alertMessage": ""});
    final request = {"jsonRpcData": jsonEncode(rpc)};
    await _sdkPlugin.callJsonRpc(request);
  }

  @override
  void initState() {
    super.initState();

    _sdkPlugin.onListenStreamData((data) {
      String event = data as String;
      if (event == "walletExecuteEvent") {
        setState(() {
          description = "資料處理中，請勿移動卡片";
        });
      }
    }, (error) {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(15, 35, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back))),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                    child: const Text('開始讀取'),
                    onPressed: () {
                      setState(() {
                        description = "請觸碰卡片";
                      });
                      callResetWallet();
                    },
                  ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        description,
                        style: const TextStyle(fontSize: 26),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
