import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:sdk_example/constant.dart';
import 'package:sdk_example/json_model.dart';
import 'package:sdk_example/json_rpc.dart';

class CreateWallet extends StatefulWidget {
  const CreateWallet({super.key});

  @override
  State<CreateWallet> createState() => _CreateWalletState();
}

class _CreateWalletState extends State<CreateWallet> {
  final _sdkPlugin = FortVaxSdk();

  String description = "";

  String _uid = "";

  int _status = 0;

  void callCreateWallet() async {
    try {
      final rpc = JSONRPCRequest("create_wallet", "1", {"caPublicKey": caKey, "pinCode": "asd12558", "wordCount": 12, "createPassword": _status == 0, "cardId": _uid});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          if (jsonResult.result != null) {
            String seedWords = jsonResult.result!["seedWords"];
            var jsonPublicKeys = jsonResult.result!["accountModelKeys"] as List;
            var pks = jsonPublicKeys.map((e) => EcPublicKey.fromJson(e));

            var buffer = StringBuffer();
            buffer.write(seedWords);
            buffer.write("\n\n");

            for (var pk in pks) {
              buffer.write("${pk.type} , ${pk.keyToAddress}\n");
            }

            setState(() {
              description = buffer.toString();
            });
          } else {
            setState(() {
              description = "創建失敗 (無 result)";
            });
          }
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "創建失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    } finally {
      closeNFC();
    }
  }

  void closeNFC() async {
    final rpc = JSONRPCRequest("stop_scan", "1", {"alertMessage": ""});
    final request = {"jsonRpcData": jsonEncode(rpc)};
    await _sdkPlugin.callJsonRpc(request);
  }

  void readCardStatus() async {
    try {
      final rpc = JSONRPCRequest("read_card_status", "1", {"caPublicKey": caKey});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          if (jsonResult.result != null && jsonResult.result!["uid"] != null) {
            _uid = jsonResult.result!["uid"];
            _status = jsonResult.result!["cardStatus"];
            print(_uid);
            print(_status);
            callCreateWallet();
          } else {
            description = "讀取卡片 id 失敗 (無 result)";
          }
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "讀取卡片 id 失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _sdkPlugin.onListenStreamData((data) {
      String event = data as String;
      if (event == "walletExecuteEvent") {
        setState(() {
          description = "資料處理中，請勿移動卡片";
        });
      }
    }, (error) {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(15, 35, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back))),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                    child: const Text('開始讀取'),
                    onPressed: () {
                      setState(() {
                        description = "請觸碰卡片";
                      });
                      readCardStatus();
                    },
                  ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        description,
                        style: const TextStyle(fontSize: 26),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
