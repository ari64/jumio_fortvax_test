import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:sdk_example/constant.dart';
import 'package:sdk_example/json_model.dart';
import 'package:sdk_example/json_rpc.dart';

class RestoreWallet extends StatefulWidget {
  const RestoreWallet({super.key});

  @override
  State<RestoreWallet> createState() => _RestoreWalletState();
}

class _RestoreWalletState extends State<RestoreWallet> {
  final _sdkPlugin = FortVaxSdk();

  String description = "";

  String _uid = "";

  int _status = 0;

  final String _words = "report prosper burst slush surround hat urban predict local law mystery umbrella";

  void callRestoreWallet() async {
    try {
      final rpc = JSONRPCRequest("restore_wallet", "1", {"caPublicKey": caKey, "pinCode": "asd12558", "words": _words, "createPassword": _status == 0, "cardId": _uid});
      print(rpc.params);
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          if (jsonResult.result != null) {
            var jsonPublicKeys = jsonResult.result!["accountModelKeys"] as List;
            var pks = jsonPublicKeys.map((e) => EcPublicKey.fromJson(e));

            var buffer = StringBuffer();

            for (var pk in pks) {
              buffer.write("${pk.type} , ${pk.keyToAddress}\n");
            }

            setState(() {
              description = buffer.toString();
            });
          } else {
            setState(() {
              description = "恢復失敗 (無 result)";
            });
          }
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "恢復失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    } finally {
      closeNFC();
    }
  }

  void closeNFC() async {
    final rpc = JSONRPCRequest("stop_scan", "1", {"alertMessage": ""});
    final request = {"jsonRpcData": jsonEncode(rpc)};
    await _sdkPlugin.callJsonRpc(request);
  }

  void readCardStatus() async {
    try {
      final rpc = JSONRPCRequest("read_card_status", "1", {"caPublicKey": caKey});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          if (jsonResult.result != null && jsonResult.result!["uid"] != null) {
            _uid = jsonResult.result!["uid"];
            _status = jsonResult.result!["cardStatus"];
            print(_uid);
            print(_status);
            callRestoreWallet();
          } else {
            description = "讀取卡片 id 失敗 (無 result)";
          }
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "讀取卡片 id 失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _sdkPlugin.onListenStreamData((data) {
      String event = data as String;
      if (event == "walletExecuteEvent") {
        setState(() {
          description = "資料處理中，請勿移動卡片";
        });
      }
    }, (error) {});
  }

  @override
  void dispose() {
    super.dispose();
    _sdkPlugin.cancelEventSink();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(15, 35, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back))),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                    child: const Text('開始讀取'),
                    onPressed: () {
                      setState(() {
                        description = "請觸碰卡片";
                      });
                      readCardStatus();
                    },
                  ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                      child: Text(
                        description,
                        style: const TextStyle(fontSize: 26),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
