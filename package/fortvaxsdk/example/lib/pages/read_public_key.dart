import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:sdk_example/constant.dart';
import 'package:sdk_example/json_model.dart';
import 'package:sdk_example/json_rpc.dart';

class ReadPublicKey extends StatefulWidget {
  const ReadPublicKey({Key? key}) : super(key: key);

  @override
  State<ReadPublicKey> createState() => _ReadPublicKeyState();
}

class _ReadPublicKeyState extends State<ReadPublicKey> {
  final _sdkPlugin = FortVaxSdk();

  String description = "";

  String _uid = "";

  @override
  void initState() {
    super.initState();
    _sdkPlugin.onListenStreamData((data) {
      String event = data as String;
      if (event == "walletExecuteEvent") {
        setState(() {
          description = "資料處理中，請勿移動卡片";
        });
      }
    }, (error) {});
  }

  void callReadPublicKey() async {
    //account , utxo 一起讀
    try {
      final rpc = JSONRPCRequest("read_account_publicKey", "1", {
        "caPublicKey": caKey,
        "pinCode": "asd12558",
        "cardId": _uid,
        "supportedCryptoMainNet": [1, 3]
      });

      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
        return;
      }

      var jsonrpcResult = jsonrpcResultFromJson(result);
      if (jsonrpcResult.result == null && jsonrpcResult.result?["accountModelKeys"] == null) {
        setState(() {
          description = "SDK Error (accountModelKeys ? no result)";
        });
        return;
      }

      var jsonPublicKeys = jsonrpcResult.result!["accountModelKeys"] as List;
      var ecKeys = jsonPublicKeys.map((e) => EcPublicKey.fromJson(e));

      var buffer = StringBuffer();

      for (var key in ecKeys) {
        buffer.write("${key.type} , ${key.keyToAddress}\n");
      }

      String? addDogeResult = await addDogeMainChain();

      if (addDogeResult == null) {
        setState(() {
          description = "SDK Error (新增 doge 失敗)";
        });
        return;
      }

      var dogeMainChain = jsonrpcResultFromJson(addDogeResult);
      if (dogeMainChain.result == null || dogeMainChain.result!["blockchainPlus"] == null) {
        setState(() {
          description = "SDK Error (無法取得 Doge 的錢包資訊)";
        });
        return;
      }

      //用 console 驗證
      print(dogeMainChain.result);

      String? utxoBtcResult = await readUtxoKey(2);
      String? utxoLtcResult = await readUtxoKey(6);
      String? utxoDogeResult = await readUtxoKey(12);

      print(utxoDogeResult);

      if (utxoBtcResult == null || utxoLtcResult == null || utxoDogeResult == null) {
        setState(() {
          description = "SDK Error (read UTXO)";
        });
        return;
      }

      setState(() {
        description = "${buffer.toString()}\n\n$utxoBtcResult\n\n$utxoLtcResult\n\n$utxoDogeResult";
      });
    } on PlatformException catch (e) {
      setState(() {
        description = "讀取 public key fail ${e.code} , ${e.message} , ${e.details}";
      });
    } finally {
      closeNFC();
    }
  }

  Future<String?> addDogeMainChain() async {
    print("addDogeMainChain");
    final rpc = JSONRPCRequest("new_blockchain", "1", {
      "caPublicKey": caKey,
      "pinCode": "asd12558",
      "cardId": _uid,
      "types": [12]
    });

    final request = {"jsonRpcData": jsonEncode(rpc)};
    return await _sdkPlugin.callJsonRpc(request);
  }

  Future<String?> readUtxoKey(int type) async {
    print("read utxo $type");
    final utxoRpc = JSONRPCRequest("read_utxo_publicKey", "1", {"caPublicKey": caKey, "pinCode": "asd12558", "cardId": _uid, "type": type});

    final utxoRequest = {"jsonRpcData": jsonEncode(utxoRpc)};
    return await _sdkPlugin.callJsonRpc(utxoRequest);
  }

  void closeNFC() async {
    final rpc = JSONRPCRequest("stop_scan", "1", {"alertMessage": ""});
    final request = {"jsonRpcData": jsonEncode(rpc)};
    await _sdkPlugin.callJsonRpc(request);
  }

  void readCardStatus() async {
    try {
      final rpc = JSONRPCRequest("read_card_status", "1", {"caPublicKey": caKey});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      if (result == null) {
        setState(() {
          description = "SDK Error";
        });
      } else {
        JsonrpcResult jsonResult = jsonrpcResultFromJson(result);
        if (jsonResult.error != null) {
          setState(() {
            description = "SDK RPC Error -> ${jsonResult.error}";
          });
        } else {
          if (jsonResult.result != null && jsonResult.result!["uid"] != null) {
            _uid = jsonResult.result!["uid"];
            callReadPublicKey();
          } else {
            description = "讀取卡片 id 失敗 (無 result)";
          }
        }
      }
    } on PlatformException catch (e) {
      setState(() {
        description = "讀取卡片 id 失敗 ${e.code} , ${e.message} , ${e.details}";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(15, 35, 15, 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(Icons.arrow_back))),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      flex: 0,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                        child: const Text('開始讀取'),
                        onPressed: () {
                          setState(() {
                            description = "請觸碰卡片";
                          });
                          readCardStatus();
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Text(
                              description,
                              style: const TextStyle(fontSize: 26),
                            ),
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
