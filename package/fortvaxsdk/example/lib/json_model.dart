import 'dart:convert';

EcPublicKey ecPublicKeyFromJson(String str) => EcPublicKey.fromJson(json.decode(str));

String ecPublicKeyToJson(EcPublicKey data) => json.encode(data.toJson());

class EcPublicKey {
  int type;
  String compressedKey;
  String uncompressedKey;
  String keyToAddress;
  bool change;
  int addressIndex;

  EcPublicKey({
    required this.type,
    required this.compressedKey,
    required this.uncompressedKey,
    required this.keyToAddress,
    required this.change,
    required this.addressIndex,
  });

  factory EcPublicKey.fromJson(Map<String, dynamic> json) => EcPublicKey(
        type: json["type"],
        compressedKey: json["compressedKey"],
        uncompressedKey: json["uncompressedKey"],
        keyToAddress: json["keyToAddress"],
        change: json["change"],
        addressIndex: json["addressIndex"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "compressedKey": compressedKey,
        "uncompressedKey": uncompressedKey,
        "keyToAddress": keyToAddress,
        "change": change,
        "addressIndex": addressIndex,
      };
}
