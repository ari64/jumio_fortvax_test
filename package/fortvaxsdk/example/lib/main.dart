import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:sdk_example/pages/create_wallet.dart';
import 'package:sdk_example/pages/read_public_key.dart';
import 'package:sdk_example/pages/reset_wallet.dart';
import 'package:sdk_example/pages/restore_wallet.dart';

import 'json_rpc.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/home',
    routes: {
      '/home': (context) => const Home(),
      '/reset': (context) => const ResetWallet(),
      '/create': (context) => const CreateWallet(),
      '/restore': (context) => const RestoreWallet(),
      '/readPublicKey': (context) => const ReadPublicKey()
    },
  ));
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _sdkPlugin = FortVaxSdk();

  @override
  void initState() {
    super.initState();
    final rpc = JSONRPCRequest("set_scan_alert_message", "1", {"feedback": "OOOO"});
    final request = {"jsonRpcData": jsonEncode(rpc)};
    // initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      final rpc = JSONRPCRequest("set_scan_alert_message", "1", {"feedback": "OOOO"});
      final request = {"jsonRpcData": jsonEncode(rpc)};
      String? result = await _sdkPlugin.callJsonRpc(request);
      print(result);
    } on PlatformException catch (e) {
      print(e.code);
      print(e.message);
      print(e.details);
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              OutlinedButton(
                style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                child: const Text('測試創建錢包'),
                onPressed: () {
                  Navigator.pushNamed(context, '/create');
                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                child: const Text('測試恢復錢包'),
                onPressed: () {
                  Navigator.pushNamed(context, '/restore');
                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                child: const Text('測試讀取公鑰'),
                onPressed: () {
                  Navigator.pushNamed(context, "/readPublicKey");
                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                child: const Text('測試簽章'),
                onPressed: () {

                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(foregroundColor: Colors.black, side: const BorderSide(width: 2, color: Colors.black87)),
                child: const Text('卡片恢復出廠設定'),
                onPressed: () {
                  Navigator.pushNamed(context, '/reset');
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
