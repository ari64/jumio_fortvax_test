import 'dart:convert';

abstract class JSONRPC {
  final String jsonrpc = "2.0";
  final String id;

  JSONRPC(this.id);
}

class JSONRPCRequest extends JSONRPC {
  final String method;
  final Map<String, dynamic> params;

  JSONRPCRequest(this.method, String id, [this.params = const {}]) : super(id);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'method': method,
        'params': params,
        'id': id,
        'jsonrpc': jsonrpc,
      };

  factory JSONRPCRequest.fromJson(Map<String, dynamic> json) => JSONRPCRequest(
        json['method'] as String,
        json['id'],
        json['params'] as Map<String, dynamic>,
      );
}

JsonrpcResult jsonrpcResultFromJson(String str) => JsonrpcResult.fromJson(json.decode(str));

String jsonrpcResultToJson(JsonrpcResult data) => json.encode(data.toJson());

class JsonrpcResult {
  String jsonrpc;
  Error? error;
  Map<String, dynamic>? result;
  String id;

  JsonrpcResult({
    required this.jsonrpc,
    this.error,
    this.result,
    required this.id,
  });

  factory JsonrpcResult.fromJson(Map<String, dynamic> json) => JsonrpcResult(
        jsonrpc: json["jsonrpc"],
        error: json["error"] != null ? Error.fromJson(json["error"]) : null,
        result: json["result"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "jsonrpc": jsonrpc,
      "id": id,
    };

    if (error != null) {
      map['error'] = error?.toJson();
    }

    if (result != null) {
      map["result"] = result;
    }

    return map;
  }
}

class Error {
  int code;
  String message;

  Error({
    required this.code,
    required this.message,
  });

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        code: json["code"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
      };
}
