import Flutter
import UIKit
import XCTest

@testable import fortvax_sdk

// This demonstrates a simple unit test of the Swift portion of this plugin's implementation.
//
// See https://developer.apple.com/documentation/xctest for more information about using XCTest.

class RunnerTests: XCTestCase {

    func testGetPlatformVersion() {
        let plugin = FortVaxSdkPlugin()

        let call = FlutterMethodCall(methodName: "getPlatformVersion", arguments: [])

        let resultExpectation = expectation(description: "result block must be called.")
        plugin.handle(call) { result in
            XCTAssertEqual(result as! String, "iOS " + UIDevice.current.systemVersion)
            resultExpectation.fulfill()
        }
        waitForExpectations(timeout: 1)
    }

    func testJsonRpcSerialization() {
        let json = """
                    {
                      "jsonrpc": "2.0",
                      "method": "read_card_status",
                      "params": {
                        "caPublicKey": "353637383940"
                      },
                      "id": "1"
                    }
                    """

        if let jsonData = json.data(using: .utf8) {
            do {
                let decoder = JSONDecoder()
                let de = try decoder.decode(FVJsonRpc.self, from: jsonData)
                XCTAssertEqual("read_card_status", de.method)
                XCTAssertEqual("1", de.id)
//                XCTAssertEqual("353637383940", de.params?.caPublicKey)
            } catch {
                XCTFail("解析失敗 \(error)")
            }
        }
    }
}
