import 'package:fortvax_sdk/api/requests/estimate_erc20_gas_request.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:test/test.dart';

main() {
  // Set up service config
  FortVaxServiceConfig.init(
      'C549985CF98C2DEA9EFB9AC76674A4130075981D115B50586B822F6C1C7BD2136A1624E30783ED44AEEB8296F7247196F0D6DE2EC89ACD852B8759BF08825064883615A4655C92D3C4E46C5C477C3EC55072453C16A5BC37A395AF47E20C68E33BADBB2CC1DE2C445784BB6FAFA3D451267DCE24499DECB92DFCA76A8EAADBE9D881B57E4C5848B3571DF3BD26AE475590626FB8DE2F2609C64126B8BF5245D7D50A8403E86EDA3C1BCB8FC54C883CC34E9B35B23198E882C6EE6561B9AC4C89C5261612BDE45BCCA5EF37BF2D28DC8D99EA46EBE12C0924D6A02160227CF789117003B19047FFC2041C1B57BDF7C52350B6F90650E6CAAFF08A74EBB0025A95');
  FortVaxServiceConfig.setWalletId('TDdpBVnjsfLKJGTZeYLfrdJ5sYLLUnuNSv');
  FortVaxServiceConfig.setCardId('46565930303030304a33');

  group('Gas limit requests', () {
    test('test eth gas limit', () async {
      try {
        final ethRequest = FortVaxEstimateErc20GasRequest(
            'ethereum',
            '0xD3D60E2469F8eF355e1478aDc2f909cB5b0d83CD',
            '0xE9D28DE6d4f787d606b0277E049C7b608f4888Dc',
            '0xdAC17F958D2ee523a2206206994597C13D831ec7',
            double.parse('1'),
            6);
        final response = await ethRequest.estimateGasLimit(1000);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test bsc gas limit', () async {
      try {
        final ethRequest = FortVaxEstimateErc20GasRequest(
            'bsc',
            '0x508552F40fb87Dc19317ae4b5D9796D54B2760F6',
            '0xE4eDb277e41dc89aB076a1F049f4a3EfA700bCE8',
            '0x2170Ed0880ac9A755fd29B2688956BD959F933F8',
            double.parse('1'),
            18);
        final response = await ethRequest.estimateGasLimit(1000);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon gas limit', () async {
      try {
        final ethRequest = FortVaxEstimateErc20GasRequest(
            'polygon',
            '0x641fb555527b9108a1f58ea24e0c04ff86c4ed0d',
            '0xade8f7f25c712d076bfcead256d9af269170eb33',
            '0xc2132D05D31c914a87C6611C10748AEb04B58e8F',
            double.parse('1'),
            6);
        final response = await ethRequest.estimateGasLimit(1000);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
  });
}
