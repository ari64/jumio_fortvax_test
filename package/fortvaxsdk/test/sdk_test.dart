import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:fortvax_sdk/sdk.dart';
import 'package:fortvax_sdk/sdk_method_channel.dart';
import 'package:fortvax_sdk/sdk_platform_interface.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'package:pointycastle/export.dart';

class MockSdkPlatform with MockPlatformInterfaceMixin implements FortVaxSdkPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<String?> callJsonRpc(Map<String, dynamic> request) => Future.value("success");

  @override
  StreamSubscription onListenStreamData(onEvent, onError) {
    throw UnimplementedError();
  }
}

void main() {
  final FortVaxSdkPlatform initialPlatform = FortVaxSdkPlatform.instance;

  test('$MethodChannelSdk is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelSdk>());
  });

  test('16進位資料處理 (hex to bytes)', () async {
    final hex = 'ff656c6c6f20576f726c64';
    final bytes = hex.toBytes();
    final compareData = [255, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100];

    bool isEqual = true;
    if (bytes.length != compareData.length) {
      isEqual = false;
    } else {
      for (int i = 0; i < bytes.length; i++) {
        if (bytes[i] != compareData[i]) {
          isEqual = false;
          break;
        }
      }
    }

    expect(true, isEqual);

    final reverse = compareData.bytesToHex();
    expect(true, hex == reverse);
  });

  test('測試 json encode 輸出', () {
    final xsign = FVApiSignature();
    xsign.card = "46563030303030464523";
    xsign.walletId = "TRCJhQw5u4F3wZ1PkY199p3jm2XN4WjKYR";
    print(jsonEncode(xsign));
  });

  test('Flutter RSA 加密', () {
    //to utf8 byte array
    final xsign = FVApiSignature();
    xsign.card = "46563030303030464523";
    xsign.walletId = "TRCJhQw5u4F3wZ1PkY199p3jm2XN4WjKYR";
    xsign.nonce = 1687843513;

    final jsonText = jsonEncode(xsign);
    final rawBytes = utf8.encode(jsonText);
    final List<int> padding = [];
    padding.addAll(rawBytes);
    for (int i = rawBytes.length; i < 256; i++) {
      padding.add(0);
    }

    final rawPk = "c549985cf98c2dea9efb9ac76674a4130075981d115b50586b822f6c1c7bd2136a1624e30783ed44aeeb8296f7247196f0d6de2ec89acd852b8759bf08825064883615a4655c92d3c4e46c5c477c3ec55072453c16a5bc37a395af47e20c68e33badbb2cc1de2c445784bb6fafa3d451267dce24499decb92dfca76a8eaadbe9d881b57e4c5848b3571df3bd26ae475590626fb8de2f2609c64126b8bf5245d7d50a8403e86eda3c1bcb8fc54c883cc34e9b35b23198e882c6ee6561b9ac4c89c5261612bde45bcca5ef37bf2d28dc8d99ea46ebe12c0924d6a02160227cf789117003b19047ffc2041c1b57bdf7c52350b6f90650e6caaff08a74ebb0025a95".toBytes();
    final pk = RSAPublicKey(BigInt.parse(rawPk.bytesToHex(), radix: 16), BigInt.from(65537));
    final rasCipher = RSAEngine()..init(true, PublicKeyParameter<RSAPublicKey>(pk));
    final encrypt = rasCipher.process(Uint8List.fromList(padding));
    expect(encrypt.bytesToHex(), "8655e04648363f9acbcd73d8c757cf2eb736f5ef9bf19c5d9c5557c64be6b3560c7805792f312b077d146dd9b7a9a8406c6435ed7b974d403eefbbf0883d9fa01bc0c42630c724abc85cdc71dc14e540e12a252a68aed5a0917458a50c5cde1c489a1e23f326d74362e861d83a2de53426ea9e107979fb7d802d617fae39eb3f464c5c53735fead62dde5666f5f0b3b691995c37e652638e35b2c08459fb48eb3ff2ac8383e765bddabb085f156b4c88cb86af0b515aaca59933aa009ca487711023cb1624e885b82a578da8fa34e1d41e41659f86a186eefca2638935ae15811042e630ce73a7e8b5fdfff6995ec01c16f207837c8b4c4cfeef22839f692229");
  });
}
