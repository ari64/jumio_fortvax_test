import 'package:fortvax_sdk/api/requests/polygon_request.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:test/test.dart';

main() {
  // Set up service config
  FortVaxServiceConfig.init(
      'C549985CF98C2DEA9EFB9AC76674A4130075981D115B50586B822F6C1C7BD2136A1624E30783ED44AEEB8296F7247196F0D6DE2EC89ACD852B8759BF08825064883615A4655C92D3C4E46C5C477C3EC55072453C16A5BC37A395AF47E20C68E33BADBB2CC1DE2C445784BB6FAFA3D451267DCE24499DECB92DFCA76A8EAADBE9D881B57E4C5848B3571DF3BD26AE475590626FB8DE2F2609C64126B8BF5245D7D50A8403E86EDA3C1BCB8FC54C883CC34E9B35B23198E882C6EE6561B9AC4C89C5261612BDE45BCCA5EF37BF2D28DC8D99EA46EBE12C0924D6A02160227CF789117003B19047FFC2041C1B57BDF7C52350B6F90650E6CAAFF08A74EBB0025A95');
  FortVaxServiceConfig.setWalletId('TDdpBVnjsfLKJGTZeYLfrdJ5sYLLUnuNSv');
  FortVaxServiceConfig.setCardId('46565930303030304a33');
  final polygonRequest = FortVaxPolygonRequest();
  const String account = "0x18Cd6A66FaA8bA6D0E7F8397cA9f3e6eE58F94C8";
  const String contractAddress = "0xc2132D05D31c914a87C6611C10748AEb04B58e8F";

  group('Polygon requests', () {
    test('test polygon asset balance', () async {
      try {
        final response = await polygonRequest.assetBalance(account);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon ERC20 asset balance', () async {
      try {
        final response = await polygonRequest.assetERC20Balance(account, contractAddress);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon contract information', () async {
      try {
        final response = await polygonRequest.contractInformation(contractAddress);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon token transfer', () async {
      try {
        final response = await polygonRequest.tokenTransferRecords(account, contractAddress, 1);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon transaction records', () async {
      try {
        final response = await polygonRequest.transactionRecords(account, 1);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon EIP1559 gas station', () async {
      try {
        final response = await polygonRequest.gasStation1559();
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test polygon pending nonce', () async {
      try {
        final response = await polygonRequest.pendingNonce(account);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
  });
  group('Polygon requests (need card support)', () {
    test('test polygon broadcast', () async {
      try {
        const rawTx = "raw_tx";
        final response = await polygonRequest.broadcast(rawTx, account);
        expect(response, isNotNull);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
  });
}