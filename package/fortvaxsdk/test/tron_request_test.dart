import 'package:fortvax_sdk/api/requests/tron_request.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:test/test.dart';

main() {
  // Set up service config
  FortVaxServiceConfig.init(
      'C549985CF98C2DEA9EFB9AC76674A4130075981D115B50586B822F6C1C7BD2136A1624E30783ED44AEEB8296F7247196F0D6DE2EC89ACD852B8759BF08825064883615A4655C92D3C4E46C5C477C3EC55072453C16A5BC37A395AF47E20C68E33BADBB2CC1DE2C445784BB6FAFA3D451267DCE24499DECB92DFCA76A8EAADBE9D881B57E4C5848B3571DF3BD26AE475590626FB8DE2F2609C64126B8BF5245D7D50A8403E86EDA3C1BCB8FC54C883CC34E9B35B23198E882C6EE6561B9AC4C89C5261612BDE45BCCA5EF37BF2D28DC8D99EA46EBE12C0924D6A02160227CF789117003B19047FFC2041C1B57BDF7C52350B6F90650E6CAAFF08A74EBB0025A95');
  FortVaxServiceConfig.setWalletId('TQMVXnKscYMFMBGRWtuBqEohZAcjKH9nuP');
  FortVaxServiceConfig.setCardId('46565930303030304a33');
  // Set up test properties
  final tronRequest = FortVaxTronRequest();
  const tronErc20Symbol = "USDT";
  const tronAddress = "TPRXjR69bCtMMHXmHR41vh5E78FoDKVu4w";
  const tronContractAddress = "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t";

  group('Tron requests', () {
    test('test tron assetBalance', () async {
      try {
        await tronRequest.assetBalance(tronAddress);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron assetTrc20Balance', () async {
      try {
        await tronRequest.assetTrc20Balance(tronAddress, tronErc20Symbol);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron accountInformation', () async {
      try {
        await tronRequest.accountInformation(tronAddress);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron transactionRecords', () async {
      try {
        await tronRequest.transactionRecords(tronAddress, 1);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron tokenTransactionRecords', () async {
      try {
        await tronRequest.tokenTransactionRecords(
            tronAddress, tronContractAddress, 1);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron currentTronBlock', () async {
      try {
        await tronRequest.currentTronBlock();
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron consumeEnergy', () async {
      try {
        const contractAddress = "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t";
        const toAddress = "THPvaUhoh2Qn2y9THCZML3H815hhFhn5YC";
        const decimal = 6;
        const symbol = "USDT";
        const amount = "5";
        await tronRequest.consumeEnergy(
            contractAddress, toAddress, amount, decimal, symbol);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron contractInfo', () async {
      try {
        await tronRequest.contractInfo(tronContractAddress);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron estimate energy', () async {
      try {
        await tronRequest.estimateEnergy(tronAddress, 5);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
    test('test tron estimate bandwidth', () async {
      try {
        await tronRequest.estimateBandwidth(tronAddress, 5);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
  });
  group('Tron requests (need card support)', () {
    test('test tron broadcast', () async {
      try {
        const rawTx = "raw_tx";
        await tronRequest.boardcast(rawTx, tronAddress);
      } catch (e) {
        fail('catch error: ${e.toString()}');
      }
    });
  });
}
