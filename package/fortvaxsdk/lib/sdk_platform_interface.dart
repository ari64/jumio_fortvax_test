import 'dart:async';

import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'sdk_method_channel.dart';

abstract class FortVaxSdkPlatform extends PlatformInterface {
  /// Constructs a SdkPlatform.
  FortVaxSdkPlatform() : super(token: _token);

  static final Object _token = Object();

  static FortVaxSdkPlatform _instance = MethodChannelSdk();

  /// The default instance of [SdkPlatform] to use.
  ///
  /// Defaults to [MethodChannelSdk].
  static FortVaxSdkPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SdkPlatform] when
  /// they register themselves.
  static set instance(FortVaxSdkPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> callJsonRpc(Map<String, dynamic> request) {
    throw UnimplementedError('runJSONRPCRequest() has not been implemented.');
  }

  StreamSubscription onListenStreamData(onEvent, onError) {
    throw UnimplementedError('onListenStreamData() has not been implemented.');
  }
}
