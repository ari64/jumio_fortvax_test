import 'dart:async';

import 'sdk_platform_interface.dart';

class FortVaxSdk {
  StreamSubscription? _eventSinkSubscription;

  Future<String?> getPlatformVersion() {
    return FortVaxSdkPlatform.instance.getPlatformVersion();
  }

  Future<String?> callJsonRpc(Map<String, dynamic> request) {
    return FortVaxSdkPlatform.instance.callJsonRpc(request);
  }

  void onListenStreamData(onEvent, onError) {
    _eventSinkSubscription = FortVaxSdkPlatform.instance.onListenStreamData(onEvent, onError);
  }

  void cancelEventSink() {
    _eventSinkSubscription?.cancel();
  }
}
