import 'dart:convert';

import 'package:fortvax_sdk/api/configs/config_release.dart';
import 'package:fortvax_sdk/api/fv_xsign_util.dart';
import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:http/http.dart' as http;

enum HttpMethod {
  get,
  post,
}

/// 基礎請求
abstract class FortVaxServiceTarget {
  Map<String, String> header = {
    'content-type': 'application/json; charset=utf-8',
    'Accept': 'application/json'
  };

  //post 會用到
  Map<String, dynamic>?
      postBody; // FIX: 定義成 Map<String, dynamic>?，dynamic 可視為 any，該值默認為 null

  //base url
  String authority() => Config.baseUrl;

  HttpMethod httpMethod();

  String path();

  /// 如果需要登入，可以向 header 添加 token
  // bool needLogin();

  Map<String, String>?
      queryParameters; //  FIX: 定義成 Map<String, String>?，該值默認為 null

  Uri url() {
    return Uri.https(authority(), path(), queryParameters);
  }

  /// 添加 header
  FortVaxServiceTarget addHeader(String key, Object v) {
    header[key] = v.toString();
    return this;
  }

  /// 添加 post body
  FortVaxServiceTarget addBodyParameters(String key, Object v) {
    postBody?[key] = v; // FIX: nullable 後的賦值方式
    return this;
  }
}

class FortVaxServiceConfig {
  static String _apiKey = "";
  static String _cardId = "";
  static String _walletId = "";

  static String get apiKey => _apiKey;

  static String get cardId => _cardId;

  static String get walletId => _walletId;

  static void init(String apiKey) {
    _apiKey = apiKey;
  }

  static void setCardId(String cId) {
    _cardId = cId;
  }

  static void setWalletId(String wId) {
    _walletId = wId;
  }
}

class FortVaxServiceClient {
  int connectTimeout = 15;

  FortVaxServiceClient([this.connectTimeout = 15]);

  Future<FortVaxApiResponse> send(FortVaxServiceTarget target) async {
    final response = await _sendRequest(target, connectTimeout);
    return _responseHandler(response);
  }

  Future<http.Response> _sendRequest(
      FortVaxServiceTarget target, int timeoutSeconds) async {
    target.addHeader(
        "x-signature", FVXSignatureUtil.instance.xsign()); //add x-sign
    final timeout = Duration(seconds: connectTimeout);
    final http.Response response;
    switch (target.httpMethod()) {
      case HttpMethod.get:
        response = await http
            .get(target.url(), headers: target.header)
            .timeout(timeout);
      case HttpMethod.post:
        response = await http
            .post(target.url(),
                headers: target.header, body: jsonEncode(target.postBody))
            .timeout(timeout);
    }
    return response;
  }

  FortVaxApiResponse _responseHandler(http.Response response) {
    final json = jsonDecode(response.body) as Map<String, dynamic>;
    switch (response.statusCode) {
      case >= 200 && < 300:
        if (json.containsKey('meta')) {
          return FortVaxPagingApiResponse.fromJson(json);
        }
        return FortVaxApiResponse.fromJson(json);
      default:
        final message = json["message"] as String? ?? "Something error";
        throw FortVaxApiException(
            statusCode: response.statusCode, message: message);
    }
  }
}
