import 'package:fortvax_sdk/api/models/bsc/bsc_gas_tracker_type_adapter.dart';
import 'package:fortvax_sdk/api/models/evm/all_models.dart';
import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';

import '../targets/bsc_target.dart';
import 'evm_request.dart';

class FortVaxBscRequest extends FortVaxEVMRequest {

  FortVaxBscRequest(): super("bsc");

  Future<FVGasResult> gasStation() async {
    final FortVaxGasStationTarget target = FortVaxGasStationTarget();
    final FortVaxApiResponse response = await client.send(target);
    final FvBscGasTrackerAdapter fvBscGasTrackerAdapter = fvBscGasTrackerAdapterFromJson(response.rawBody);
    final FVGasResultData data = FVGasResultData(fvBscGasTrackerAdapter.slow.toDouble(), fvBscGasTrackerAdapter.standard.toDouble(), fvBscGasTrackerAdapter.fast.toDouble());
    return FVGasResult(data);
  }

  Future<int> pendingNonce(String address) async {
    final FortVaxPendingNonceTarget target = FortVaxPendingNonceTarget(address);
    final FortVaxApiResponse response = await client.send(target);
    return fvEvmNonceFromJson(response.rawBody).nonce;
  }

  Future<String> broadcast(String rawTx, String address) async {
    final FortVaxBroadcastTarget target = FortVaxBroadcastTarget(rawTx, address);
    final FortVaxApiResponse response = await client.send(target);
    final FvSendRawTxResult data = fvFvSendRawTxResultFromJson(response.rawBody);
    return data.result;
  }
}