
import 'dart:convert';

import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';

import '../targets/evm_target.dart';
import 'evm_request.dart';

class FortVaxEstimateErc20GasRequest extends FortVaxEVMRequest {

  final String _blockchain;
  final String fromAddress;
  final String toAddress;
  final String contractAddress;
  final double transferAmount;
  final int decimal;

  FortVaxEstimateErc20GasRequest(this._blockchain, this.fromAddress, this.toAddress, this.contractAddress, this.transferAmount, this.decimal): super(_blockchain);

  Future<int> estimateGasLimit(double suggestedGas) async {
    final FortVaxEstimateGasLimitTarget target = FortVaxEstimateGasLimitTarget(blockchain: _blockchain, fromAddress: fromAddress, toAddress: toAddress, contractAddress: contractAddress, transferAmount: transferAmount, decimal: decimal, suggestedGas: suggestedGas);
    final FortVaxApiResponse response = await client.send(target);
    return int.parse(jsonDecode(response.rawBody)["estimate"]) + 10000;
  }
}