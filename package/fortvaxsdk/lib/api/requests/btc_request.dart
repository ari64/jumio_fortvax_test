import 'dart:convert';

import 'package:fortvax_sdk/api/models/btc/all_models.dart';
import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:fortvax_sdk/api/targets/btc_target.dart';

class FortVaxBtcRequest {
  final client = FortVaxServiceClient();

  Future<List<FVBtcUtxo>> assetBalance(List<String> addresses) async {
    final request = FortVaxBtcAssetBalanceTarget(addresses);
    final response = await client.send(request);
    return fvBtcUtxoFromJson(response.rawBody);
  }

  Future<FVPaginationData<FVBtcTxRecord>> transactionRecords(
      List<String> addresses, int page) async {
    final request = FortVaxBtcTransactionRecordsTarget(addresses, page);
    final response = await client.send(request) as FortVaxPagingApiResponse;
    final list = fvBtcTxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<String> broadcast(String rawTx) async {
    final request = FortVaxBtcBroadcastTarget(rawTx);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["result"];
  }

  Future<FVBtcNetworkStatus> networkStatus() async {
    final request = FortVaxBtcNetworkStatusTarget();
    final response = await client.send(request);
    return fvBtcNetworkStatusFromJson(response.rawBody);
  }
}
