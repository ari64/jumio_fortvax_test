import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:fortvax_sdk/api/targets/general_target.dart';
import 'package:fortvax_sdk/sdk.dart';

class FortVaxGeneralRequest {
  final client = FortVaxServiceClient();

  Future<List<FVPrice>> exchangeTicker() async {
    final request = FortVaxExchangeTickerTarget();
    final response = await client.send(request);
    return fvPriceListFromJson(response.rawBody);
  }
}