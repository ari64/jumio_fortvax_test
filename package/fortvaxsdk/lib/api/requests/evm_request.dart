import 'dart:core';

import '../models/evm/all_models.dart';
import '../models/fortvax/all_models.dart';
import '../sdk_api_interface.dart';
import '../targets/evm_target.dart';

abstract class FortVaxEVMRequest {

  final String blockchain;
  final FortVaxServiceClient client = FortVaxServiceClient();

  FortVaxEVMRequest(this.blockchain);

  Future<BigInt> assetBalance(String account) async {
    final FortVaxBalanceTarget target = FortVaxBalanceTarget(blockchain, account);
    final FortVaxApiResponse response = await client.send(target);
    return fvDeltaBalanceFromJson(response.rawBody).firstOrNull?.value ?? BigInt.zero;
  }

  Future<BigInt> assetERC20Balance(String account, String contractAddress) async {
    final FortVaxERC20BalanceTarget target = FortVaxERC20BalanceTarget(blockchain, account, contractAddress);
    final FortVaxApiResponse response = await client.send(target);
    return fvDeltaBalanceFromJson(response.rawBody).firstOrNull?.value ?? BigInt.zero;
  }

  Future<FVContractInfo> contractInformation(String contractAddress) async {
    final FortVaxContractInfoTarget target = FortVaxContractInfoTarget(blockchain, contractAddress);
    final FortVaxApiResponse response = await client.send(target);
    return fvContractInfoFromJson(response.rawBody);
  }

  Future<FVPaginationData<FVWeb3TxRecord>> tokenTransferRecords(String account, String contractAddress, int page) async {
    final FortVaxERC20TransactionRecordTarget target = FortVaxERC20TransactionRecordTarget(blockchain, account, contractAddress, page);
    final FortVaxPagingApiResponse response = await client.send(target) as FortVaxPagingApiResponse;
    final list = fvWeb3TxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<FVPaginationData<FVWeb3TxRecord>> transactionRecords(String account, int page) async {
    final FortVaxTransactionRecordTarget target = FortVaxTransactionRecordTarget(blockchain, account, page);
    final FortVaxPagingApiResponse response = await client.send(target) as FortVaxPagingApiResponse;
    final list = fvWeb3TxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }
}