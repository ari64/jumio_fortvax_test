import 'dart:convert';

import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:fortvax_sdk/api/models/ltc/all_models.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:fortvax_sdk/api/targets/ltc_target.dart';

class FortVaxLtcRequest {
  final client = FortVaxServiceClient();

  Future<List<FVLtcUtxo>> assetBalance(List<String> addresses) async {
    final request = FortVaxLtcAssetBalanceTarget(addresses);
    final response = await client.send(request);
    return fvLtcUtxoFromJson(response.rawBody);
  }

  Future<FVPaginationData<FVLtcTxRecord>> transactionRecords(
      List<String> addresses, int page) async {
    final request = FortVaxLtcTransactionRecordsTarget(addresses, page);
    final response = await client.send(request) as FortVaxPagingApiResponse;
    final list = fvLtcTxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<String> broadcast(String rawTx) async {
    final request = FortVaxLtcBroadcastTarget(rawTx);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["result"];
  }

  Future<FVLtcNetworkStatus> networkStatus() async {
    final request = FortVaxLtcNetworkStatusTarget();
    final response = await client.send(request);
    return fvLtcNetworkStatusFromJson(response.rawBody);
  }
}
