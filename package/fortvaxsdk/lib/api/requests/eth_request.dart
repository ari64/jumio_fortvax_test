import 'package:fortvax_sdk/api/models/evm/all_models.dart';

import '../models/eth/all_models.dart';
import '../models/fortvax/all_models.dart';
import '../targets/eth_target.dart';
import 'evm_request.dart';

class FortVaxEthRequest extends FortVaxEVMRequest {

  FortVaxEthRequest(): super("ethereum");

  Future<FVEIP1559GasResult> gasStation1559() async {
    final FortVaxEIP1559GasStationTarget target = FortVaxEIP1559GasStationTarget();
    final FortVaxApiResponse response = await client.send(target);
    final FvEthEIP1559FeeStation fvEthSuggestedFeeStation = fvEthEIP1559FeeStationFromJson(response.rawBody);
    final FVEIP1559GasOption low = FVEIP1559GasOption(fvEthSuggestedFeeStation.low.suggestedMaxPriorityFeePerGas, fvEthSuggestedFeeStation.low.suggestedMaxFeePerGas);
    final FVEIP1559GasOption medium = FVEIP1559GasOption(fvEthSuggestedFeeStation.medium.suggestedMaxPriorityFeePerGas, fvEthSuggestedFeeStation.medium.suggestedMaxFeePerGas);
    final FVEIP1559GasOption high = FVEIP1559GasOption(fvEthSuggestedFeeStation.high.suggestedMaxPriorityFeePerGas, fvEthSuggestedFeeStation.high.suggestedMaxFeePerGas);
    final FVEIP1559GasResultData data = FVEIP1559GasResultData(low, medium, high, fvEthSuggestedFeeStation.estimatedBaseFee);
    return FVEIP1559GasResult(data);
  }

  Future<int> pendingNonce(String address) async {
    final FortVaxPendingNonceTarget target = FortVaxPendingNonceTarget(address);
    final FortVaxApiResponse response = await client.send(target);
    return fvEvmNonceFromJson(response.rawBody).nonce;
  }

  Future<String> broadcast(String rawTx, String address) async {
    final FortVaxBroadcastTarget target = FortVaxBroadcastTarget(rawTx, address);
    final FortVaxApiResponse response = await client.send(target);
    final FvSendRawTxResult data = fvFvSendRawTxResultFromJson(response.rawBody);
    return data.result;
  }
}