import 'dart:convert';

import 'package:fortvax_sdk/api/models/btc/all_models.dart';
import 'package:fortvax_sdk/api/models/doge/all_models.dart';
import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:fortvax_sdk/api/targets/doge_target.dart';

class FortVaxDogeRequest {
  final client = FortVaxServiceClient();

  Future<List<FVBtcUtxo>> assetBalance(List<String> addresses) async {
    final request = FortVaxDogeAssetBalanceTarget(addresses);
    final response = await client.send(request);
    return fvBtcUtxoFromJson(response.rawBody);
  }

  Future<FVPaginationData<FVDogeTxRecord>> transactionRecords(
    List<String> addresses,
    int page,
  ) async {
    final request = FortVaxDogeTransactionRecordsTarget(addresses, page);
    final response = await client.send(request) as FortVaxPagingApiResponse;
    final list = fvDogeTxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<String> broadcast(String rawTx) async {
    final request = FortVaxDogeBroadcastTarget(rawTx);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["result"];
  }
}
