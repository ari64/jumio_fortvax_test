import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:fortvax_sdk/api/models/evm/all_models.dart';
import 'package:fortvax_sdk/api/models/fortvax/all_models.dart';
import 'package:fortvax_sdk/api/models/tron/all_models.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:fortvax_sdk/api/targets/tron_target.dart';
import 'package:fortvax_sdk/api/utils/tronkit/trc20.dart';

class FortVaxTronRequest {
  final client = FortVaxServiceClient();

  Future<double> assetBalance(String address) async {
    final request = FortVaxTronAssetBalanceTarget(address);
    final response = await client.send(request);
    final assets = fvAssetFromJson(response.rawBody);
    final balance = _getTronAssetBalance(assets, 'trx');
    return balance;
  }

  Future<double> assetTrc20Balance(String address, String symbol) async {
    final request = FortVaxTronAssetBalanceTarget(address);
    final response = await client.send(request);
    final assets = fvAssetFromJson(response.rawBody);
    final balance = _getTronAssetBalance(assets, symbol);
    return balance;
  }

  Future<FVAccount> accountInformation(String address) async {
    final request = FortVaxTronAccountInformationTarget(address);
    final response = await client.send(request);
    return fvAccountFromJson(response.rawBody);
  }

  Future<FVBlock> currentTronBlock() async {
    final request = FortVaxTronCurrentBlockTarget();
    final response = await client.send(request);
    return fvBlockFromJson(response.rawBody);
  }

  Future<FVPaginationData<FVTronTxRecord>> transactionRecords(
      String address, int page) async {
    final request = FortVaxTronTransactionsRecordTarget(address, page);
    final response = await client.send(request) as FortVaxPagingApiResponse;
    final list = fvTronTxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<FVPaginationData<FVTronTxRecord>> tokenTransactionRecords(
      String address, String contractAddress, int page) async {
    final request = FortVaxTronTokenTransactionRecordsTarget(
        address, contractAddress, page);
    final response = await client.send(request) as FortVaxPagingApiResponse;
    final list = fvTronTxRecordFromJson(response.rawBody);
    return FVPaginationData(list: list, meta: response.meta);
  }

  Future<String> boardcast(String rawTx, String fromAddress) async {
    final request = FortVaxTronBroadcastTarget(rawTx, fromAddress);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["result"];
  }

  Future<double> estimateEnergy(String address, int amount) async {
    final request = FortVaxTronEstimateObtainEnergyTarget(address, amount);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["estimate"];
  }

  Future<double> estimateBandwidth(String address, int amount) async {
    final request = FortVaxTronEstimateObtainBandwidthTarget(address, amount);
    final response = await client.send(request);
    return jsonDecode(response.rawBody)["estimate"];
  }

  Future<FVConsumeEnergy> consumeEnergy(
      String contractAddress, String toAddress, String sendAmount, int tokenDecimal, String symbol) async {
    final trc20 = FVTRC20(
        contractAddress: contractAddress, decimal: tokenDecimal, symbol: symbol);
    final parameter = trc20.generateDataParameters(toAddress, sendAmount);
    final request = FortVaxTronConsumeEnergyTarget(contractAddress, parameter);
    final response = await client.send(request);
    return fvConsumeEnergyFromJson(response.rawBody);
  }

  Future<FVContractInfo> contractInfo(String contractAddress) async {
    final request = FortVaxTronContractInfoTarget(contractAddress);
    final response = await client.send(request);
    return fvContractInfoFromJson(response.rawBody);
  }

  // private methods
  double _getTronAssetBalance(List<FVAsset> assets, String symbol) {
    final asset =
        assets.firstWhereOrNull((element) => element.symbol == symbol);
    if (asset == null) {
      return 0;
    }
    return double.parse(asset.balance);
  }
}
