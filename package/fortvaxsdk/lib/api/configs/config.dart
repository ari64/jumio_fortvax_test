import 'package:flutter/foundation.dart' show kReleaseMode;
import 'package:fortvax_sdk/api/configs/config_debug.dart' as debug;
import 'package:fortvax_sdk/api/configs/config_release.dart' as release;

class Config {
  static const String baseUrl =
      kReleaseMode ? release.Config.baseUrl : debug.Config.baseUrl;
}
