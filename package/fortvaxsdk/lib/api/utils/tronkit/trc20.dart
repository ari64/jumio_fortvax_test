import 'package:bs58check/bs58check.dart' as bs58check;
import 'package:decimal/decimal.dart';
import 'package:fortvax_sdk/extension/decimal.dart';
import 'package:fortvax_sdk/extension/strings.dart';

class FVTRC20 {
  final String contractAddress;
  final int decimal;
  final String symbol;

  FVTRC20({
    required this.contractAddress,
    required this.decimal,
    required this.symbol,
  });

  int get _lengthOf256bits {
    return 256 ~/ 4;
  }

  String generateDataParameters(String toAddress, String amount) {
    final _value = Decimal.parse(amount).divideTen(decimal);
    final poweredAmount = _power("$_value");
    final data = _contractFunctionData(toAddress, poweredAmount);
    return data.bytesToHex();
  }

  List<int> _contractFunctionData(String address, BigInt amount) {
    final base58Decode = bs58check.decode(address);
    final rawAddress = base58Decode.getRange(1, 21).toList();
    final lengthOf256Address = _pad(rawAddress.bytesToHex());
    final amountStr = _pad(amount.toRadixString(16));
    return (lengthOf256Address + amountStr).toBytes();
  }

  String _pad(String string) {
    var str = string;
    while (str.length != _lengthOf256bits) {
      str = "0$str";
    }
    return str;
  }

  BigInt _power(String amount) {
    final components = amount.split('.');
    if ((components.isEmpty) || (components.length > 2)) {
      throw Exception('contract amount invalid: ${components.length})');
    }
    final integer = BigInt.parse(components[0]);
    final poweredInteger = integer * (BigInt.from(10).pow(decimal));
    if (components.length == 2) {
      final count = components[1].length;
      if (count > decimal) {
        throw Exception('contract amount invalid');
      }
      final digit = BigInt.parse(components[1]);
      final poweredDigit = digit * (BigInt.from(10).pow(decimal - count));
      return poweredInteger + poweredDigit;
    } else {
      return poweredInteger;
    }
  }
}
