import 'dart:convert';

FvEthEIP1559FeeStation fvEthEIP1559FeeStationFromJson(String str) => FvEthEIP1559FeeStation.fromJson(json.decode(str));

String fvEthEIP1559FeeStationToJson(FvEthEIP1559FeeStation data) => json.encode(data.toJson());

class FvEthEIP1559FeeStation {
  final FVEthSuggestedFeeStationData low;
  final FVEthSuggestedFeeStationData medium;
  final FVEthSuggestedFeeStationData high;
  final double estimatedBaseFee;
  final double networkCongestion;
  final List<String> latestPriorityFeeRange;
  final List<String> historicalPriorityFeeRange;
  final List<String> historicalBaseFeeRange;
  final String priorityFeeTrend;
  final String baseFeeTrend;

  FvEthEIP1559FeeStation({
    required this.low,
    required this.medium,
    required this.high,
    required this.estimatedBaseFee,
    required this.networkCongestion,
    required this.latestPriorityFeeRange,
    required this.historicalPriorityFeeRange,
    required this.historicalBaseFeeRange,
    required this.priorityFeeTrend,
    required this.baseFeeTrend,
  });

  factory FvEthEIP1559FeeStation.fromJson(Map<String, dynamic> json) => FvEthEIP1559FeeStation(
    low: FVEthSuggestedFeeStationData.fromJson(json["low"]),
    medium: FVEthSuggestedFeeStationData.fromJson(json["medium"]),
    high: FVEthSuggestedFeeStationData.fromJson(json["high"]),
    estimatedBaseFee: double.parse(json["estimatedBaseFee"]),
    networkCongestion: json["networkCongestion"]?.toDouble(),
    latestPriorityFeeRange: List<String>.from(json["latestPriorityFeeRange"].map((x) => x)),
    historicalPriorityFeeRange: List<String>.from(json["historicalPriorityFeeRange"].map((x) => x)),
    historicalBaseFeeRange: List<String>.from(json["historicalBaseFeeRange"].map((x) => x)),
    priorityFeeTrend: json["priorityFeeTrend"],
    baseFeeTrend: json["baseFeeTrend"],
  );

  Map<String, dynamic> toJson() => {
    "low": low.toJson(),
    "medium": medium.toJson(),
    "high": high.toJson(),
    "estimatedBaseFee": estimatedBaseFee,
    "networkCongestion": networkCongestion,
    "latestPriorityFeeRange": List<dynamic>.from(latestPriorityFeeRange.map((x) => x)),
    "historicalPriorityFeeRange": List<dynamic>.from(historicalPriorityFeeRange.map((x) => x)),
    "historicalBaseFeeRange": List<dynamic>.from(historicalBaseFeeRange.map((x) => x)),
    "priorityFeeTrend": priorityFeeTrend,
    "baseFeeTrend": baseFeeTrend,
  };
}

class FVEthSuggestedFeeStationData {
  final double suggestedMaxPriorityFeePerGas;
  final double suggestedMaxFeePerGas;
  final int minWaitTimeEstimate;
  final int maxWaitTimeEstimate;

  FVEthSuggestedFeeStationData({
    required this.suggestedMaxPriorityFeePerGas,
    required this.suggestedMaxFeePerGas,
    required this.minWaitTimeEstimate,
    required this.maxWaitTimeEstimate,
  });

  factory FVEthSuggestedFeeStationData.fromJson(Map<String, dynamic> json) => FVEthSuggestedFeeStationData(
    suggestedMaxPriorityFeePerGas: double.parse(json["suggestedMaxPriorityFeePerGas"]),
    suggestedMaxFeePerGas: double.parse(json["suggestedMaxFeePerGas"]),
    minWaitTimeEstimate: json["minWaitTimeEstimate"],
    maxWaitTimeEstimate: json["maxWaitTimeEstimate"],
  );

  Map<String, dynamic> toJson() => {
    "suggestedMaxPriorityFeePerGas": suggestedMaxPriorityFeePerGas,
    "suggestedMaxFeePerGas": suggestedMaxFeePerGas,
    "minWaitTimeEstimate": minWaitTimeEstimate,
    "maxWaitTimeEstimate": maxWaitTimeEstimate,
  };
}