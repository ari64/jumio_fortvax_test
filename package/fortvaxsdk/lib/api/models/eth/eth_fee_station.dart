import 'dart:convert';

FvEthFeeStation fvEthFeeStationFromJson(String str) => FvEthFeeStation.fromJson(json.decode(str));

String fvEthFeeStationToJson(FvEthFeeStation data) => json.encode(data.toJson());

class FvEthFeeStation {
  final int fast;
  final int safeLow;
  final int average;

  FvEthFeeStation({
    required this.fast,
    required this.safeLow,
    required this.average,
  });

  factory FvEthFeeStation.fromJson(Map<String, dynamic> json) => FvEthFeeStation(
    fast: json["fast"],
    safeLow: json["safeLow"],
    average: json["average"],
  );

  Map<String, dynamic> toJson() => {
    "fast": fast,
    "safeLow": safeLow,
    "average": average,
  };
}