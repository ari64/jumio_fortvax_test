import 'dart:convert';

FvPolygonEIP1559FeeStation fvPolygonEIP1559FeeStationFromJson(String str) => FvPolygonEIP1559FeeStation.fromJson(json.decode(str));

String fvPolygonEIP1559FeeStationToJson(FvPolygonEIP1559FeeStation data) => json.encode(data.toJson());

class FvPolygonEIP1559FeeStation {
  final FVPolygonGasTrackerOption fast;
  final FVPolygonGasTrackerOption safeLow;
  final FVPolygonGasTrackerOption standard;
  final double estimatedBaseFee;

  FvPolygonEIP1559FeeStation({
    required this.fast,
    required this.safeLow,
    required this.standard,
    required this.estimatedBaseFee,
  });

  factory FvPolygonEIP1559FeeStation.fromJson(Map<String, dynamic> json) => FvPolygonEIP1559FeeStation(
    fast: FVPolygonGasTrackerOption.fromJson(json["fast"]),
    safeLow: FVPolygonGasTrackerOption.fromJson(json["safeLow"]),
    standard: FVPolygonGasTrackerOption.fromJson(json["standard"]),
    estimatedBaseFee: json["estimatedBaseFee"]
  );

  Map<String, dynamic> toJson() => {
    "fast": fast.toJson(),
    "safeLow": safeLow.toJson(),
    "standard": standard.toJson(),
    "estimatedBaseFee": estimatedBaseFee
  };
}

class FVPolygonGasTrackerOption {
  final double maxPriorityFee;
  final double maxFee;

  FVPolygonGasTrackerOption({
    required this.maxPriorityFee,
    required this.maxFee,
  });

  factory FVPolygonGasTrackerOption.fromJson(Map<String, dynamic> json) => FVPolygonGasTrackerOption(
    maxPriorityFee: double.parse(json["maxPriorityFee"]),
    maxFee: double.parse(json["maxFee"]),
  );

  Map<String, dynamic> toJson() => {
    "maxPriorityFee": maxPriorityFee,
    "maxFee": maxFee,
  };
}