class FVEIP1559GasResult {
  final FVEIP1559GasResultData data;
  FVEIP1559GasResult(this.data);
}

class FVEIP1559GasResultData {
  final FVEIP1559GasOption low;
  final FVEIP1559GasOption medium;
  final FVEIP1559GasOption high;
  final double estimatedBaseFee;
  FVEIP1559GasResultData(this.low, this.medium, this.high, this.estimatedBaseFee);
}

class FVEIP1559GasOption {
  final double maxPriorityFee;
  final double maxFee;
  FVEIP1559GasOption(this.maxPriorityFee, this.maxFee);
}