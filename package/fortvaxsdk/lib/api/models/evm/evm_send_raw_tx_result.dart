import 'dart:convert';

FvSendRawTxResult fvFvSendRawTxResultFromJson(String str) => FvSendRawTxResult.fromJson(json.decode(str));

String fvFvSendRawTxResultToJson(FvSendRawTxResult data) => json.encode(data.toJson());

class FvSendRawTxResult {
  final String result;

  FvSendRawTxResult({
    required this.result,
  });

  factory FvSendRawTxResult.fromJson(Map<String, dynamic> json) => FvSendRawTxResult(
    result: json["result"],
  );

  Map<String, dynamic> toJson() => {
    "result": result,
  };
}