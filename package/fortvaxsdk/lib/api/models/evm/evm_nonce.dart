import 'dart:convert';

FVEvmNonce fvEvmNonceFromJson(String str) => FVEvmNonce.fromJson(json.decode(str));

String fvEvmNonceToJson(FVEvmNonce data) => json.encode(data.toJson());

class FVEvmNonce {
  final int nonce;

  FVEvmNonce({required this.nonce});

  factory FVEvmNonce.fromJson(Map<String, dynamic> json) => FVEvmNonce(
    nonce: int.parse(json["nonce"]),
  );

  Map<String, dynamic> toJson() => {
    "nonce": nonce,
  };
}