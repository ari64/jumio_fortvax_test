import 'dart:convert';

List<FVDeltaBalance> fvDeltaBalanceFromJson(String str) => List<FVDeltaBalance>.from(json.decode(str).map((x) => FVDeltaBalance.fromJson(x)));

String fvDeltaBalanceToJson(List<FVDeltaBalance> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVDeltaBalance {
  final String token;
  final BigInt value;

  FVDeltaBalance({
    required this.token,
    required this.value,
  });

  factory FVDeltaBalance.fromJson(Map<String, dynamic> json) => FVDeltaBalance(
    token: json["token"],
    value: BigInt.parse("0x${json["value"]}"),
  );

  Map<String, dynamic> toJson() => {
    "token": token,
    "value": value,
  };
}