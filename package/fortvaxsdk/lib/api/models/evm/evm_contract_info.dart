import 'dart:convert';

FVContractInfo fvContractInfoFromJson(String str) => FVContractInfo.fromJson(json.decode(str));

String fvContractInfoToJson(FVContractInfo data) => json.encode(data.toJson());

class FVContractInfo {
  final String contract_address;
  final String token_name;
  final String token_type;
  final String symbol;
  final int? decimal;

  FVContractInfo({
    required this.contract_address,
    required this.token_name,
    required this.token_type,
    required this.symbol,
    required this.decimal
  });

  factory FVContractInfo.fromJson(Map<String, dynamic> json) => FVContractInfo(
    contract_address: json["contract_address"],
    token_name: json["token_name"],
    token_type: json["token_type"],
    symbol: json["symbol"],
    decimal: json["decimal"],
  );

  Map<String, dynamic> toJson() => {
    "contract_address": contract_address,
    "token_name": token_name,
    "token_type": token_type,
    "symbol": symbol,
    "decimal": decimal,
  };
}