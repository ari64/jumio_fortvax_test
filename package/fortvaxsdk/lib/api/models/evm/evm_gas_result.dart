class FVGasResult {
  final FVGasResultData data;
  FVGasResult(this.data);
}

class FVGasResultData {
  final double low;
  final double medium;
  final double high;
  FVGasResultData(this.low, this.medium, this.high);
}