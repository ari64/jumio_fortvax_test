import 'dart:convert';

List<FVWeb3TxRecord> fvWeb3TxRecordFromJson(String str) => List<FVWeb3TxRecord>.from(json.decode(str).map((x) => FVWeb3TxRecord.fromJson(x)));

String fvWeb3TxRecordToJson(List<FVWeb3TxRecord> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVWeb3TxRecord {
  final int timestamp;
  final String hash;
  final String from;
  final String to;
  final String value;
  final int confirmations;
  final String status;
  final int? gas;
  final int? gasPrice;

  FVWeb3TxRecord({
    required this.timestamp,
    required this.hash,
    required this.from,
    required this.to,
    required this.value,
    required this.confirmations,
    required this.status,
    required this.gas,
    required this.gasPrice
  });

  factory FVWeb3TxRecord.fromJson(Map<String, dynamic> json) => FVWeb3TxRecord(
    timestamp: json["timestamp"],
    hash: json["hash"],
    from: json["from"],
    to: json["to"],
    value: json["value"],
    confirmations: json["confirmations"],
    status: json["status"],
    gas: json["gas_used"],
    gasPrice: json["gas_price"]
  );

  Map<String, dynamic> toJson() => {
    "timestamp": timestamp,
    "hash": hash,
    "from": from,
    "to": to,
    "value": value,
    "confirmations": confirmations,
    "status": status,
    "gas_used": gas,
    "gas_price": gasPrice
  };
}