class FVWeb3EstimateRpcParams {
  final String from;
  final String to;
  final String gas;
  final String gasPrice;
  final String data;

  FVWeb3EstimateRpcParams({
    required this.from,
    required this.to,
    required this.gas,
    required this.gasPrice,
    required this.data
  });

  factory FVWeb3EstimateRpcParams.fromJson(Map<String, dynamic> json) => FVWeb3EstimateRpcParams(
    from: json["from"],
    to: json["to"],
    gas: json["gas"],
    gasPrice: json["gasPrice"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "from": from,
    "to": to,
    "gas": gas,
    "gasPrice": gasPrice,
    "data": data
  };

}