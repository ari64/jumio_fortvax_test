import 'dart:convert';

FvBscGasTrackerAdapter fvBscGasTrackerAdapterFromJson(String str) => FvBscGasTrackerAdapter.fromJson(json.decode(str));

String fvBscGasTrackerAdapterToJson(FvBscGasTrackerAdapter data) => json.encode(data.toJson());

class FvBscGasTrackerAdapter {
  final int fast;
  final int standard;
  final int slow;
  final int timestamp;
  final double gasUsedRatio;
  final int pendingQueueLength;

  FvBscGasTrackerAdapter({
    required this.fast,
    required this.standard,
    required this.slow,
    required this.timestamp,
    required this.gasUsedRatio,
    required this.pendingQueueLength,
  });

  factory FvBscGasTrackerAdapter.fromJson(Map<String, dynamic> json) => FvBscGasTrackerAdapter(
    fast: int.parse(json["fast"]),
    standard: int.parse(json["standard"]),
    slow: int.parse(json["slow"]),
    timestamp: json["timestamp"],
    gasUsedRatio: json["gasUsedRatio"]?.toDouble(),
    pendingQueueLength: json["pendingQueueLength"],
  );

  Map<String, dynamic> toJson() => {
    "fast": fast,
    "standard": standard,
    "slow": slow,
    "timestamp": timestamp,
    "gasUsedRatio": gasUsedRatio,
    "pendingQueueLength": pendingQueueLength,
  };
}