import 'dart:convert';

class FortVaxApiResponse {
  String? message;
  String rawBody;

  FortVaxApiResponse({this.message, required this.rawBody});

  factory FortVaxApiResponse.fromJson(Map<String, dynamic> json) =>
      FortVaxApiResponse(
        message: json["message"],
        rawBody: jsonEncode(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "data": rawBody,
      };
}

class FortVaxPagingApiResponse extends FortVaxApiResponse {
  FortVaxPagingMeta meta;

  FortVaxPagingApiResponse(
      {super.message, required super.rawBody, required this.meta});

  factory FortVaxPagingApiResponse.fromJson(Map<String, dynamic> json) =>
      FortVaxPagingApiResponse(
        message: json["message"],
        rawBody: jsonEncode(json["data"]),
        meta: FortVaxPagingMeta.fromJson(json["meta"]),
      );

  @override
  Map<String, dynamic> toJson() => {
        "message": message,
        "data": rawBody,
        "meta": meta.toJson(),
      };
}

class FortVaxPagingMeta {
  int currentPage;
  int? from;
  int lastPage;
  int perPage;
  int? to;
  int total;

  FortVaxPagingMeta({
    required this.currentPage,
    required this.from,
    required this.lastPage,
    required this.perPage,
    required this.to,
    required this.total,
  });

  factory FortVaxPagingMeta.fromJson(Map<String, dynamic> json) =>
      FortVaxPagingMeta(
        currentPage: json["current_page"],
        from: json["from"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "from": from,
        "last_page": lastPage,
        "per_page": perPage,
        "to": to,
        "total": total,
      };
}
