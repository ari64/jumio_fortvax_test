import 'dart:convert';

List<FVPrice> fvPriceListFromJson(String str) =>
    List<FVPrice>.from(json.decode(str).map((x) => FVPrice.fromJson(x)));

String fvPriceListToJson(List<FVPrice> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVPrice {
  final String pair;
  final String base;
  final String quote;
  final String price;

  const FVPrice({required this.pair, required this.base, required this.quote, required this.price});

  factory FVPrice.fromJson(Map<String, dynamic> json) => FVPrice(
    pair: json["pair"],
    base: json["base"],
    quote: json["quote"],
    price: json["price"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "pair": pair,
    "base": base,
    "quote": quote,
    "price": price,
  };
}