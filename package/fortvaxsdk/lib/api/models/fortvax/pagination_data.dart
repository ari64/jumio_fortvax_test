import 'package:fortvax_sdk/api/models/fortvax/api_response.dart';

class FVPaginationData<T> {
  final List<T> list;
  final FortVaxPagingMeta meta;

  FVPaginationData({required this.list, required this.meta});
}
