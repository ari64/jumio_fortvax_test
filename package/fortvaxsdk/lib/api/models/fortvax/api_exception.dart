class FortVaxApiException implements Exception {
  final int statusCode;
  final String? message;
  final _prefix = "API Error";

  FortVaxApiException({required this.statusCode, this.message});

  @override
  String toString() {
    return "$_prefix($statusCode):  $message";
  }
}
