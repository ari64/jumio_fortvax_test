import 'dart:convert';

List<FVLtcTxRecord> fvLtcTxRecordFromJson(String str) =>
    List<FVLtcTxRecord>.from(
        json.decode(str).map((x) => FVLtcTxRecord.fromJson(x)));

String fvLtcTxRecordToJson(List<FVLtcTxRecord> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVLtcTxRecord {
  int? blockHeight;
  int? blockIndex;
  String hash;
  String value;
  String fees;
  int confirmations;
  DateTime? confirmed;
  DateTime received;
  List<FVLtcTxRecordInput> inputs;
  List<FVLtcTxRecordOutput> outputs;

  FVLtcTxRecord({
    required this.blockHeight,
    required this.blockIndex,
    required this.hash,
    required this.value,
    required this.fees,
    required this.confirmations,
    required this.confirmed,
    required this.received,
    required this.inputs,
    required this.outputs,
  });

  factory FVLtcTxRecord.fromJson(Map<String, dynamic> json) => FVLtcTxRecord(
        blockHeight: json["block_height"],
        blockIndex: json["block_index"],
        hash: json["hash"],
        value: json["value"],
        fees: json["fees"],
        confirmations: json["confirmations"],
        confirmed: DateTime.tryParse(json["confirmed_at"].toString()),
        received: DateTime.parse(json["received_at"]),
        inputs: List<FVLtcTxRecordInput>.from(
            json["inputs"].map((x) => FVLtcTxRecordInput.fromJson(x))),
        outputs: List<FVLtcTxRecordOutput>.from(
            json["outputs"].map((x) => FVLtcTxRecordOutput.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "block_height": blockHeight,
        "block_index": blockIndex,
        "hash": hash,
        "value": value,
        "fees": fees,
        "confirmations": confirmations,
        "confirmed_at": confirmed?.toIso8601String(),
        "received_at": received.toIso8601String(),
        "inputs": List<dynamic>.from(inputs.map((x) => x.toJson())),
        "outputs": List<dynamic>.from(outputs.map((x) => x.toJson())),
      };
}

class FVLtcTxRecordInput {
  List<String> addresses;
  String prevHash;
  int outputIndex;
  int outputValue;

  FVLtcTxRecordInput({
    required this.addresses,
    required this.prevHash,
    required this.outputIndex,
    required this.outputValue,
  });

  factory FVLtcTxRecordInput.fromJson(Map<String, dynamic> json) =>
      FVLtcTxRecordInput(
        addresses: List<String>.from(json["addresses"].map((x) => x)),
        prevHash: json["prev_hash"],
        outputIndex: json["output_index"],
        outputValue: json["output_value"],
      );

  Map<String, dynamic> toJson() => {
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
        "prev_hash": prevHash,
        "output_index": outputIndex,
        "output_value": outputValue,
      };
}

class FVLtcTxRecordOutput {
  int value;
  List<String> addresses;

  FVLtcTxRecordOutput({
    required this.value,
    required this.addresses,
  });

  factory FVLtcTxRecordOutput.fromJson(Map<String, dynamic> json) =>
      FVLtcTxRecordOutput(
        value: json["value"],
        addresses: List<String>.from(json["addresses"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
      };
}
