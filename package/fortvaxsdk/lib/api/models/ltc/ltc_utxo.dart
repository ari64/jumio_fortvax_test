import 'dart:convert';

List<FVLtcUtxo> fvLtcUtxoFromJson(String str) =>
    List<FVLtcUtxo>.from(json.decode(str).map((x) => FVLtcUtxo.fromJson(x)));

String fvLtcUtxoToJson(List<FVLtcUtxo> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVLtcUtxo {
  String txHash;
  int txInputN;
  int txOutputN;
  int value;
  int confirmations;
  String script;
  String address;
  dynamic received;

  FVLtcUtxo({
    required this.txHash,
    required this.txInputN,
    required this.txOutputN,
    required this.value,
    required this.confirmations,
    required this.script,
    required this.address,
    this.received,
  });

  factory FVLtcUtxo.fromJson(Map<String, dynamic> json) => FVLtcUtxo(
        txHash: json["tx_hash"],
        txInputN: json["tx_input_n"],
        txOutputN: json["tx_output_n"],
        value: json["value"],
        confirmations: json["confirmations"],
        script: json["script"],
        address: json["address"],
        received: json["received"],
      );

  Map<String, dynamic> toJson() => {
        "tx_hash": txHash,
        "tx_input_n": txInputN,
        "tx_output_n": txOutputN,
        "value": value,
        "confirmations": confirmations,
        "script": script,
        "address": address,
        "received": received,
      };
}
