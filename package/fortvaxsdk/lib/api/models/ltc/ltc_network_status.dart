import 'dart:convert';

FVLtcNetworkStatus fvLtcNetworkStatusFromJson(String str) =>
    FVLtcNetworkStatus.fromJson(json.decode(str));

String fvLtcNetworkStatusToJson(FVLtcNetworkStatus data) =>
    json.encode(data.toJson());

class FVLtcNetworkStatus {
  int highFeePerKb;
  int mediumFeePerKb;
  int lowFeePerKb;

  FVLtcNetworkStatus({
    required this.highFeePerKb,
    required this.mediumFeePerKb,
    required this.lowFeePerKb,
  });

  factory FVLtcNetworkStatus.fromJson(Map<String, dynamic> json) =>
      FVLtcNetworkStatus(
        highFeePerKb: json["high_fee_per_kb"],
        mediumFeePerKb: json["medium_fee_per_kb"],
        lowFeePerKb: json["low_fee_per_kb"],
      );

  Map<String, dynamic> toJson() => {
        "high_fee_per_kb": highFeePerKb,
        "medium_fee_per_kb": mediumFeePerKb,
        "low_fee_per_kb": lowFeePerKb,
      };
}
