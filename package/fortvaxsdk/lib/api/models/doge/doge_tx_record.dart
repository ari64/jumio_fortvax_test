import 'dart:convert';

List<FVDogeTxRecord> fvDogeTxRecordFromJson(String str) => List<FVDogeTxRecord>.from(json.decode(str).map((x) => FVDogeTxRecord.fromJson(x)));

String fvDogeTxRecordToJson(List<FVDogeTxRecord> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVDogeTxRecord {
    int? blockHeight;
    int? blockIndex;
    String hash;
    List<String> addresses;
    String value;
    String fees;
    int confirmations;
    DateTime? confirmedAt;
    DateTime receivedAt;
    List<FVDogeInput> inputs;
    List<FVDogeOutput> outputs;

    FVDogeTxRecord({
        required this.blockHeight,
        required this.blockIndex,
        required this.hash,
        required this.addresses,
        required this.value,
        required this.fees,
        required this.confirmations,
        required this.confirmedAt,
        required this.receivedAt,
        required this.inputs,
        required this.outputs,
    });

    factory FVDogeTxRecord.fromJson(Map<String, dynamic> json) {
      int? confirmAtTimestamp = json["confirmed_at"];
      DateTime? confirmDataTime;

      if (confirmAtTimestamp != null) {
          confirmDataTime = DateTime.fromMillisecondsSinceEpoch(
              confirmAtTimestamp * 1000,
              isUtc: true);
      }

      return FVDogeTxRecord(
          blockHeight: json["block_height"],
          blockIndex: json["block_index"],
          hash: json["hash"],
          addresses: List<String>.from(json["addresses"].map((x) => x)),
          value: json["value"],
          fees: json["fees"],
          confirmations: json["confirmations"],
          confirmedAt: confirmDataTime,
          receivedAt: DateTime.fromMillisecondsSinceEpoch(
              json["received_at"] * 1000,
              isUtc: true),
          inputs: List<FVDogeInput>.from(
              json["inputs"].map((x) => FVDogeInput.fromJson(x))),
          outputs: List<FVDogeOutput>.from(
              json["outputs"].map((x) => FVDogeOutput.fromJson(x))),
      );
    }

    Map<String, dynamic> toJson() => {
        "block_height": blockHeight,
        "block_index": blockIndex,
        "hash": hash,
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
        "value": value,
        "fees": fees,
        "confirmations": confirmations,
        "confirmed_at": confirmedAt?.toIso8601String(),
        "received_at": receivedAt.toIso8601String(),
        "inputs": List<dynamic>.from(inputs.map((x) => x.toJson())),
        "outputs": List<dynamic>.from(outputs.map((x) => x.toJson())),
    };
}

class FVDogeInput {
    List<String> addresses;
    String prevHash;
    int outputIndex;
    int outputValue;

    FVDogeInput({
        required this.addresses,
        required this.prevHash,
        required this.outputIndex,
        required this.outputValue,
    });

    factory FVDogeInput.fromJson(Map<String, dynamic> json) => FVDogeInput(
        addresses: List<String>.from(json["addresses"].map((x) => x)),
        prevHash: json["prev_hash"],
        outputIndex: json["output_index"],
        outputValue: json["output_value"],
    );

    Map<String, dynamic> toJson() => {
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
        "prev_hash": prevHash,
        "output_index": outputIndex,
        "output_value": outputValue,
    };
}

class FVDogeOutput {
    int value;
    List<String> addresses;

    FVDogeOutput({
        required this.value,
        required this.addresses,
    });

    factory FVDogeOutput.fromJson(Map<String, dynamic> json) => FVDogeOutput(
        value: json["value"],
        addresses: List<String>.from(json["addresses"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "value": value,
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
    };
}