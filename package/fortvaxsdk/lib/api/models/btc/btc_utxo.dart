import 'dart:convert';

List<FVBtcUtxo> fvBtcUtxoFromJson(String str) =>
    List<FVBtcUtxo>.from(json.decode(str).map((x) => FVBtcUtxo.fromJson(x)));

String fvBtcUtxoToJson(List<FVBtcUtxo> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVBtcUtxo {
  String txHash;
  int txInputN;
  int txOutputN;
  int value;
  int confirmations;
  String script;
  String address;
  dynamic received;

  FVBtcUtxo({
    required this.txHash,
    required this.txInputN,
    required this.txOutputN,
    required this.value,
    required this.confirmations,
    required this.script,
    required this.address,
    this.received,
  });

  factory FVBtcUtxo.fromJson(Map<String, dynamic> json) => FVBtcUtxo(
        txHash: json["tx_hash"],
        txInputN: json["tx_input_n"],
        txOutputN: json["tx_output_n"],
        value: json["value"],
        confirmations: json["confirmations"],
        script: json["script"],
        address: json["address"],
        received: json["received"],
      );

  Map<String, dynamic> toJson() => {
        "tx_hash": txHash,
        "tx_input_n": txInputN,
        "tx_output_n": txOutputN,
        "value": value,
        "confirmations": confirmations,
        "script": script,
        "address": address,
        "received": received,
      };
}
