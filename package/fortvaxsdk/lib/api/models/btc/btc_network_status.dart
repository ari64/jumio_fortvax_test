import 'dart:convert';

FVBtcNetworkStatus fvBtcNetworkStatusFromJson(String str) =>
    FVBtcNetworkStatus.fromJson(json.decode(str));

String fvBtcNetworkStatusToJson(FVBtcNetworkStatus data) =>
    json.encode(data.toJson());

class FVBtcNetworkStatus {
  int highFeePerKb;
  int mediumFeePerKb;
  int lowFeePerKb;

  FVBtcNetworkStatus({
    required this.highFeePerKb,
    required this.mediumFeePerKb,
    required this.lowFeePerKb,
  });

  factory FVBtcNetworkStatus.fromJson(Map<String, dynamic> json) =>
      FVBtcNetworkStatus(
        highFeePerKb: json["high_fee_per_kb"],
        mediumFeePerKb: json["medium_fee_per_kb"],
        lowFeePerKb: json["low_fee_per_kb"],
      );

  Map<String, dynamic> toJson() => {
        "high_fee_per_kb": highFeePerKb,
        "medium_fee_per_kb": mediumFeePerKb,
        "low_fee_per_kb": lowFeePerKb,
      };
}
