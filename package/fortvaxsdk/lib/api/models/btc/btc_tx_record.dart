import 'dart:convert';

List<FVBtcTxRecord> fvBtcTxRecordFromJson(String str) =>
    List<FVBtcTxRecord>.from(
        json.decode(str).map((x) => FVBtcTxRecord.fromJson(x)));

String fvBtcTxRecordToJson(List<FVBtcTxRecord> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVBtcTxRecord {
  int? blockHeight;
  int? blockIndex;
  String hash;
  String value;
  String fees;
  int confirmations;
  DateTime? confirmed;
  DateTime received;
  List<FVBtcTxRecordInput> inputs;
  List<FVBtcTxRecordOutput> outputs;

  FVBtcTxRecord({
    required this.blockHeight,
    required this.blockIndex,
    required this.hash,
    required this.value,
    required this.fees,
    required this.confirmations,
    required this.confirmed,
    required this.received,
    required this.inputs,
    required this.outputs,
  });

  factory FVBtcTxRecord.fromJson(Map<String, dynamic> json) => FVBtcTxRecord(
        blockHeight: json["block_height"],
        blockIndex: json["block_index"],
        hash: json["hash"],
        value: json["value"],
        fees: json["fees"],
        confirmations: json["confirmations"],
        confirmed: DateTime.tryParse(json["confirmed_at"].toString()),
        received: DateTime.parse(json["received_at"]),
        inputs: List<FVBtcTxRecordInput>.from(
            json["inputs"].map((x) => FVBtcTxRecordInput.fromJson(x))),
        outputs: List<FVBtcTxRecordOutput>.from(
            json["outputs"].map((x) => FVBtcTxRecordOutput.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "block_height": blockHeight,
        "block_index": blockIndex,
        "hash": hash,
        "value": value,
        "fees": fees,
        "confirmations": confirmations,
        "confirmed_at": confirmed?.toIso8601String(),
        "received_at": received.toIso8601String(),
        "inputs": List<dynamic>.from(inputs.map((x) => x.toJson())),
        "outputs": List<dynamic>.from(outputs.map((x) => x.toJson())),
      };
}

class FVBtcTxRecordInput {
  List<String> addresses;
  String prevHash;
  int outputIndex;
  int outputValue;

  FVBtcTxRecordInput({
    required this.addresses,
    required this.prevHash,
    required this.outputIndex,
    required this.outputValue,
  });

  factory FVBtcTxRecordInput.fromJson(Map<String, dynamic> json) =>
      FVBtcTxRecordInput(
        addresses: List<String>.from(json["addresses"].map((x) => x)),
        prevHash: json["prev_hash"],
        outputIndex: json["output_index"],
        outputValue: json["output_value"],
      );

  Map<String, dynamic> toJson() => {
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
        "prev_hash": prevHash,
        "output_index": outputIndex,
        "output_value": outputValue,
      };
}

class FVBtcTxRecordOutput {
  int value;
  List<String> addresses;

  FVBtcTxRecordOutput({
    required this.value,
    required this.addresses,
  });

  factory FVBtcTxRecordOutput.fromJson(Map<String, dynamic> json) =>
      FVBtcTxRecordOutput(
        value: json["value"],
        addresses: List<String>.from(json["addresses"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "addresses": List<dynamic>.from(addresses.map((x) => x)),
      };
}
