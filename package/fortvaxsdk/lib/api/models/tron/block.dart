import 'dart:convert';

FVBlock fvBlockFromJson(String str) => FVBlock.fromJson(json.decode(str));

String fvBlockToJson(FVBlock data) => json.encode(data.toJson());

class FVBlock {
  String blockId;
  FVBlockHeader blockHeader;

  FVBlock({
    required this.blockId,
    required this.blockHeader,
  });

  factory FVBlock.fromJson(Map<String, dynamic> json) => FVBlock(
        blockId: json["blockID"],
        blockHeader: FVBlockHeader.fromJson(json["block_header"]),
      );

  Map<String, dynamic> toJson() => {
        "blockID": blockId,
        "block_header": blockHeader.toJson(),
      };
}

class FVBlockHeader {
  FVBlockRawData rawData;
  String witnessSignature;

  FVBlockHeader({
    required this.rawData,
    required this.witnessSignature,
  });

  factory FVBlockHeader.fromJson(Map<String, dynamic> json) => FVBlockHeader(
        rawData: FVBlockRawData.fromJson(json["raw_data"]),
        witnessSignature: json["witness_signature"],
      );

  Map<String, dynamic> toJson() => {
        "raw_data": rawData.toJson(),
        "witness_signature": witnessSignature,
      };
}

class FVBlockRawData {
  int number;
  String txTrieRoot;
  String witnessAddress;
  String parentHash;
  int version;
  int timestamp;

  FVBlockRawData({
    required this.number,
    required this.txTrieRoot,
    required this.witnessAddress,
    required this.parentHash,
    required this.version,
    required this.timestamp,
  });

  factory FVBlockRawData.fromJson(Map<String, dynamic> json) => FVBlockRawData(
        number: json["number"],
        txTrieRoot: json["txTrieRoot"],
        witnessAddress: json["witness_address"],
        parentHash: json["parentHash"],
        version: json["version"],
        timestamp: json["timestamp"],
      );

  Map<String, dynamic> toJson() => {
        "number": number,
        "txTrieRoot": txTrieRoot,
        "witness_address": witnessAddress,
        "parentHash": parentHash,
        "version": version,
        "timestamp": timestamp,
      };
}
