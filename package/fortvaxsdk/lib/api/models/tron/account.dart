import 'dart:convert';

FVAccount fvAccountFromJson(String str) => FVAccount.fromJson(json.decode(str));

String fvAccountToJson(FVAccount data) => json.encode(data.toJson());

class FVAccount {
  bool activated;
  FVResource resource;
  FVStakingInfo staking;
  List<FVUnstakingItem> unstaking;

  FVAccount({
    required this.activated,
    required this.resource,
    required this.staking,
    required this.unstaking,
  });

  factory FVAccount.fromJson(Map<String, dynamic> json) => FVAccount(
        activated: json["activated"],
        resource: FVResource.fromJson(json["resource"]),
        staking: FVStakingInfo.fromJson(json["staking"]),
        unstaking: List<FVUnstakingItem>.from(
            json["unstaking"].map((x) => FVUnstakingItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "activated": activated,
        "resource": resource.toJson(),
        "staking": staking.toJson(),
        "unstaking": List<dynamic>.from(unstaking.map((x) => x.toJson())),
      };
}

class FVResource {
  int bandwidth;
  int energy;
  int bandwidthLimit;
  int energyLimit;

  FVResource({
    required this.bandwidth,
    required this.energy,
    required this.bandwidthLimit,
    required this.energyLimit,
  });

  factory FVResource.fromJson(Map<String, dynamic> json) => FVResource(
        bandwidth: json["bandwidth"],
        energy: json["energy"],
        bandwidthLimit: json["bandwidthLimit"],
        energyLimit: json["energyLimit"],
      );

  Map<String, dynamic> toJson() => {
        "bandwidth": bandwidth,
        "energy": energy,
        "bandwidthLimit": bandwidthLimit,
        "energyLimit": energyLimit,
      };
}

class FVStakingInfo {
  String bandwidth;
  String energy;

  FVStakingInfo({
    required this.bandwidth,
    required this.energy,
  });

  factory FVStakingInfo.fromJson(Map<String, dynamic> json) => FVStakingInfo(
        bandwidth: json["bandwidth"],
        energy: json["energy"],
      );

  Map<String, dynamic> toJson() => {
        "bandwidth": bandwidth,
        "energy": energy,
      };
}

class FVUnstakingItem {
  String type;
  int amount;
  int expireTime;

  FVUnstakingItem({
    required this.type,
    required this.amount,
    required this.expireTime,
  });

  factory FVUnstakingItem.fromJson(Map<String, dynamic> json) =>
      FVUnstakingItem(
        type: json["type"],
        amount: json["amount"],
        expireTime: json["expire_time"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "amount": amount,
        "expire_time": expireTime,
      };
}
