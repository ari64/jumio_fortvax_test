import 'dart:convert';

List<FVAsset> fvAssetFromJson(String str) =>
    List<FVAsset>.from(json.decode(str).map((x) => FVAsset.fromJson(x)));

String fvAssetToJson(List<FVAsset> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVAsset {
  String symbol;
  String balance;
  String rawBalance;
  String image;

  FVAsset({
    required this.symbol,
    required this.balance,
    required this.rawBalance,
    required this.image,
  });

  factory FVAsset.fromJson(Map<String, dynamic> json) => FVAsset(
        symbol: json["symbol"],
        balance: json["balance"],
        rawBalance: json["raw_balance"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "symbol": symbol,
        "balance": balance,
        "raw_balance": rawBalance,
        "image": image,
      };
}
