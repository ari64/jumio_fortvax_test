import 'dart:convert';

FVConsumeEnergy fvConsumeEnergyFromJson(String str) =>
    FVConsumeEnergy.fromJson(json.decode(str));

String fvConsumeEnergyToJson(FVConsumeEnergy data) =>
    json.encode(data.toJson());

class FVConsumeEnergy {
  int estimate;

  FVConsumeEnergy({
    required this.estimate,
  });

  factory FVConsumeEnergy.fromJson(Map<String, dynamic> json) =>
      FVConsumeEnergy(
        estimate: json["estimate"],
      );

  Map<String, dynamic> toJson() => {
        "estimate": estimate,
      };
}
