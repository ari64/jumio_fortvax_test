import 'dart:convert';

List<FVTronTxRecord> fvTronTxRecordFromJson(String str) =>
    List<FVTronTxRecord>.from(
        json.decode(str).map((x) => FVTronTxRecord.fromJson(x)));

String fvTronTxRecordToJson(List<FVTronTxRecord> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FVTronTxRecord {
  int blockNumber;
  int timestamp;
  String hash;
  String from;
  String to;
  int? contractType;
  int? confirmed;
  String contractRet;
  String result;
  String value;
  int? fee;

  FVTronTxRecord({
    required this.blockNumber,
    required this.timestamp,
    required this.hash,
    required this.from,
    required this.to,
    required this.contractType,
    required this.confirmed,
    required this.contractRet,
    required this.result,
    required this.value,
    required this.fee
  });

  factory FVTronTxRecord.fromJson(Map<String, dynamic> json) => FVTronTxRecord(
        blockNumber: json["block_number"],
        timestamp: json["timestamp"],
        hash: json["hash"],
        from: json["from"],
        to: json["to"],
        contractType: json["contract_type"],
        confirmed: json["confirmed"],
        contractRet: json["contract_ret"],
        result: json["result"],
        value: json["value"],
        fee: json["cost"]?["fee"] == null ? (json["cost"]?["net_fee"] ?? 0) + (json["cost"]?["energy_fee"] ?? 0) : json["cost"]?["fee"]
      );

  Map<String, dynamic> toJson() => {
        "block_number": blockNumber,
        "timestamp": timestamp,
        "hash": hash,
        "from": from,
        "to": to,
        "contract_type": contractType,
        "confirmed": confirmed,
        "contract_ret": contractRet,
        "result": result,
        "value": value,
        "cost": {
          "fee": fee
        }
      };
}
