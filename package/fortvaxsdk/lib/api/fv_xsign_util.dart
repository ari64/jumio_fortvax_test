import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:fortvax_sdk/api/sdk_api_interface.dart';
import 'package:fortvax_sdk/extension/strings.dart';
import 'package:pointycastle/export.dart';

class FVXSignatureUtil {
  FVXSignatureUtil._();

  static final instance = FVXSignatureUtil._();

  int _latencyTime = 0;

  //hex
  String _tempXsign = "";

  String xsign() {
    List<int> numbers = [30, 40, 50, 35, 45, 55];
    Random random = Random();
    int randomIndex = random.nextInt(numbers.length);
    int randomElement = numbers[randomIndex];
    int currentTimeMillis = DateTime.now().millisecondsSinceEpoch ~/ 1000;

    if (currentTimeMillis - _latencyTime >= randomElement) {
      _latencyTime = currentTimeMillis;
      //重建
      final sign = FVApiSignature();
      sign.card = FortVaxServiceConfig.cardId;
      sign.walletId = FortVaxServiceConfig.walletId;
      sign.nonce = _latencyTime;
      final jsonText = jsonEncode(sign);

      //to utf8 byte array
      final rawBytes = utf8.encode(jsonText);
      final List<int> padding = [];
      padding.addAll(rawBytes);
      for (int i = rawBytes.length; i < 256; i++) {
        padding.add(0);
      }

      final rawPk = FortVaxServiceConfig.apiKey.toBytes();
      final pk = RSAPublicKey(BigInt.parse(rawPk.bytesToHex(), radix: 16), BigInt.from(65537));
      final rasCipher = RSAEngine()..init(true, PublicKeyParameter<RSAPublicKey>(pk));
      final encrypt = rasCipher.process(Uint8List.fromList(padding));
      _tempXsign = encrypt.bytesToHex();
      return _tempXsign;
    } else {
      //不重建
      return _tempXsign;
    }
  }
}

class FVApiSignature {
  var nonce = DateTime.now().millisecondsSinceEpoch ~/ 1000;
  var card = "";
  var walletId = "";

  Map<String, dynamic> toJson() {
    return {'card': card, 'nonce': nonce, 'wallet_id': walletId};
  }
}