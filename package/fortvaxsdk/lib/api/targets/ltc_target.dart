import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxLtcAssetBalanceTarget extends FortVaxServiceTarget {
  final List<String> addresses;

  FortVaxLtcAssetBalanceTarget(this.addresses);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/ltc/multiaddr/utxos";
  @override
  Map<String, dynamic>? get postBody => {'addresses': addresses};
}

class FortVaxLtcTransactionRecordsTarget extends FortVaxServiceTarget {
  final List<String> addresses;
  final int page;

  FortVaxLtcTransactionRecordsTarget(this.addresses, this.page);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/ltc/multiaddr/transactions";
  @override
  Map<String, String>? get queryParameters => {'page': page.toString()};
  @override
  Map<String, dynamic>? get postBody => {'addresses': addresses};
}

class FortVaxLtcBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  FortVaxLtcBroadcastTarget(this.rawTx);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/ltc/broadcast";
  @override
  Map<String, dynamic>? get postBody => {'data': rawTx};
}

class FortVaxLtcNetworkStatusTarget extends FortVaxServiceTarget {
  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/ltc/status/network";
}
