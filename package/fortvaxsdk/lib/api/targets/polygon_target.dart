import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxEIP1559GasStationTarget extends FortVaxServiceTarget {
  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/polygon/suggested_gas";
}

class FortVaxPendingNonceTarget extends FortVaxServiceTarget {
  final String address;

  FortVaxPendingNonceTarget(this.address) {
    queryParameters = {"address": address};
  }

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/polygon/pending_nonce";
}

class FortVaxBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  final String address;
  FortVaxBroadcastTarget(this.rawTx, this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/polygon/broadcast";
  @override
  Map<String, dynamic>? get postBody => {'data': rawTx, 'address': address};
}
