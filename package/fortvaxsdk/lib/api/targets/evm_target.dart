import 'dart:math';

import 'package:fortvax_sdk/api/models/evm/all_models.dart';
import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxTransactionRecordTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String address;
  final int page;

  FortVaxTransactionRecordTarget(this.blockchain, this.address, this.page) {
    queryParameters = {"page": "$page"};
  }

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/$blockchain/$address/transactions";
}

class FortVaxERC20TransactionRecordTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String address;
  final String token;
  final int page;

  FortVaxERC20TransactionRecordTarget(
      this.blockchain, this.address, this.token, this.page) {
    queryParameters = {
      "page": "$page",
      "contract_address": token,
    };
  }

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/$blockchain/$address/erc20/transfers";
}

class FortVaxBalanceTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String address;

  FortVaxBalanceTarget(this.blockchain, this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/$blockchain/$address/assets";
}

class FortVaxERC20BalanceTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String address;
  final String contractAddress;

  FortVaxERC20BalanceTarget(
      this.blockchain, this.address, this.contractAddress);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/$blockchain/$address/assets";
  @override
  Map<String, dynamic>? get postBody => {
        'tokens': [contractAddress]
      };
}

class FortVaxContractInfoTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String address;

  FortVaxContractInfoTarget(this.blockchain, this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() =>
      "/api/v4/$blockchain/contract_address/$address/info";
}

class FortVaxEstimateGasLimitTarget extends FortVaxServiceTarget {
  final String blockchain;
  final String fromAddress;
  final String toAddress;
  final String contractAddress;
  final double transferAmount;
  final int decimal;
  final double suggestedGas;

  FortVaxEstimateGasLimitTarget(
      {required this.blockchain,
      required this.fromAddress,
      required this.toAddress,
      required this.contractAddress,
      required this.transferAmount,
      required this.decimal,
      required this.suggestedGas});

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/$blockchain/estimate_gas";
  @override
  Map<String, dynamic>? get postBody => {
        'params': FVWeb3EstimateRpcParams(
                from: fromAddress,
                to: contractAddress,
                gas: '0x${100000.toRadixString(16)}',
                gasPrice: '0x${suggestedGas.toInt().toRadixString(16)}',
                data:
                    "0xa9059cbb${toAddress.substring(2, toAddress.length).padLeft(64, '0')}${(transferAmount * pow(10, decimal)).toInt().toRadixString(16).padLeft(64, '0')}")
            .toJson()
      };
}
