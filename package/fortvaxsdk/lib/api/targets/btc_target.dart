import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxBtcAssetBalanceTarget extends FortVaxServiceTarget {
  final List<String> addresses;
  FortVaxBtcAssetBalanceTarget(this.addresses);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/btc/multiaddr/utxos";
  @override
  Map<String, dynamic> get postBody => {'addresses': addresses};
}

class FortVaxBtcTransactionRecordsTarget extends FortVaxServiceTarget {
  final List<String> addresses;
  final int page;
  FortVaxBtcTransactionRecordsTarget(this.addresses, this.page);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/btc/multiaddr/transactions";
  @override
  Map<String, String>? get queryParameters => {'page': page.toString()};
  @override
  Map<String, dynamic>? get postBody => {'addresses': addresses};
}

class FortVaxBtcBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  FortVaxBtcBroadcastTarget(this.rawTx);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/btc/broadcast";
  @override
  Map<String, String> get postBody => {'data': rawTx};
}

class FortVaxBtcNetworkStatusTarget extends FortVaxServiceTarget {
  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/btc/status/network";
}
