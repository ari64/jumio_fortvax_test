import 'package:fortvax_sdk/api/sdk_api_interface.dart';

// general service
class FortVaxTronAssetBalanceTarget extends FortVaxServiceTarget {
  final String address;
  FortVaxTronAssetBalanceTarget(this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/tron/address/$address/assets";
  @override
  Map<String, String>? get queryParameters => {'address': address};
}

class FortVaxTronAssetTrc20BalanceTarget extends FortVaxServiceTarget {
  final String address;
  final String symbol;
  FortVaxTronAssetTrc20BalanceTarget(this.address, this.symbol);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/tron/address/$address/assets";
  @override
  Map<String, String> get queryParameters => {'address': address};
}

// Tron service
class FortVaxTronAccountInformationTarget extends FortVaxServiceTarget {
  final String address;
  FortVaxTronAccountInformationTarget(this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/tron/account";
  @override
  Map<String, String> get queryParameters => {'address': address};
}

class FortVaxTronCurrentBlockTarget extends FortVaxServiceTarget {
  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/tron/current_block";
}

class FortVaxTronTransactionsRecordTarget extends FortVaxServiceTarget {
  final String address;
  final int page;
  FortVaxTronTransactionsRecordTarget(this.address, this.page);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() =>
      "/api/v4/tron/address/$address/transactions/normal";
  @override
  Map<String, String>? get queryParameters => {'page': page.toString()};
}

class FortVaxTronTokenTransactionRecordsTarget extends FortVaxServiceTarget {
  final String address;
  final String contractAddress;
  final int page;
  FortVaxTronTokenTransactionRecordsTarget(
      this.address, this.contractAddress, this.page);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() =>
      "/api/v4/tron/address/$address/transactions/token";
  @override
  Map<String, String>? get queryParameters =>
      {'page': page.toString(), 'contract_address': contractAddress};
}

class FortVaxTronBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  final String fromAddress;
  FortVaxTronBroadcastTarget(this.rawTx, this.fromAddress);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/tron/broadcast";
  @override
  Map<String, dynamic>? get postBody => {'data': rawTx, "address": fromAddress};
}

class FortVaxTronConsumeEnergyTarget extends FortVaxServiceTarget {
  String contractAddress;
  String parameter;
  FortVaxTronConsumeEnergyTarget(this.contractAddress, this.parameter);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/tron/estimate_energy";
  @override
  Map<String, dynamic>? get postBody => {
        'contract_address': contractAddress,
        'parameter': parameter,
        'function_selector': 'transfer(address,uint256)'
      };
}

class FortVaxTronContractInfoTarget extends FortVaxServiceTarget {
  String contractAddress;
  FortVaxTronContractInfoTarget(this.contractAddress);

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() =>
      "/api/v4/tron/contract_address/$contractAddress/info";
}

class FortVaxTronEstimateObtainEnergyTarget extends FortVaxServiceTarget {
  String address;
  int amount;
  FortVaxTronEstimateObtainEnergyTarget(
    this.address,
    this.amount,
  );

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/tron/staking/estimate/energy";
  @override
  Map<String, dynamic>? get postBody => {
        "address": address,
        "amount": amount,
      };
}

class FortVaxTronEstimateObtainBandwidthTarget extends FortVaxServiceTarget {
  String address;
  int amount;
  FortVaxTronEstimateObtainBandwidthTarget(
    this.address,
    this.amount,
  );

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/tron/staking/estimate/bandwidth";
  @override
  Map<String, dynamic>? get postBody => {
        "address": address,
        "amount": amount,
      };
}
