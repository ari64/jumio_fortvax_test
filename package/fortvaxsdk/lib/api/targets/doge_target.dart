import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxDogeBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  FortVaxDogeBroadcastTarget(this.rawTx);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/doge/broadcast";
  @override
  Map<String, dynamic>? get postBody => {
        "data": rawTx,
      };
}

class FortVaxDogeAssetBalanceTarget extends FortVaxServiceTarget {
  final List<String> addresses;
  FortVaxDogeAssetBalanceTarget(this.addresses);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/doge/multiaddr/utxos";
  @override
  Map<String, dynamic>? get postBody => {
        "addresses": addresses,
      };
}

class FortVaxDogeTransactionRecordsTarget extends FortVaxServiceTarget {
  final List<String> addresses;
  final int page;
  FortVaxDogeTransactionRecordsTarget(
    this.addresses,
    this.page,
  );

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/doge/multiaddr/transactions";
  @override
  Map<String, String>? get queryParameters => {'page': page.toString()};
  @override
  Map<String, dynamic>? get postBody => {"addresses": addresses};
}
