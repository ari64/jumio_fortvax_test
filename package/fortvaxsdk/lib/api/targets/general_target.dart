import 'package:fortvax_sdk/sdk.dart';

class FortVaxExchangeTickerTarget extends FortVaxServiceTarget {
  FortVaxExchangeTickerTarget();

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/tickers";
}
