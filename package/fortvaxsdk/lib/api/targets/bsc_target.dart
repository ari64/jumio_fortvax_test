import 'package:fortvax_sdk/api/sdk_api_interface.dart';

class FortVaxGasStationTarget extends FortVaxServiceTarget {
  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/bsc/suggested_gas";
}

class FortVaxPendingNonceTarget extends FortVaxServiceTarget {
  final String address;

  FortVaxPendingNonceTarget(this.address) {
    queryParameters = {"address": address};
  }

  @override
  HttpMethod httpMethod() => HttpMethod.get;
  @override
  String path() => "/api/v4/bsc/pending_nonce";
}

class FortVaxBroadcastTarget extends FortVaxServiceTarget {
  final String rawTx;
  final String address;
  FortVaxBroadcastTarget(this.rawTx, this.address);

  @override
  HttpMethod httpMethod() => HttpMethod.post;
  @override
  String path() => "/api/v4/bsc/broadcast";
  @override
  Map<String, dynamic>? get postBody => {'data': rawTx, 'address': address};
}
