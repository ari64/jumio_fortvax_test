import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'sdk_platform_interface.dart';

/// An implementation of [SdkPlatform] that uses method channels.
class MethodChannelSdk extends FortVaxSdkPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('fortvax_sdk');
  final eventChannel = const EventChannel("fortvax_sdk.event");

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String?> callJsonRpc(Map<String, dynamic> request) async {
    final result = await methodChannel.invokeMethod<String>("callJsonRpc", request);
    return result;
  }

  @override
  StreamSubscription onListenStreamData(onEvent, onError) {
    return eventChannel.receiveBroadcastStream().listen(onEvent, onError: onError);
  }
}
