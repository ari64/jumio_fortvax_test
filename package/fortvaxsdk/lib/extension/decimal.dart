import 'package:decimal/decimal.dart';

extension DecimalExtension on Decimal {
  Decimal powTen(int decimal) {
    final sq = Decimal.ten.pow(decimal).toDecimal();
    return this * sq;
  }

  Decimal divideTen(int decimal) {
    final sq = Decimal.ten.pow(decimal).toDecimal();
    return (this / sq).toDecimal();
  }
}
