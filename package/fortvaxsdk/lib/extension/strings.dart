extension StringExtension on String {
  List<int> toBytes() {
    List<int> bytes = [];

    try {
      for (int i = 0; i < length; i += 2) {
        String hex = substring(i, i + 2);
        int byte = int.parse(hex, radix: 16);
        bytes.add(byte);
      }
    } catch (_) {}

    return bytes;
  }
}

extension BytesExtension on List<int> {
  String bytesToHex() {
    return map((byte) => byte.toRadixString(16).padLeft(2, '0')).join();
  }
}
