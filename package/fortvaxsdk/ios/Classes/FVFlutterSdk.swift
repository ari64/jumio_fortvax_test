//
//  FVFlutterSdk.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/9.
//

import CoreNFC
import Flutter

/**
 * 中間掛一層 flutter 專用，處理各種 json rpc
 */
class FVFlutterSdk: NSObject, NFCTagReaderSessionDelegate {

    public static let walletExecuteEvent = "walletExecuteEvent"
    public static let walletConnectFailEvent = "walletConnectFailEvent"
    public static let walletPasswordCreatedEvent = "walletPasswordCreatedEvent"

    private var session: NFCTagReaderSession?
    private let sq = DispatchQueue(label: "nfc.session.fortvax.flutter.sdk", qos: .default)

    private var isProcessing: Bool = false
    private var eventSink: FlutterEventSink?

    //需要顯示就給，否則都是預設空白字
    var scanAlertMessage = ""
    var errorAlertMessage = ""
    var finishAlertMessage = ""
    var readingAlertMessage = ""

    private var jsonRpc: FVJsonRpc?
    private var result: FlutterResult?
    private var tag: NFCISO7816Tag?

    private var executor: FVBaseSdkJsonRpcExecutor?
    //第一次開啟感應時
    func starScanAndHandleJsonRpc(_ jsonRpc: FVJsonRpc, eventSink: FlutterEventSink?, result: @escaping FlutterResult) {
        session?.invalidate()
        session = nil
        isProcessing = false
        
        guard NFCTagReaderSession.readingAvailable else {
            return
        }
        
        self.jsonRpc = jsonRpc
        self.result = result
        self.eventSink = eventSink

        session = NFCTagReaderSession(pollingOption: .iso14443, delegate: self, queue: sq)
        session?.alertMessage = scanAlertMessage
        session?.begin()
    }

    //連線不關閉，持續感應
    func handleJsonRpc(_ jsonRpc: FVJsonRpc, isFromTagSession: Bool = false, eventSink: FlutterEventSink?, result: @escaping FlutterResult) {
        // 藉由 isFromTagSession 判斷這個 JsonRpc 是來自 TagSession 還是外部
        // 如果 isFromTagSession 為 true，那麼就讓請求持續進行（否則原本的正常流程 tagSession -> handleJsonRpc 會卡住）
        // 如果 isFromTagSession 為 false，就需要判斷 jsonRpc 是否相同來決定他能否持續進行（連續使用）
        if !isFromTagSession && jsonRpc == self.jsonRpc {
            // NFC 有可能出現已經呼叫 startScan 但 NFC 並沒有被成功喚起可感應狀態（在 stop_scan 後立即執行有機率會發生）
            // 因此這邊會根據 isProcessing 判斷 nfc 是否正在執行中。若無，則開始從 startScan 開始執行
            if !isProcessing {
                starScanAndHandleJsonRpc(jsonRpc, eventSink: eventSink, result: result)
            }
            return
        }

        let decoder = JSONDecoder()

        do {


            switch jsonRpc.method {
            case FVJsonRpcMethod.readCardStatus.rawValue:
                let p = try decoder.decode(FVJsonParamReadCardStatus.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVReadCardStatusRpcExecutor(jsonRpcId: jsonRpc.id, tag: tag, cardId: nil, caPublicKey: p.caPublicKey, pinCode: nil)
                break

            case FVJsonRpcMethod.createWallet.rawValue:
                let p = try decoder.decode(FVJsonParamCreateWallet.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVCreateWalletRpcExecutor(wordCount: p.wordCount, createPassword: p.createPassword, jsonRpcId: jsonRpc.id, tag: tag, cardId: p.cardId, caPublicKey: p.caPublicKey, pinCode: p.pinCode)

            case FVJsonRpcMethod.resetWallet.rawValue:
                let p = try decoder.decode(FVJsonParamResetWallet.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVResetWalletRpcExecutor(jsonRpcId: jsonRpc.id, tag: tag, cardId: nil, caPublicKey: p.caPublicKey, pinCode: nil)

            case FVJsonRpcMethod.restoreWallet.rawValue:
                let p = try decoder.decode(FVJsonParamRestoreWallet.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVRestoreWalletRpcExecutor(words: p.words, createPassword: p.createPassword, jsonRpcId: jsonRpc.id, tag: tag, cardId: p.cardId, caPublicKey: p.caPublicKey, pinCode: p.pinCode)

            case FVJsonRpcMethod.readAccountPublicKey.rawValue:
                let p = try decoder.decode(FVJsonParamReadAccountModelKey.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVReadAccountModelKeyRpcExecutor(supportMainNet: p.supportedCryptoMainNet, jsonRpcId: jsonRpc.id, tag: tag, cardId: p.cardId, caPublicKey: p.caPublicKey, pinCode: p.pinCode)

            case FVJsonRpcMethod.readUtxoPublicKey.rawValue:
                let p = try decoder.decode(FVJsonParamReadUtxoModelKey.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVReadUtxoModelKeyRpcExecutor(type: p.type, jsonRpcId: jsonRpc.id, tag: tag, cardId: p.cardId, caPublicKey: p.caPublicKey, pinCode: p.pinCode)

            case FVJsonRpcMethod.newBlockchain.rawValue:
                let p = try decoder.decode(FVJsonParamNewBlockchain.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVNewBlockchainRpcExecutor(types: p.types, jsonRpcId: jsonRpc.id, tag: tag, cardId: p.cardId, caPublicKey: p.caPublicKey, pinCode: p.pinCode)

            case FVJsonRpcMethod.sign.rawValue:
                executor = FVSignRpcExecutor(signJsonParams: jsonRpc.params ?? "", jsonRpcId: jsonRpc.id, tag: tag)
                break

            case FVJsonRpcMethod.settleUtxoTransaction.rawValue:
                let p = try decoder.decode(FVJsonParamSettleUtxo.self, from: jsonRpc.params?.data(using: .utf8) ?? Data())
                executor = FVSettleUtxoTransactionRpcExecutor(jsonRpcId: jsonRpc.id, params: p)
            default:
                self.isProcessing = false
                result(FlutterError(code: String(FVJsonRpcErrorType.methodNotFound.code), message: FVJsonRpcErrorType.methodNotFound.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.methodNotFound, extraMessage: "\(jsonRpc.method)").jsonString()))
                return
            }

            executor?.eventSink = eventSink
            executor?.execute { executeResult in
                self.isProcessing = false
                switch executeResult {
                case let .success(resultJson):
                    result(resultJson)
                    break
                case let .failure(e):
                    if case let FVJsonRpcExecutorError.fail(type, detailMessage) = e {
                        result(FlutterError(code: String(type.code), message: type.message, details: FVJsonRpcError(id: jsonRpc.id, errorType: type, extraMessage: detailMessage).jsonString()))
                        self.errorFinishNFC()
                    } else {
                        result(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: jsonRpc.id, errorType: FVJsonRpcErrorType.internalError).jsonString()))
                        self.errorFinishNFC()
                    }
                }
            }
        } catch {
            self.isProcessing = false
            if error is DecodingError {
                result(FlutterError(code: String(FVJsonRpcErrorType.invalidParams.code), message: FVJsonRpcErrorType.invalidParams.message, details: FVJsonRpcError(id: jsonRpc.id, errorType: FVJsonRpcErrorType.invalidParams).jsonString()))
                self.errorFinishNFC()
            } else {
                result(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: jsonRpc.id, errorType: FVJsonRpcErrorType.internalError, extraMessage: "").jsonString()))
                self.errorFinishNFC()
            }
        }
    }

    func errorFinishNFC(errorMessage: String? = nil) {
        let message = errorMessage ?? errorAlertMessage
        session?.alertMessage = message
        session?.invalidate(errorMessage: message)
        session = nil
    }

    func finishNFC() {
        session?.alertMessage = self.finishAlertMessage
        session?.invalidate()
        session = nil
    }

    // MARK: NFCTagReaderSessionDelegate
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        self.isProcessing = true
        self.session = session
    }

    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        self.isProcessing = false
        guard let nfcError = error as? NFCReaderError else {
            self.result?(FlutterError(code: String(FVJsonRpcErrorType.unknownNFCError.code), message: error.localizedDescription, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: FVJsonRpcErrorType.unknownNFCError).jsonString()))
            return
        }
        var errorType = FVJsonRpcErrorType.unknownNFCError
        switch nfcError.code {
        case .ndefReaderSessionErrorTagNotWritable:
            errorType = .ndefReaderSessionErrorTagNotWritable
        case .ndefReaderSessionErrorTagSizeTooSmall:
            errorType = .ndefReaderSessionErrorTagSizeTooSmall
        case .ndefReaderSessionErrorTagUpdateFailure:
            errorType = .ndefReaderSessionErrorTagUpdateFailure
        case .ndefReaderSessionErrorZeroLengthMessage:
            errorType = .ndefReaderSessionErrorZeroLengthMessage
        case .readerErrorInvalidParameter:
            errorType = .readerErrorInvalidParameter
        case .readerErrorInvalidParameterLength:
            errorType = .readerErrorInvalidParameterLength
        case .readerErrorUnsupportedFeature:
            errorType = .readerErrorUnsupportedFeature
        case .readerErrorSecurityViolation:
            errorType = .readerErrorSecurityViolation
        case .readerErrorParameterOutOfBound:
            errorType = .readerErrorParameterOutOfBound
        case .readerErrorRadioDisabled:
            errorType = .readerErrorRadioDisabled
        case .readerTransceiveErrorTagConnectionLost:
            errorType = .readerTransceiveErrorTagConnectionLost
        case .readerTransceiveErrorRetryExceeded:
            errorType = .readerTransceiveErrorRetryExceeded
        case .readerTransceiveErrorTagResponseError:
            errorType = .readerTransceiveErrorTagResponseError
        case .readerTransceiveErrorSessionInvalidated:
            errorType = .readerTransceiveErrorSessionInvalidated
        case .readerTransceiveErrorTagNotConnected:
            errorType = .readerTransceiveErrorTagNotConnected
        case .readerTransceiveErrorPacketTooLong:
            errorType = .readerTransceiveErrorPacketTooLong
        case .readerSessionInvalidationErrorUserCanceled:
            errorType = .readerSessionInvalidationErrorUserCanceled
        case .readerSessionInvalidationErrorSessionTimeout:
            errorType = .readerSessionInvalidationErrorSessionTimeout
        case .readerSessionInvalidationErrorSessionTerminatedUnexpectedly:
            errorType = .readerSessionInvalidationErrorSessionTerminatedUnexpectedly
        case .readerSessionInvalidationErrorSystemIsBusy:
            errorType = .readerSessionInvalidationErrorSystemIsBusy
        case .readerSessionInvalidationErrorFirstNDEFTagRead:
            errorType = .readerSessionInvalidationErrorFirstNDEFTagRead
        case .tagCommandConfigurationErrorInvalidParameters:
            errorType = .tagCommandConfigurationErrorInvalidParameters
        @unknown default:
            errorType = .unknownNFCError
        }
        
        self.result?(FlutterError(code: String(errorType.code), message: errorType.message, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: errorType).jsonString()))
        errorFinishNFC()
    }

    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        self.isProcessing = true
        let tag = tags.first!
        session.connect(to: tag) { error in
            if let _ = error {
                self.isProcessing = false
                self.errorFinishNFC()
                self.eventSink?(FVFlutterSdk.walletConnectFailEvent)
                self.result?(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: FVJsonRpcErrorType.internalError).jsonString()))
                return
            }
            
            self.session?.alertMessage = self.readingAlertMessage

            guard let result = self.result else {
                self.isProcessing = false
                self.errorFinishNFC(errorMessage: "internal connection error.")
                self.eventSink?(FVFlutterSdk.walletConnectFailEvent)
                self.result?(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: FVJsonRpcErrorType.internalError).jsonString()))
                return
            }

            guard let rpc = self.jsonRpc else {
                self.isProcessing = false
                self.errorFinishNFC(errorMessage: "internal connection error.")
                self.eventSink?(FVFlutterSdk.walletConnectFailEvent)
                self.result?(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: FVJsonRpcErrorType.internalError).jsonString()))
                return
            }

            if case let .iso7816(isoTag) = tag {
                self.tag = isoTag
                self.handleJsonRpc(rpc, isFromTagSession: true, eventSink: self.eventSink, result: result)
            } else {
                self.isProcessing = false
                self.errorFinishNFC(errorMessage: "internal connection error.")
                self.eventSink?(FVFlutterSdk.walletConnectFailEvent)
                self.result?(FlutterError(code: String(FVJsonRpcErrorType.internalError.code), message: FVJsonRpcErrorType.internalError.message, details: FVJsonRpcError(id: self.jsonRpc?.id ?? "-1", errorType: FVJsonRpcErrorType.internalError).jsonString()))
            }
        }
    }
}
