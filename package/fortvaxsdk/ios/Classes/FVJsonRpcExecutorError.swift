//
//  FVJsonRpcExecutorError.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Foundation

enum FVJsonRpcExecutorError: Error {
    case fail(type: FVJsonRpcErrorType, detailMessage: String)
}
