//
//  FVBaseSdkJsonRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import CoreNFC
import Core
import Flutter

public class FVBaseSdkJsonRpcExecutor {

    let jsonRpcId: String
    let tag: NFCISO7816Tag?
    let cardId: String?
    let caPublicKey: String
    let pinCode: String?

    var eventSink: FlutterEventSink?

    init(jsonRpcId: String, tag: NFCISO7816Tag?, cardId: String?, caPublicKey: String, pinCode: String?) {
        self.jsonRpcId = jsonRpcId
        self.tag = tag
        self.cardId = cardId
        self.caPublicKey = caPublicKey
        self.pinCode = pinCode
    }

    func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {
        //需覆寫
    }
    
    func transformError(error:Error) -> FVJsonRpcErrorType {
        guard let domainError = error as? FVCommandDomainError else {
            return .undefined
        }
        
        switch domainError {
        case let .nfcCommandFail(_, detail):
            switch detail {
            case .noCard:
                return .hardwareError
            case .undefined:
                return .hardwareError
            case .noAnyCardResult:
                return .hardwareError
            case .dataVerify:
                return .hardwareError
            case .unidentifiedApplet:
                return .unidentifiedApplet
            case .cardCertFail:
                return .cardCertFail
            case .cardCertStateError:
                return .cardCertStateError
            case .passwordFormat:
                return .passwordFormat
            case .passwordBuild:
                return .passwordBuild
            case .passwordAuth:
                return .passwordAuth
            case .passwordStateError:
                return .passwordStateError
            case .buildSkAuth:
                return .buildSkAuth
            case .buildSkEpinCertRandom:
                return .buildSkEpinCertRandom
            case .buildSkNoInitPassword:
                return .buildSkNoInitPassword
            case .buildSkPinLimited:
                return .buildSkPinLimited
            case .buildSkStateError:
                return .buildSkStateError
            case .menmonicBuild:
                return .menmonicBuild
            case .menmonicStateError:
                return .menmonicStateError
            case .menmonicMac:
                return .menmonicMac
            case .createWalletBuild:
                return .createWalletBuild
            case .createWalletStateError:
                return .createWalletStateError
            case .createWalletMac:
                return .createWalletMac
            case .recoveryWalletBuild:
                return .recoveryWalletBuild
            case .recoveryWalletStateError:
                return .recoveryWalletStateError
            case .recoveryWalletMac:
                return .recoveryWalletMac
            case .retrievePublickState:
                return .retrievePublicState
            case .retrievePublickAuth:
                return .retrievePublicAuth
            case .retrievePublickIndex:
                return .retrievePublicIndex
            case .retrievePublickWallet:
                return .retrievePublicWallet
            case .txSignAuth:
                return .txSignAuth
            case .txSignState:
                return .txSignState
            case .txSignNoWallet:
                return .txSignNoWallet
            case .changePasswordModified:
                return .changePasswordModified
            case .changePasswordStateError:
                return .changePasswordStateError
            case .changePasswordAuth:
                return .changePasswordAuth
            case .changePasswordNotInit:
                return .changePasswordNotInit
            case .newBlockchainAlgError:
                return .newBlockchainAlgError
            case .newBlockchainAuthError:
                return .newBlockchainAuthError
            case .newBlockchainCommandNotSupport:
                return .newBlockchainCommandNotSupport
            case .newBlockchainCreateFail:
                return .newBlockchainCreateFail
            case .newBlockchainNoWallet:
                return .newBlockchainNoWallet
            case .newBlockchainWalletLimited:
                return .newBlockchainWalletLimited
            case .newBlockchainStateError:
                return .newBlockchainStateError
            case .walletInfoAuthError:
                return .walletInfoAuthError
            case .walletInfoStateError:
                return .walletInfoStateError
            @unknown default:
                return .hardwareError
            }
        case .dataEmpty:
            return .internalError
        case .cardIncorrect:
            return .cardIncorrect
        case .cla:
            return .cla
        case .ins:
            return .ins
        case .p1p2:
            return .p1p2
        case .lcle:
            return .lcle
        case .cardLocked:
            return .cardLocked
        case .tagLoss:
            return .tagLoss
        case .caFail:
            return .caFail
        case .invalidKeyCount:
            return .internalError
        case .undefined:
            return .undefined
        @unknown default:
            return .hardwareError
        }
    }
}
