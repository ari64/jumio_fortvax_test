//
//  FVReadUtxoModelKeyRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/11.
//

import Core
import CoreNFC

class FVReadUtxoModelKeyRpcExecutor: FVBaseSdkJsonRpcExecutor {

    let type: Int

    init(type: Int, jsonRpcId: String, tag: NFCISO7816Tag?, cardId: String?, caPublicKey: String, pinCode: String?) {
        self.type = type
        super.init(jsonRpcId: jsonRpcId, tag: tag, cardId: cardId, caPublicKey: caPublicKey, pinCode: pinCode)
    }

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        guard let cId = self.cardId else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "cid null")))
            return
        }

        guard let pin = self.pinCode else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "pin null")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.setPinCode(pinCode: pin)
        fortvaxWallet.assignNFCTag(tag: tag)

        switch type {
        case 2, 6:
            readUTXOKey(coindCode: type.blockchainTypeFromRpc().coinCode(), resultHandler: resultHandler)
        case 12:
            fortvaxWallet.readWalletInfo(bindingId: cId) { [weak self] result in
                guard let sl = self else {
                    return
                }

                switch result {
                case let .walletDidReadInfo(info):
                    let dogeInfo = info.first { wf in
                        wf.coinPath == .doge
                    }

                    guard let dogeChain = dogeInfo else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                        return
                    }

                    sl.readUTXOKey(coindCode: UInt8(dogeChain.coinCode), resultHandler: resultHandler)

                case .walletExecuting:
                    sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                case let .walletExecuteError(er):
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))

                default:
                    break
                }
            }
        default:
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "type invalid")))
        }
    }

    private func readUTXOKey(coindCode: UInt8, resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {
        guard let cId = self.cardId else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "cid null")))
            return
        }

        fortvaxWallet.readUTXOPublicKey(bindingId: cId, coinCode: coindCode, startIndex: 0, count: 2) { [weak self] result in
            guard let sl = self else {
                return
            }

            switch result {
            case let .walletDidReadUTXOPublicKey(utxoPublicKeys):
                guard let key = utxoPublicKeys.first, let success = FVJsonRpcResultMaker.makeReadUtxoModelKey(id: sl.jsonRpcId, publicKey: key) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "read utxo fail")))
                    return
                }
                resultHandler(.success(success))

            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

            case let .walletExecuteError(er):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))

            default:
                break
            }
        }
    }
}
