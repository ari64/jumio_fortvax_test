import Flutter
import UIKit
import Core
import CoreNFC

public class FortVaxSdkPlugin: NSObject, FlutterPlugin, FlutterStreamHandler {

    private var sdk: FVFlutterSdk? = nil
    private var sdkWithNoNFC = FVFlutterSdk()

    var eventSink: FlutterEventSink?

    private var scanAlertMessage = ""
    private var errorMessage = ""
    private var finishMessage = ""
    private var readingMessage = ""

    public static func register(with registrar: FlutterPluginRegistrar) {

        let channel = FlutterMethodChannel(name: "fortvax_sdk", binaryMessenger: registrar.messenger())
        let instance = FortVaxSdkPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)

        let eventChannel = FlutterEventChannel(name: "fortvax_sdk.event", binaryMessenger: registrar.messenger())
        eventChannel.setStreamHandler(instance)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        FVSDKCore.shared.initCore(isTestNet: false, apiDomain: "")

        if call.method != "callJsonRpc" {
            result(FlutterError(code: String(FVJsonRpcErrorType.invalidRequest.code), message: FVJsonRpcErrorType.invalidRequest.message, details: ""))
            return
        }


        guard let args = call.arguments as? [String: Any], let jsonRpcString = args["jsonRpcData"] as? String else {
            result(FlutterError(code: String(FVJsonRpcErrorType.invalidRequest.code), message: FVJsonRpcErrorType.invalidRequest.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.invalidRequest, extraMessage: "missing argument : jsonRpcData").jsonString()))
            return
        }

        let jsonDecoder = JSONDecoder()

        //jsonRpc -> decode 後結果 , jsonRpcDictionary -> 原始資料
        guard let jsonRpcStringToData = jsonRpcString.data(using: .utf8), let jsonRpc = try? jsonDecoder.decode(FVJsonRpc.self, from: jsonRpcStringToData), let jsonRpcDictionary = try? JSONSerialization.jsonObject(with: jsonRpcStringToData) as? [String: Any] else {
            result(FlutterError(code: String(FVJsonRpcErrorType.parserError.code), message: FVJsonRpcErrorType.parserError.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.parserError, extraMessage: "invalid json rpc data").jsonString()))
            return
        }


        guard let params = jsonRpcDictionary["params"], let paramsRawJson = try? JSONSerialization.data(withJSONObject: params), let paramsRawJsonString = String(bytes: paramsRawJson, encoding: .utf8) else {
            result(FlutterError(code: String(FVJsonRpcErrorType.parserError.code), message: FVJsonRpcErrorType.parserError.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.parserError, extraMessage: "invalid json rpc data").jsonString()))
            return
        }

        jsonRpc.params = paramsRawJsonString

        switch jsonRpc.method {
        case FVJsonRpcMethod.readCardStatus.rawValue, FVJsonRpcMethod.createWallet.rawValue, FVJsonRpcMethod.resetWallet.rawValue, FVJsonRpcMethod.restoreWallet.rawValue, FVJsonRpcMethod.readAccountPublicKey.rawValue, FVJsonRpcMethod.readUtxoPublicKey.rawValue, FVJsonRpcMethod.newBlockchain.rawValue, FVJsonRpcMethod.sign.rawValue:
            if sdk == nil {
                sdk = FVFlutterSdk()
                sdk?.scanAlertMessage = self.scanAlertMessage
                sdk?.errorAlertMessage = self.errorMessage
                sdk?.finishAlertMessage = self.finishMessage
                sdk?.readingAlertMessage = self.readingMessage
                sdk?.starScanAndHandleJsonRpc(jsonRpc, eventSink: eventSink, result: result)
            } else {
                sdk?.handleJsonRpc(jsonRpc, eventSink: eventSink, result: result)
            }
        case FVJsonRpcMethod.setScanAlertMessage.rawValue:

            guard let p = try? jsonDecoder.decode(FVJsonParamSetScanAlertMessage.self, from: paramsRawJsonString.data(using: .utf8) ?? Data()) else {
                result(FlutterError(code: String(FVJsonRpcErrorType.parserError.code), message: FVJsonRpcErrorType.invalidParams.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.invalidParams).jsonString()))
                return
            }

            self.scanAlertMessage = p.scanAlertMessage
            self.errorMessage = p.errorMessage
            self.finishMessage = p.finishMessage
            self.readingMessage = p.readingMessage

        case FVJsonRpcMethod.settleUtxoTransaction.rawValue:
            sdkWithNoNFC.handleJsonRpc(jsonRpc, eventSink: nil, result: result)
            break

        case FVJsonRpcMethod.stopScan.rawValue:
            sdk?.finishNFC()
            sdk = nil
            result(FVJsonRpcResultMaker.makeSuccessMessage(id: jsonRpc.id))
        default:
            result(FlutterError(code: String(FVJsonRpcErrorType.methodNotFound.code), message: FVJsonRpcErrorType.methodNotFound.message, details: FVJsonRpcError(id: "-1", errorType: FVJsonRpcErrorType.methodNotFound, extraMessage: "\(jsonRpc.method)").jsonString()))
            break
        }
    }

    public func onListen(withArguments arguments: Any?, eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink
        // 開始監聽事件
        return nil
    }

    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        // 停止監聽事件
        self.eventSink = nil
        return nil
    }
}
