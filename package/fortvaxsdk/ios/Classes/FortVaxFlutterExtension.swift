//
//  FortVaxFlutterExtension.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Core

extension Int {
    func blockchainTypeFromRpc() -> FVCardBlockChainType {
        switch self {
        case 1: return .eth
        case 2: return .btc
        case 3: return .tron
        case 4: return .usdt
        case 5: return .bnb
        case 6: return .ltc
        case 7: return .xrp
        case 8: return .eos
        case 9: return .bch
        case 10: return .bcd
        case 11: return .bchsv
        case 12: return .doge
        default: break
        }

        fatalError()
    }
}

extension FVCardBlockChainType {
    func toRpcType() -> Int {
        switch self {
        case .eth:
            return 1
        case .btc:
            return 2
        case .tron:
            return 3
        case .usdt:
            return 4
        case .bnb:
            return 5
        case .ltc:
            return 6
        case .xrp:
            return 7
        case .eos:
            return 8
        case .bch:
            return 9
        case .bcd:
            return 10
        case .bchsv:
            return 11
        case .doge:
            return 12
        @unknown default:
            fatalError()
        }
    }
}
