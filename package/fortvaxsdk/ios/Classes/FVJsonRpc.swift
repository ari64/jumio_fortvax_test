//
//  FVJsonRpc.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/9.
//

import Core

public class FVJsonRpc: Decodable, Equatable {
    
    public let id: String
    public let method: String
    public let jsonrpc: String
    public var params: String?

    enum CodingKeys: CodingKey {
        case id
        case method
        case jsonrpc
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.method = try container.decode(String.self, forKey: .method)
        self.jsonrpc = try container.decode(String.self, forKey: .jsonrpc)
    }
    
    public static func == (lhs: FVJsonRpc, rhs: FVJsonRpc) -> Bool {
        return (lhs.id == rhs.id) && (lhs.method == rhs.method) && (lhs.jsonrpc == rhs.jsonrpc) && (lhs.params == rhs.params)
    }
}

public class FVJsonParamReadCardStatus: Decodable {
    public let caPublicKey: String
}

public class FVJsonParamResetWallet: Decodable {
    public let caPublicKey: String
}

public class FVJsonParamCreateWallet: Decodable {
    public let caPublicKey: String
    public let wordCount: Int
    public let createPassword: Bool
    public let cardId: String
    public let pinCode: String

}

public class FVJsonParamNewBlockchain: Decodable {
    public let caPublicKey: String
    public let types: [Int]
    public let pinCode: String
    public let cardId: String
}

public class FVJsonParamReadAccountModelKey: Decodable {
    public let caPublicKey: String
    public let supportedCryptoMainNet: [Int]
    public let cardId: String
    public let pinCode: String
}

public class FVJsonParamReadUtxoModelKey: Decodable {
    public let caPublicKey: String
    public let type: Int
    public let cardId: String
    public let pinCode: String
}

public class FVJsonParamRestoreWallet: Decodable {
    public let caPublicKey: String
    public let words: String
    public let createPassword: Bool
    public let cardId: String
    public let pinCode: String

}

public class FVJsonParamSettleUtxo: Decodable {
    public let type: Int
    public let feeRate: Int64
    public let fromPublicKey: String
    public let toAddress: String
    public let candidateUtxo: [CandidateUtxo]
    public let sendAmount: UInt64
}

public class CandidateUtxo: Decodable {
    public let tx_hash: String
    public let tx_input_n: Int64
    public let tx_output_n: Int64
    public let value: Int64
    public let script: String
    public let address: String
    public let confirmations: Int64

    func transformBtcUtxo() -> FVBtcUtxo {
        let btcUtxo = FVBtcUtxo()
        btcUtxo.address = address
        btcUtxo.script = script
        btcUtxo.tx_hash = tx_hash
        btcUtxo.confirmations = confirmations
        btcUtxo.tx_input_n = tx_input_n
        btcUtxo.tx_output_n = tx_output_n
        btcUtxo.value = value
        return btcUtxo
    }
}

public class FVJsonParamSetScanAlertMessage: Decodable {
    public let scanAlertMessage: String
    public let errorMessage: String
    public let finishMessage: String
    public let readingMessage: String
}

// MARK: Sign
public class FVJsonParamSignType: Decodable {
    public let caPublicKey: String
    public let cardId: String
    public let pinCode: String
    public let type: Int
    public let transactionType: String
    public let evmType: Int?
}

public class FVJsonParamSign<T : Decodable>: Decodable {
    public let caPublicKey: String
    public let cardId: String
    public let pinCode: String
    public let type: Int
    public let transactionType: String
    public let evmType: Int?
    public let data: T
}

public class FVEthereumLegacySignData: Decodable {
    let nonce: String
    let gasPrice: String
    let gasLimit: String
    let amount: String
    let toAddress: String
    let chainId: Int
    let publicKey: String
}

public class FVEthereumLegacyErc20SignData: Decodable {
    let nonce: String
    let gasPrice: String
    let gasLimit: String
    let amount: String
    let decimal: Int
    let toAddress: String
    let chainId: Int
    let publicKey: String
    let contractAddress: String
}

public class FVEthereum1559SignData: Decodable {
    let nonce: String
    let maxFeePerGas: String
    let maxPriorityFeePerGas: String
    let gasLimit: String
    let amount: String
    let toAddress: String
    let chainId: Int
    let publicKey: String
}

public class FVEthereum1559Erc20SignData: Decodable {
    let nonce: String
    let maxFeePerGas: String
    let maxPriorityFeePerGas: String
    let gasLimit: String
    let amount: String
    let decimal: Int
    let toAddress: String
    let chainId: Int
    let publicKey: String
    let contractAddress: String
}

public class FVTronSignData: Decodable {
    let fromAddress: String
    let toAddress: String
    let amount: Int64
    let publicKey: String
    let nowBlock: FVTronFlutterBlock

}

public class FVTronTrc20SignData: Decodable {
    let fromAddress: String
    let toAddress: String
    let amount: Int64
    let publicKey: String
    let decimal: Int
    let nowBlock: FVTronFlutterBlock
    let contractAddress: String
}

public class FVTronFlutterBlockRawData: Decodable {
    public var number: Int
    public var txTrieRoot: String
    public var witness_address: String
    public var parentHash: String
    public var version: Int
    public var timestamp: Int
}

public class FVTronFlutterBlockHeader: Decodable {
    public var raw_data: FVTronFlutterBlockRawData
    public var witness_signature: String
}

public class FVTronFlutterBlock: Decodable {
    public var blockID: String
    public var block_header: FVTronFlutterBlockHeader
}

public class FVUtxoSignData: Decodable {
    public let feeRate: Int64
    public let fromPublicKey: FVWalletPublicKey
    public let toAddress: String
    public let candidateUtxo: [CandidateUtxo]
    public let sendAmount: UInt64
    public let coinCode: String?

    enum CodingKeys: CodingKey {
        case feeRate
        case fromPublicKey
        case toAddress
        case candidateUtxo
        case sendAmount
        case coinCode
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.feeRate = try container.decode(Int64.self, forKey: .feeRate)
        self.fromPublicKey = try container.decode(FVWalletPublicKey.self, forKey: .fromPublicKey)
        self.toAddress = try container.decode(String.self, forKey: .toAddress)
        self.candidateUtxo = try container.decode([CandidateUtxo].self, forKey: .candidateUtxo)
        self.sendAmount = try container.decode(UInt64.self, forKey: .sendAmount)
        self.coinCode = try container.decodeIfPresent(String.self, forKey: .coinCode)
    }
}

public class FVWalletPublicKey: Decodable {
    let type: Int
    let compressedKey: String
    let uncompressedKey: String
    let change: Bool
    let addressIndex: Int
}
