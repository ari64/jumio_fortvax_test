//
//  FVReadAccountModelKeyRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/11.
//

import Core
import CoreNFC

class FVReadAccountModelKeyRpcExecutor: FVBaseSdkJsonRpcExecutor {

    let supportMainNet: [Int]

    init(supportMainNet: [Int], jsonRpcId: String, tag: NFCISO7816Tag?, cardId: String?, caPublicKey: String, pinCode: String?) {
        self.supportMainNet = supportMainNet
        super.init(jsonRpcId: jsonRpcId, tag: tag, cardId: cardId, caPublicKey: caPublicKey, pinCode: pinCode)
    }

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        guard let cId = self.cardId else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "cid null")))
            return
        }

        guard let pin = self.pinCode else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "pin null")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.setPinCode(pinCode: pin)
        fortvaxWallet.assignNFCTag(tag: tag)

        let noneUtxoCryptos = supportMainNet.map { net in
            net.blockchainTypeFromRpc()
        }

        fortvaxWallet.readAccountPublicKey(bindingId: cId, supportedCryptoCurrency: noneUtxoCryptos) { [weak self] result in
            guard let sl = self else {
                return
            }

            switch result {
            case let .walletDidReadAccountPublicKey(publicKeys):
                guard let success = FVJsonRpcResultMaker.makeReadAccountModelKey(id: sl.jsonRpcId, publicKeys: publicKeys) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                    return
                }
                resultHandler(.success(success))

            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

            case let .walletExecuteError(er):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))

            default:
                break
            }
        }
    }
}
