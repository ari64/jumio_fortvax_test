//
//  FVRestoreWalletRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Core
import CoreNFC

public class FVRestoreWalletRpcExecutor: FVBaseSdkJsonRpcExecutor {

    let words: String
    let createPassword: Bool

    init(words: String, createPassword: Bool, jsonRpcId: String, tag: NFCISO7816Tag?, cardId: String?, caPublicKey: String, pinCode: String?) {
        self.words = words
        self.createPassword = createPassword
        super.init(jsonRpcId: jsonRpcId, tag: tag, cardId: cardId, caPublicKey: caPublicKey, pinCode: pinCode)
    }

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        guard let cId = self.cardId else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "cid null")))
            return
        }

        guard let pin = self.pinCode else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "pin null")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.setPinCode(pinCode: pin)
        fortvaxWallet.assignNFCTag(tag: tag)

        fortvaxWallet.restoreWallet(words: words.components(separatedBy: " "), bindingId: cId, createPassword: createPassword) { [weak self] result in
            guard let sl = self else {
                return
            }
            switch result {
            case let .walletDidRestoreChain(accountPublicKeys):
                guard let success = FVJsonRpcResultMaker.makeRestoreWallet(id: sl.jsonRpcId, accountPublicKeys: accountPublicKeys) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                    return
                }
                resultHandler(.success(success))

            case .walletDidSetUpPinCode:
                sl.eventSink?(FVFlutterSdk.walletPasswordCreatedEvent)
            case let .walletExecuteError(er):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))
            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)
            default:
                break
            }
        }
    }
}
