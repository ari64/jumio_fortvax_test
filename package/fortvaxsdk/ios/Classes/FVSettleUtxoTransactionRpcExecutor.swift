//
//  FVSettleUtxoTransactionRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/11.
//

import Core

class FVSettleUtxoTransactionRpcExecutor: FVBaseSdkJsonRpcExecutor {


    let params: FVJsonParamSettleUtxo

    init(jsonRpcId: String, params: FVJsonParamSettleUtxo) {
        self.params = params
        super.init(jsonRpcId: jsonRpcId, tag: nil, cardId: nil, caPublicKey: "", pinCode: nil)
    }

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        do {
            switch params.type {
            case 2:
                //BTC
                let rx = FVBtcRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch BTCUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2shP2wpkh:
                    try rx.addNestedSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2wpkh:
                    try rx.addNativeSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .undefined:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                @unknown default:
                    break
                }

                do {
                    let estimateFee = try rx.estimateNetworkFee()
                    guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                        return
                    }
                    resultHandler(.success(success))
                } catch {
                    if let e = error as? FVBlockChainError {
                        switch e {
                        case .btcUtxoNotCover(let estimateFee):
                            guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                                return
                            }
                            resultHandler(.success(success))
                        default:
                            resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                        }
                    } else {
                        resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                    }
                }

            case 6:
                //LTC
                let rx = FVLtcRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch LTCUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2sh_p2wpkh:
                    try rx.addNestedSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2wpkh:
                    try rx.addNativeSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                default:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                }

                do {
                    let estimateFee = try rx.estimateNetworkFee()
                    guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                        return
                    }
                    resultHandler(.success(success))
                } catch {
                    if let e = error as? FVBlockChainError {
                        switch e {
                        case .ltcUtxoNotCover(let estimateFee):
                            guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                                return
                            }
                            resultHandler(.success(success))
                        default:
                            resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                        }
                    } else {
                        resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                    }
                }
                break
            case 12:
                //DOGE
                let rx = FVDogeRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch DogeUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                default:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                }

                do {
                    let estimateFee = try rx.estimateNetworkFee()
                    guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                        return
                    }
                    resultHandler(.success(success))
                } catch {
                    if let e = error as? FVBlockChainError {
                        switch e {
                        case .dogeUtxoNotCover(let estimateFee):
                            guard let success = FVJsonRpcResultMaker.makeSettleUtxoTransaction(id: jsonRpcId, estimateFee: estimateFee) else {
                                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                                return
                            }
                            resultHandler(.success(success))
                        default:
                            resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                        }
                    } else {
                        resultHandler(.failure(.fail(type: .invalidParams, detailMessage: "estimateNetworkFee error")))
                    }
                }
                break
            default:
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "type invalid")))

            }
        } catch {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "\(error)")))
        }
    }
}
