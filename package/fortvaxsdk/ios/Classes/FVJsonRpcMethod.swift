//
//  FVJsonRpcMethod.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/6/28.
//

import Foundation

public enum FVJsonRpcMethod: String {
    case readCardStatus = "read_card_status"
    case createWallet = "create_wallet"
    case restoreWallet = "restore_wallet"
    case readAccountPublicKey = "read_account_publicKey"
    case readUtxoPublicKey = "read_utxo_publicKey"
    case resetWallet = "reset_wallet"
    case newBlockchain = "new_blockchain"
    case sign = "sign"
    case setScanAlertMessage = "set_scan_alert_message"
    case stopScan = "stop_scan"
    case settleUtxoTransaction = "settle_utxo_transaction"
}
