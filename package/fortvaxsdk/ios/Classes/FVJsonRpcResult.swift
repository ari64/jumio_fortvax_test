//
//  FVJsonRpcResult.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/9.
//

import Core

//頂層
class FVJsonRpcResult<T:Encodable>: Encodable {
    let id: String
    let jsonrpc: String
    let result: T?

    init(id: String, jsonrpc: String, result: T?) {
        self.id = id
        self.jsonrpc = jsonrpc
        self.result = result
    }
}


// MARK: 以下都是 result
class FVJsonRpcResultMessage: Encodable {
    let message: String

    init(message: String) {
        self.message = message
    }
}

class FVJsonRpcResultCardStatus: Encodable {
    let caIndex: Int
    let capVer: String
    let uid: String
    let cardStatus: Int

    init(caIndex: Int, capVer: String, uid: String, cardStatus: Int) {
        self.caIndex = caIndex
        self.capVer = capVer
        self.uid = uid
        self.cardStatus = cardStatus
    }
}

class FVJsonRpcResultCreateWallet: Encodable {
    let seedWords: String
    let accountModelKeys: [FVJsonRpcResultWalletKey]

    init(seedWords: String, accountModelKeys: [FVJsonRpcResultWalletKey]) {
        self.seedWords = seedWords
        self.accountModelKeys = accountModelKeys
    }
}

class FVJsonRpcResultRestoreWallet: Encodable {
    let accountModelKeys: [FVJsonRpcResultWalletKey]

    init(accountModelKeys: [FVJsonRpcResultWalletKey]) {
        self.accountModelKeys = accountModelKeys
    }
}

class FVJsonRpcResultReadUTXOModelKey: Encodable {
    let utxoModelKey: FVJsonRpcResultWalletKey

    init(key: FVJsonRpcResultWalletKey) {
        self.utxoModelKey = key
    }
}

class FVJsonRpcResultNewBlockchain: Encodable {
    let blockchainPlus: [FVJsonRpcResultNewBlockchainItem]

    init(blockchainPlus: [FVJsonRpcResultNewBlockchainItem]) {
        self.blockchainPlus = blockchainPlus
    }
}

class FVJsonRpcResultNewBlockchainItem: Encodable {
    let type: Int
    let coinCode: String

    init(type: Int, coinCode: String) {
        self.type = type
        self.coinCode = coinCode
    }
}

class FVJsonRpcResultSettleUtxo: Encodable {
    let estimateFee: UInt64

    init(estimateFee: UInt64) {
        self.estimateFee = estimateFee
    }
}

class FVJsonRpcResultSign: Encodable {
    let sign: String

    init(sign: String) {
        self.sign = sign
    }
}

class FVJsonRpcResultWalletKey: Encodable {
    let type: Int
    let compressedKey: String
    let uncompressedKey: String
    let keyToAddress: String
    let change: Bool
    let addressIndex: Int

    init(type: Int, compressedKey: String, uncompressedKey: String, keyToAddress: String, change: Bool, addressIndex: Int) {
        self.type = type
        self.compressedKey = compressedKey
        self.uncompressedKey = uncompressedKey
        self.keyToAddress = keyToAddress
        self.change = change
        self.addressIndex = addressIndex
    }
}

extension FVJsonRpcResult {
    func jsonString() -> String? {
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(self) else {
            return nil
        }

        return String(bytes: data, encoding: .utf8)
    }
}

// MARK: 各種 Result 製造方法
class FVJsonRpcResultMaker {
    static func makeSuccessMessage(id: String) -> String? {
        return FVJsonRpcResult<FVJsonRpcResultMessage>(id: id, jsonrpc: "2.0", result: FVJsonRpcResultMessage(message: "success")).jsonString()
    }

    static func makeCardStatus(id: String, cert: FVCert) -> String? {
        let hexCardID = cert.uid.toHexString().lowercased()
        let cardVer = cert.capVer
        let caIndex = cert.caIndex

        var cardStatus = 0

        if cert.isEpinCreated() && !cert.isWalletHasBeenBuild() {
            cardStatus = 1
        } else if cert.isEpinCreated() && cert.isWalletHasBeenBuild() {
            cardStatus = 2
        } else {
            cardStatus = 0
        }

        let c = FVJsonRpcResultCardStatus(caIndex: caIndex, capVer: cardVer, uid: hexCardID, cardStatus: cardStatus)
        return FVJsonRpcResult<FVJsonRpcResultCardStatus>(id: id, jsonrpc: "2.0", result: c).jsonString()
    }

    static func makeCreateWallet(id: String, seedWords: [String], accountPublicKeys: [FVECPublicKey], btcUtxoPublicKeys: [FVECPublicKey]) -> String? {

        //sdk parserAddress 還沒有完全 全部支援 11 條鏈地址，所以用了會噴錯
        //錯誤就略過

        var accountKeys = [FVJsonRpcResultWalletKey]()

        for key in accountPublicKeys {
            do {
                let item = FVJsonRpcResultWalletKey(type: key.type.toRpcType(), compressedKey: key.compressedKey, uncompressedKey: key.uncompressedKey, keyToAddress: try key.parserAddress(), change: key.change, addressIndex: key.addressIndex)
                accountKeys.append(item)
            } catch {

            }
        }

        let wallet = FVJsonRpcResultCreateWallet(seedWords: seedWords.joined(separator: " "), accountModelKeys: accountKeys)

        return FVJsonRpcResult<FVJsonRpcResultCreateWallet>(id: id, jsonrpc: "2.0", result: wallet).jsonString()
    }

    static func makeRestoreWallet(id: String, accountPublicKeys: [FVECPublicKey]) -> String? {
        var accountKeys = [FVJsonRpcResultWalletKey]()

        for key in accountPublicKeys {
            do {
                let item = FVJsonRpcResultWalletKey(type: key.type.toRpcType(), compressedKey: key.compressedKey, uncompressedKey: key.uncompressedKey, keyToAddress: try key.parserAddress(), change: key.change, addressIndex: key.addressIndex)
                accountKeys.append(item)
            } catch {

            }
        }

        let wallet = FVJsonRpcResultRestoreWallet(accountModelKeys: accountKeys)

        return FVJsonRpcResult<FVJsonRpcResultRestoreWallet>(id: id, jsonrpc: "2.0", result: wallet).jsonString()
    }

    static func makeReadUtxoModelKey(id: String, publicKey: FVECPublicKey) -> String? {
        do {
            let item = FVJsonRpcResultWalletKey(type: publicKey.type.toRpcType(), compressedKey: publicKey.compressedKey, uncompressedKey: publicKey.uncompressedKey, keyToAddress: try publicKey.parserAddress(), change: publicKey.change, addressIndex: publicKey.addressIndex)
            let utxoItem = FVJsonRpcResultReadUTXOModelKey(key: item)
            return FVJsonRpcResult<FVJsonRpcResultReadUTXOModelKey>(id: id, jsonrpc: "2.0", result: utxoItem).jsonString()
        } catch {
            return nil
        }
    }

    static func makeReadAccountModelKey(id: String, publicKeys: [FVECPublicKey]) -> String? {
        var accountKeys = [FVJsonRpcResultWalletKey]()

        for key in publicKeys {
            do {
                let item = FVJsonRpcResultWalletKey(type: key.type.toRpcType(), compressedKey: key.compressedKey, uncompressedKey: key.uncompressedKey, keyToAddress: try key.parserAddress(), change: key.change, addressIndex: key.addressIndex)
                accountKeys.append(item)
            } catch {

            }
        }

        let wallet = FVJsonRpcResultRestoreWallet(accountModelKeys: accountKeys)

        return FVJsonRpcResult<FVJsonRpcResultRestoreWallet>(id: id, jsonrpc: "2.0", result: wallet).jsonString()
    }

    static func makeNewBlockchain(id: String, plus: [FVBlockchainPlus]) -> String? {
        var plusData = [FVJsonRpcResultNewBlockchainItem]()

        for p in plus {
            let item = FVJsonRpcResultNewBlockchainItem(type: p.blockchain.toRpcType(), coinCode: "0x\(p.coinCode.uppercased())")
            plusData.append(item)
        }

        let result = FVJsonRpcResultNewBlockchain(blockchainPlus: plusData)
        return FVJsonRpcResult<FVJsonRpcResultNewBlockchain>(id: id, jsonrpc: "2.0", result: result).jsonString()
    }

    static func makeSettleUtxoTransaction(id: String, estimateFee: UInt64) -> String? {
        let result = FVJsonRpcResultSettleUtxo(estimateFee: estimateFee)
        return FVJsonRpcResult<FVJsonRpcResultSettleUtxo>(id: id, jsonrpc: "2.0", result: result).jsonString()
    }

    static func makeSign(id: String, signTx: String) -> String? {
        let result = FVJsonRpcResultSign(sign: signTx)
        return FVJsonRpcResult<FVJsonRpcResultSign>(id: id, jsonrpc: "2.0", result: result).jsonString()
    }
}
