//
//  FVFlutterCrypto.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Foundation
import Security

class FVFlutterCrypto {
    // 65537
    private static let exponent: [UInt8] = [0x01, 0x00, 0x01]

    public static func derPublicKey(rsaKeyRawData: [UInt8]) -> SecKey? {
        // insert your modulus and exponent here
        let modulusData = rsaKeyRawData
        var modulus = [UInt8](modulusData)

        let exponent: [UInt8] = exponent // encode the exponent as big-endian bytes

        // prefix with 0x00 to indicate that it is a non-negative number
        modulus.insert(0x00, at: 0)

        // encode the modulus and exponent as INTEGERs
        var modulusEncoded: [UInt8] = [0x02]
        modulusEncoded.append(contentsOf: lengthField(of: modulus))
        modulusEncoded.append(contentsOf: modulus)

        var exponentEncoded: [UInt8] = [0x02]
        exponentEncoded.append(contentsOf: lengthField(of: exponent))
        exponentEncoded.append(contentsOf: exponent)

        // combine these INTEGERs to a SEQUENCE
        var sequenceEncoded: [UInt8] = [0x30]
        sequenceEncoded.append(contentsOf: lengthField(of: modulusEncoded + exponentEncoded))
        sequenceEncoded.append(contentsOf: modulusEncoded + exponentEncoded)

        // all done, we now have the PKCS#1 key
        let keyData = Data(sequenceEncoded)

        // let's check it actully works...
        let attributes: [String: Any] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
            kSecAttrKeySizeInBits as String: modulus.count * 8,
        ]

        var error: Unmanaged<CFError>?
        let publicKey = SecKeyCreateWithData(keyData as CFData, attributes as CFDictionary, &error)
        return publicKey
    }

    public static func lengthField(of valueField: [UInt8]) -> [UInt8] {
        var count = valueField.count

        if count < 128 {
            return [UInt8(count)]
        }

        // The number of bytes needed to encode count.
        let lengthBytesCount = Int((log2(Double(count)) / 8) + 1)

        // The first byte in the length field encoding the number of remaining bytes.
        let firstLengthFieldByte = UInt8(128 + lengthBytesCount)

        var lengthField: [UInt8] = []
        for _ in 0 ..< lengthBytesCount {
            // Take the last 8 bits of count.
            let lengthByte = UInt8(count & 0xFF)
            // Add them to the length field.
            lengthField.insert(lengthByte, at: 0)
            // Delete the last 8 bits of count.
            count = count >> 8
        }

        // Include the first byte.
        lengthField.insert(firstLengthFieldByte, at: 0)

        return lengthField
    }
}
