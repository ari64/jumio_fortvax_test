//
//  FVReadCardStatusRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Core
import CoreNFC

public class FVReadCardStatusRpcExecutor: FVBaseSdkJsonRpcExecutor {

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.assignNFCTag(tag: tag)
        fortvaxWallet.readCardStatus { [weak self] result in
            guard let sl = self else {
                return
            }

            switch result {
            case let .walletDidReadCertStatus(cert):
                guard let success = FVJsonRpcResultMaker.makeCardStatus(id: sl.jsonRpcId, cert: cert) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                    return
                }
                resultHandler(.success(success))
            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)
                break
            case let .walletExecuteError(error):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))
                break
            default:
                break
            }
        }
    }
}
