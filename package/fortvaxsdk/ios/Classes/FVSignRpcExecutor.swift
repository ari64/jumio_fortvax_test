//
//  FVSignRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/12.
//

import Core
import CoreNFC

class FVSignRpcExecutor: FVBaseSdkJsonRpcExecutor {

    let signJsonParams: String
    private var fortvaxWallet = FortVaxWallet()

    init(signJsonParams: String, jsonRpcId: String, tag: NFCISO7816Tag?) {
        self.signJsonParams = signJsonParams
        super.init(jsonRpcId: jsonRpcId, tag: tag, cardId: nil, caPublicKey: "", pinCode: nil)
    }

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {
        let decoder = JSONDecoder()
        guard let data = signJsonParams.data(using: .utf8), let typeChecker = try? decoder.decode(FVJsonParamSignType.self, from: data) else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
            return
        }

        guard let caPbKeyData = typeChecker.caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        let cId = typeChecker.cardId
        let pin = typeChecker.pinCode

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.setPinCode(pinCode: pin)
        fortvaxWallet.assignNFCTag(tag: tag)

        let fungible = "fungible"
        let native = "native"

        if typeChecker.transactionType != fungible && typeChecker.transactionType != native {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign param transactionType invalid")))
            return
        }

        switch typeChecker.type.blockchainTypeFromRpc() {
        case .eth:
            var hasher: IFVCardHasher? = nil

            //eth , polygon
            if typeChecker.evmType == 1 || typeChecker.evmType == 2 {
                if typeChecker.transactionType == fungible {
                    guard let signParams = try? decoder.decode(FVJsonParamSign<FVEthereum1559Erc20SignData>.self, from: data) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                        return
                    }

                    //symbol 不是重點，隨意給一個值即可
                    hasher = FVErc20Eip1559Hasher(nonce: Int(signParams.data.nonce)!, gasLimit: Int(signParams.data.gasLimit)!, amount: Decimal(string: signParams.data.amount)!, toAddress: signParams.data.toAddress, publicKey: signParams.data.publicKey, symbol: "n", decimal: signParams.data.decimal, contractAddress: signParams.data.contractAddress, chainId: signParams.data.chainId, maxPriorityFeePerGas: BInt(signParams.data.maxPriorityFeePerGas)!, maxFeePerGas: BInt(signParams.data.maxFeePerGas)!)

                } else {
                    guard let signParams = try? decoder.decode(FVJsonParamSign<FVEthereum1559SignData>.self, from: data) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                        return
                    }

                    hasher = FVEthereum1559Hasher(nonce: Int(signParams.data.nonce)!, gasLimit: Int(signParams.data.gasLimit)!, amount: Decimal(string: signParams.data.amount)!, toAddress: signParams.data.toAddress, chainId: signParams.data.chainId, maxPriorityFeePerGas: BInt(signParams.data.maxPriorityFeePerGas)!, maxFeePerGas: BInt(signParams.data.maxFeePerGas)!, publicKey: signParams.data.publicKey)
                }
            } else if typeChecker.evmType == 3 {
                //bsc
                if typeChecker.transactionType == fungible {
                    guard let signParams = try? decoder.decode(FVJsonParamSign<FVEthereumLegacyErc20SignData>.self, from: data) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                        return
                    }

                    //symbol 不是重點，隨意給一個值即可
                    hasher = FVErc20Hasher(nonce: Int(signParams.data.nonce)!, gasPrice: Int(signParams.data.gasPrice)!, gasLimit: Int(signParams.data.gasLimit)!, amount: Decimal(string: signParams.data.amount)!, toAddress: signParams.data.toAddress, chainId: signParams.data.chainId, publicKey: signParams.data.publicKey, symbol: "n", decimal: signParams.data.decimal, contractAddress: signParams.data.contractAddress)
                } else {
                    guard let signParams = try? decoder.decode(FVJsonParamSign<FVEthereumLegacySignData>.self, from: data) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                        return
                    }

                    hasher = FVEthereumHasher(nonce: Int(signParams.data.nonce)!, gasPrice: Int(signParams.data.gasPrice)!, gasLimit: Int(signParams.data.gasLimit)!, amount: Decimal(string: signParams.data.amount)!, toAddress: signParams.data.toAddress, chainId: signParams.data.chainId, publicKey: signParams.data.publicKey)
                }
            } else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "evm type invalid")))
                return
            }

            guard let h = hasher else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "hash invalid")))
                return
            }

            fortvaxWallet.sign(bindingId: cId, hasher: h) { [weak self] result in
                guard let sl = self else {
                    return
                }

                switch result {
                case let .walletDidSign(_, _, _, errorMessage):
                    if errorMessage != nil {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                        return
                    }

                case let .walletDidSignAllCompleted(vrs, _):
                    guard let sig = vrs.first else {

                        return
                    }

                    var signTx = ""

                    switch h {
                    case let hasher as FVErc20Eip1559Hasher:
                        signTx = "0x02" + hasher.signRawTransaction(vrs: sig).toHexString()
                    case let hasher as FVEthereum1559Hasher:
                        signTx = "0x02" + hasher.signRawTransaction(vrs: sig).toHexString()
                    case let hasher as FVErc20Hasher:
                        signTx = "0x" + hasher.signRawTransaction(vrs: sig).toHexString()
                    case let hasher as FVEthereumHasher:
                        signTx = "0x" + hasher.signRawTransaction(vrs: sig).toHexString()
                    default:
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                    }

                    guard let success = FVJsonRpcResultMaker.makeSign(id: sl.jsonRpcId, signTx: signTx) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                        return
                    }

                    resultHandler(.success(success))

                case .walletExecuting:
                    sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                case let .walletExecuteError(error):
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))

                default:
                    break
                }
            }

            break
        case .tron:
            var hasher: IFVCardHasher? = nil
            if typeChecker.transactionType == native {
                guard let signParams = try? decoder.decode(FVJsonParamSign<FVTronSignData>.self, from: data) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                    return
                }

                let tronBlock = FVTronBlock()
                tronBlock.block_header = FVTronBlockHeader()
                tronBlock.block_header?.raw_data = FVTronBlockRawData()

                tronBlock.blockID = signParams.data.nowBlock.blockID
                tronBlock.block_header?.raw_data?.number = signParams.data.nowBlock.block_header.raw_data.number
                tronBlock.block_header?.raw_data?.timestamp = signParams.data.nowBlock.block_header.raw_data.timestamp
                tronBlock.block_header?.raw_data?.version = signParams.data.nowBlock.block_header.raw_data.version
                tronBlock.block_header?.raw_data?.parentHash = signParams.data.nowBlock.block_header.raw_data.parentHash
                tronBlock.block_header?.raw_data?.txTrieRoot = signParams.data.nowBlock.block_header.raw_data.txTrieRoot
                tronBlock.block_header?.raw_data?.witness_address = signParams.data.nowBlock.block_header.raw_data.witness_address

                hasher = FVTronHasher(fromAddress: signParams.data.fromAddress, toAddress: signParams.data.toAddress, amount: signParams.data.amount, nowBlock: tronBlock, publicKey: signParams.data.publicKey)
            } else {
                guard let signParams = try? decoder.decode(FVJsonParamSign<FVTronTrc20SignData>.self, from: data) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                    return
                }

                let tronBlock = FVTronBlock()
                tronBlock.block_header = FVTronBlockHeader()
                tronBlock.block_header?.raw_data = FVTronBlockRawData()

                tronBlock.blockID = signParams.data.nowBlock.blockID
                tronBlock.block_header?.raw_data?.number = signParams.data.nowBlock.block_header.raw_data.number
                tronBlock.block_header?.raw_data?.timestamp = signParams.data.nowBlock.block_header.raw_data.timestamp
                tronBlock.block_header?.raw_data?.version = signParams.data.nowBlock.block_header.raw_data.version
                tronBlock.block_header?.raw_data?.parentHash = signParams.data.nowBlock.block_header.raw_data.parentHash
                tronBlock.block_header?.raw_data?.txTrieRoot = signParams.data.nowBlock.block_header.raw_data.txTrieRoot
                tronBlock.block_header?.raw_data?.witness_address = signParams.data.nowBlock.block_header.raw_data.witness_address

                hasher = FVTrc20Hasher(contractAddress: signParams.data.contractAddress, fromAddress: signParams.data.fromAddress, toAddress: signParams.data.toAddress, amount: signParams.data.amount, nowBlock: tronBlock, publicKey: signParams.data.publicKey, symbol: "", decimal: signParams.data.decimal)
            }

            guard let h = hasher else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "hash invalid")))
                return
            }

            fortvaxWallet.sign(bindingId: cId, hasher: h) { [weak self] result in
                guard let sl = self else {
                    return
                }

                switch result {
                case let .walletDidSign(_, _, _, errorMessage):
                    if errorMessage != nil {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                        return
                    }

                case let .walletDidSignAllCompleted(vrs, _):
                    guard let sig = vrs.first else {

                        return
                    }

                    var signTx: Data? = nil

                    if h is FVTrc20Hasher {
                        signTx = try? (h as! FVTrc20Hasher).signRawTransaction(vrs: sig)
                    } else {
                        signTx = try? (h as! FVTronHasher).signRawTransaction(vrs: sig)
                    }

                    guard let success = FVJsonRpcResultMaker.makeSign(id: sl.jsonRpcId, signTx: signTx!.toHexString()) else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                        return
                    }

                    resultHandler(.success(success))

                case .walletExecuting:
                    sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                case let .walletExecuteError(error):
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))

                default:
                    break
                }
            }
            break
        case .btc:
            guard let params = try? decoder.decode(FVJsonParamSign<FVUtxoSignData>.self, from: data).data else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                return
            }

            do {
                let rx = FVBtcRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch BTCUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2shP2wpkh:
                    try rx.addNestedSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2wpkh:
                    try rx.addNativeSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .undefined:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                @unknown default:
                    break
                }

                let pk = FVECPublicKey()
                pk.change = params.fromPublicKey.change
                pk.compressedKey = params.fromPublicKey.compressedKey
                pk.uncompressedKey = params.fromPublicKey.uncompressedKey
                pk.addressIndex = params.fromPublicKey.addressIndex

                let totalFee = try rx.estimateNetworkFee()
                let settlement = try rx.settle(changePublicKey: pk)
                // 重建一個預備要廣播上去的交易結構
                let finalRawTx = FVBtcRawTransaction(satoshiFeeRate: params.feeRate)

                // 要保存當時傳給卡片簽章的對應公鑰，最後序列化會用到
                let hasher = FVBitcoinHasher(settlement: settlement, scriptSignPublicKeys: [pk])

                fortvaxWallet.sign(bindingId: cId, hasher: hasher) { [weak self] walletResult in
                    guard let sl = self else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                        return
                    }

                    switch walletResult {
                    case let .walletDidSign(_, derFormat, signIndex, errorMessage):
                        if errorMessage != nil {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                            return
                        }

                        let witness = BTCUtil.createWitness(signature: derFormat, publicKey: hasher.scriptSignPublicKey(index: signIndex))
                        let preSignTxIn = settlement.txInputs[signIndex]

                        let postSignTxIn = FVBTCInput(amount: preSignTxIn.amount, refTxHash: preSignTxIn.refTxHash, refTxIndex: preSignTxIn.refTxIndex, signatureScript: BTCUtil.createNestedSegWitScriptSig(publicKey: hasher.scriptSignPublicKey(index: signIndex)), refPkScript: preSignTxIn.refPkScript, witness: Data(witness), type: preSignTxIn.type)
                        finalRawTx.addSendIntput(fromSettleInput: postSignTxIn)

                    case .walletDidSignAllCompleted:
                        for txOut in settlement.txOutputs {
                            finalRawTx.addSendTxOutput(fromSettleOutput: txOut)
                        }

                        guard let success = FVJsonRpcResultMaker.makeSign(id: sl.jsonRpcId, signTx: finalRawTx.bitcoinSerialize(btcFee: totalFee).toHexString()) else {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                            return
                        }

                        resultHandler(.success(success))

                    case .walletExecuting:
                        sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                    case let .walletExecuteError(error):
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))

                    default:
                        break
                    }
                }
            } catch {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "\(error)")))
            }

            break
        case .ltc:
            guard let params = try? decoder.decode(FVJsonParamSign<FVUtxoSignData>.self, from: data).data else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                return
            }

            do {
                let rx = FVLtcRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch LTCUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2sh_p2wpkh:
                    try rx.addNestedSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .p2wpkh:
                    try rx.addNativeSegWitOutput(address: params.toAddress, amount: params.sendAmount)
                case .none:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                @unknown default:
                    break
                }

                let pk = FVECPublicKey()
                pk.change = params.fromPublicKey.change
                pk.compressedKey = params.fromPublicKey.compressedKey
                pk.uncompressedKey = params.fromPublicKey.uncompressedKey
                pk.addressIndex = params.fromPublicKey.addressIndex

                let totalFee = try rx.estimateNetworkFee()
                let settlement = try rx.settle(changePublicKey: pk)
                // 重建一個預備要廣播上去的交易結構
                let finalRawTx = FVLtcRawTransaction(satoshiFeeRate: params.feeRate)

                // 要保存當時傳給卡片簽章的對應公鑰，最後序列化會用到
                let hasher = FVLitecoinHasher(settlement: settlement, scriptSignPublicKeys: [pk])

                fortvaxWallet.sign(bindingId: cId, hasher: hasher) { [weak self] walletResult in
                    guard let sl = self else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                        return
                    }

                    switch walletResult {
                    case let .walletDidSign(_, derFormat, signIndex, errorMessage):
                        if errorMessage != nil {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                            return
                        }

                        let witness = LTCUtil.createWitness(signature: derFormat, publicKey: hasher.scriptSignPublicKey(index: signIndex))
                        let preSignTxIn = settlement.txInputs[signIndex]

                        let postSignTxIn = FVLtcInput(amount: preSignTxIn.amount, refTxHash: preSignTxIn.refTxHash, refTxIndex: preSignTxIn.refTxIndex, signatureScript: LTCUtil.createNestedSegWitScriptSig(publicKey: hasher.scriptSignPublicKey(index: signIndex)), refPkScript: preSignTxIn.refPkScript, witness: Data(witness), type: preSignTxIn.type)
                        finalRawTx.addSendIntput(fromSettleInput: postSignTxIn)

                    case .walletDidSignAllCompleted:
                        for txOut in settlement.txOutputs {
                            finalRawTx.addSendTxOutput(fromSettleOutput: txOut)
                        }

                        guard let success = FVJsonRpcResultMaker.makeSign(id: sl.jsonRpcId, signTx: finalRawTx.litecoinSerialize(ltcFee: totalFee).toHexString()) else {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                            return
                        }

                        resultHandler(.success(success))

                    case .walletExecuting:
                        sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                    case let .walletExecuteError(error):
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))

                    default:
                        break
                    }
                }
            } catch {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "\(error)")))
            }
            break
        case .doge:
            guard let params = try? decoder.decode(FVJsonParamSign<FVUtxoSignData>.self, from: data).data else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "sign params invalid")))
                return
            }

            guard var coinCode = params.coinCode else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "coinCode null")))
                return
            }
            if coinCode.hasPrefix("0x") {
                coinCode = String(coinCode.dropFirst(2))
            }

            guard let coinCodeData = UInt8(coinCode, radix: 16) else {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "coinCode null")))
                return
            }

            do {
                let rx = FVDogeRawTransaction(satoshiFeeRate: params.feeRate)
                let convertUtxo = params.candidateUtxo.compactMap { u -> FVBtcUtxo in
                    return u.transformBtcUtxo()
                }

                for u in convertUtxo {
                    rx.addCandidateUtxo(utxo: u)
                }

                switch DogeUtil.checkAddressType(address: params.toAddress) {
                case .p2pkh:
                    try rx.addP2PKHOutput(address: params.toAddress, amount: params.sendAmount)
                default:
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "to address invalid")))
                }

                let pk = FVECPublicKey()
                pk.change = params.fromPublicKey.change
                pk.compressedKey = params.fromPublicKey.compressedKey
                pk.uncompressedKey = params.fromPublicKey.uncompressedKey
                pk.addressIndex = params.fromPublicKey.addressIndex

                let totalFee = try rx.estimateNetworkFee()
                let settlement = try rx.settle(changePublicKey: pk)
                // 重建一個預備要廣播上去的交易結構
                let finalRawTx = FVDogeRawTransaction(satoshiFeeRate: params.feeRate)

                // 要保存當時傳給卡片簽章的對應公鑰，最後序列化會用到
                let hasher = FVDogeHasher(settlement: settlement, scriptSignPublicKeys: [pk], coinCode: coinCodeData)

                fortvaxWallet.sign(bindingId: cId, hasher: hasher) { [weak self] walletResult in
                    guard let sl = self else {
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                        return
                    }

                    switch walletResult {
                    case let .walletDidSign(_, derFormat, signIndex, errorMessage):
                        if errorMessage != nil {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign verify fail")))
                            return
                        }

                        let preSignTxIn = settlement.txInputs[signIndex]
                        let postSignTxIn = FVDogeInput(amount: preSignTxIn.amount, refTxHash: preSignTxIn.refTxHash, refTxIndex: preSignTxIn.refTxIndex, signatureScript: Data(DogeUtil.createWitness(signature: derFormat, publicKey: hasher.scriptSignPublicKey(index: signIndex))), refPkScript: preSignTxIn.refPkScript, type: preSignTxIn.type)
                        finalRawTx.addSendIntput(fromSettleInput: postSignTxIn)

                    case .walletDidSignAllCompleted:
                        for txOut in settlement.txOutputs {
                            finalRawTx.addSendTxOutput(fromSettleOutput: txOut)
                        }

                        guard let success = FVJsonRpcResultMaker.makeSign(id: sl.jsonRpcId, signTx: finalRawTx.dogecoinSerialize(btcFee: totalFee).toHexString()) else {
                            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "sign tx invalid")))
                            return
                        }

                        resultHandler(.success(success))

                    case .walletExecuting:
                        sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

                    case let .walletExecuteError(error):
                        resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: error), detailMessage: "")))

                    default:
                        break
                    }
                }
            } catch {
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "\(error)")))
            }
            break
        default:
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .invalidParams, detailMessage: "type invalid")))
        }
    }
}
