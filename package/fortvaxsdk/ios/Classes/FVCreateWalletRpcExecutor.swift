//
//  FVCreateWalletRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/10.
//

import Foundation
import Core
import CoreNFC

public class FVCreateWalletRpcExecutor: FVBaseSdkJsonRpcExecutor {

    let wordCount: Int
    let createPassword: Bool

    init(wordCount: Int, createPassword: Bool, jsonRpcId: String, tag: NFCISO7816Tag?, cardId: String?, caPublicKey: String, pinCode: String?) {
        self.wordCount = wordCount
        self.createPassword = createPassword
        super.init(jsonRpcId: jsonRpcId, tag: tag, cardId: cardId, caPublicKey: caPublicKey, pinCode: pinCode)
    }

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {

        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }
        
        guard let cId = self.cardId else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "cid null")))
            return
        }
        
        guard let pin = self.pinCode else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "pin null")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.setPinCode(pinCode: pin)
        fortvaxWallet.assignNFCTag(tag: tag)

        fortvaxWallet.createWallet(wordsCount: wordCount, bindingId: cId, createPassword: createPassword) { [weak self] result in
            guard let sl = self else {
                return
            }
            switch result {
            case let .walletDidCreateChain(seedWords, accountPublicKeys, btcUtxoPublicKeys):
                guard let success = FVJsonRpcResultMaker.makeCreateWallet(id: sl.jsonRpcId, seedWords: seedWords, accountPublicKeys: accountPublicKeys, btcUtxoPublicKeys: btcUtxoPublicKeys) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                    return
                }
                resultHandler(.success(success))
            case .walletDidSetUpPinCode:
                sl.eventSink?(FVFlutterSdk.walletPasswordCreatedEvent)
            case let .walletExecuteError(er):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))
            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)
            default:
                break
            }
        }
    }
}
