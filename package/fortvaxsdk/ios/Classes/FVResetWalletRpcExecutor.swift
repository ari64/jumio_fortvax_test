//
//  FVResetWalletRpcExecutor.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/11.
//

import Core
import CoreNFC

class FVResetWalletRpcExecutor: FVBaseSdkJsonRpcExecutor {

    private var fortvaxWallet = FortVaxWallet()

    override func execute(resultHandler: @escaping (Result<String, FVJsonRpcExecutorError>) -> Void) {
        guard let caPbKeyData = caPublicKey.hexData(), let secCAKey = FVFlutterCrypto.derPublicKey(rsaKeyRawData: caPbKeyData.bytes) else {

            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "ca key")))
            return
        }

        guard let tag = self.tag else {
            resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .tagConnectionFail, detailMessage: "")))
            return
        }

        fortvaxWallet.assignCA(caPbk: secCAKey)
        fortvaxWallet.assignNFCTag(tag: tag)

        fortvaxWallet.resetWallet { [weak self] result in
            guard let sl = self else {
                return
            }

            switch result {
            case .walletDidReset:
                guard let success = FVJsonRpcResultMaker.makeSuccessMessage(id: sl.jsonRpcId) else {
                    resultHandler(.failure(FVJsonRpcExecutorError.fail(type: .internalError, detailMessage: "")))
                    return
                }
                resultHandler(.success(success))

            case .walletExecuting:
                sl.eventSink?(FVFlutterSdk.walletExecuteEvent)

            case let .walletExecuteError(er):
                resultHandler(.failure(FVJsonRpcExecutorError.fail(type: sl.transformError(error: er), detailMessage: "")))
            default:
                break
            }
        }
    }
}
