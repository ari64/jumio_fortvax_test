//
//  FVJsonRpcError.swift
//  fortvax_sdk
//
//  Created by tnp on 2023/7/9.
//

import Foundation

class FVJsonRpcError: Encodable {
    let id: String
    let jsonrpc: String
    let error: FVRpcError

    init(id: String, jsonrpc: String, error: FVRpcError) {
        self.id = id
        self.jsonrpc = jsonrpc
        self.error = error
    }

    convenience init(id: String, errorType: FVJsonRpcErrorType, extraMessage: String = "") {
        let e = FVRpcError(code: errorType.code, message: "\(errorType.message) (\(extraMessage))")
        self.init(id: id, jsonrpc: "2.0", error: e)
    }
}

class FVRpcError: Encodable {
    let code: Int
    let message: String

    init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
}

extension FVJsonRpcError {
    func jsonString() -> String? {
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(self) else {
            return nil
        }

        return String(bytes: data, encoding: .utf8)
    }
}
