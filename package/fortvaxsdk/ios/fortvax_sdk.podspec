#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint sdk.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'fortvax_sdk'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter plugin project.'
  s.description      = <<-DESC
A new Flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '14.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
  s.dependency 'CryptoSwift', '1.4.2'
  s.dependency "BigInt" , '5.2.0'
  s.dependency 'SwiftProtobuf', '1.18.0'
  s.dependency 'ObjectMapper' , '4.2.0'
  s.vendored_frameworks = 'Frameworks/Core.xcframework'
end
