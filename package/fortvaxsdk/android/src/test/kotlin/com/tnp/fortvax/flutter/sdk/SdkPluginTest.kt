package com.tnp.fortvax.flutter.sdk

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.json.JSONObject
import org.mockito.Mockito
import kotlin.test.Test

/*
 * This demonstrates a simple unit test of the Kotlin portion of this plugin's implementation.
 *
 * Once you have built the plugin's example app, you can run these tests from the command
 * line by running `./gradlew testDebugUnitTest` in the `example/android/` directory, or
 * you can run them directly from IDEs that support JUnit such as Android Studio.
 */

internal class SdkPluginTest {
    @Test
    fun onMethodCall_getPlatformVersion_returnsExpectedValue() {
        val plugin = FortVaxSdkPlugin()

        val call = MethodCall("getPlatformVersion", null)
        val mockResult: MethodChannel.Result = Mockito.mock(MethodChannel.Result::class.java)
        plugin.onMethodCall(call, mockResult)

        Mockito.verify(mockResult).success("Android " + android.os.Build.VERSION.RELEASE)
    }

    @Test
    fun testJsonPureSystemAPI() {
        val map = mapOf<String, Any>(
            "jsonrpc" to "2.0",
            "result" to mutableMapOf<String, Any>(
                "seedWords" to "benefit gasp hub slush mushroom simple shop strategy ten bottom photo process",
                "accountModelKeys" to listOf(
                    mutableMapOf<String, Any>(
                        "type" to 0,
                        "compressedKey" to "",
                        "uncompressedKey" to "",
                        "keyToAddress" to "",
                        "change" to false,
                        "addressIndex" to -1
                    )
                ),
                "utxoModelKeys" to listOf(
                    mutableMapOf<String, Any>(
                        "type" to 1,
                        "compressedKey" to "",
                        "uncompressedKey" to "",
                        "keyToAddress" to "",
                        "change" to false,
                        "addressIndex" to -1
                    )
                )
            ),
            "id" to "1"
        )

        println(JSONObject(map).toString())

        val json = "{\n" +
                "  \"jsonrpc\": \"2.0\",\n" +
                "  \"method\": \"new_blockchain\",\n" +
                "  \"params\": {\n" +
                "\t\t\"pinCode\": \"\",\n" +
                "    \"caPublicKey\": \"hex 16 進位字串\",\n" +
                "    \"types\": [\n" +
                "      12,\n" +
                "      13,\n" +
                "      14\n" +
                "    ],\n" +
                "    \"cardId\": \"46563030303030303020\"\n" +
                "  },\n" +
                "  \"id\": \"1\"\n" +
                "}"
        val rpc = FVJsonRpc.parser(json)
        println(rpc?.params)
    }
}
