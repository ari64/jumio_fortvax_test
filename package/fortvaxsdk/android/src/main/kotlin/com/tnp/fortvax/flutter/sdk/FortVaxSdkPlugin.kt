package com.tnp.fortvax.flutter.sdk

import android.app.Activity
import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import com.tnp.fortvax.core.FVSDKCore
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.lang.ref.WeakReference

/** SdkPlugin */
@RequiresApi(Build.VERSION_CODES.M)
class FortVaxSdkPlugin : FlutterPlugin, MethodCallHandler, EventChannel.StreamHandler,
    ActivityAware {

    private var nfcAdapter: NfcAdapter? = null
    private var nfcManager: NfcManager? = null
    private val handler = Handler(Looper.getMainLooper())
    private lateinit var wActivity: WeakReference<Activity>

    internal var eventSink: EventChannel.EventSink? = null
    private var methodChannel: MethodChannel? = null
    private var eventChannel: EventChannel? = null

    private var sdk: FVFlutterSdk? = null

    //想不到什麼好方法..，先這樣
    private var sdkWithNoNFC: FVFlutterSdk? = null

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        val messenger = flutterPluginBinding.binaryMessenger
        methodChannel = MethodChannel(messenger, "fortvax_sdk")
        methodChannel!!.setMethodCallHandler(this)
        eventChannel = EventChannel(messenger, "fortvax_sdk.event")
        eventChannel!!.setStreamHandler(this)
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        require(wActivity.get() != null) { "Plugin not ready yet" }

        FVSDKCore.init(false, "")

        if (call.method != "callJsonRpc") {
            return result.error(
                FVJsonRpcErrorType.InvalidRequest.code.toString(),
                FVJsonRpcErrorType.InvalidRequest.message,
                FVJsonRpcUtil.errorJsonRpc(
                    "-1", FVJsonRpcErrorType.InvalidRequest, "call method not supported."
                )
            )
        }

        //上面已經先強制檢查，因此這裡一定有值。安全拆包。
        val activity = wActivity.get() ?: return

        val topLevelArgumentKey = "jsonRpcData"
        val jsonRpcDataString = optArgument(call, topLevelArgumentKey) ?: ""
        if (jsonRpcDataString.isEmpty()) {
            return result.error(
                FVJsonRpcErrorType.InvalidRequest.code.toString(),
                FVJsonRpcErrorType.InvalidRequest.message,
                FVJsonRpcUtil.errorJsonRpc(
                    "-1", FVJsonRpcErrorType.InvalidRequest, "missing argument : jsonRpcData"
                )
            )
        }

        val rpc = FVJsonRpc.parser(jsonRpcDataString)
            ?: return result.error(
                FVJsonRpcErrorType.ParserError.code.toString(),
                FVJsonRpcErrorType.ParserError.message,
                FVJsonRpcUtil.errorJsonRpc(
                    "-1", FVJsonRpcErrorType.ParserError, "invalid json rpc data"
                )
            )

        when (rpc.method) {
            FVJsonRpcMethod.ReadCardStatus.methodKey,
            FVJsonRpcMethod.CreateWallet.methodKey,
            FVJsonRpcMethod.RestoreWallet.methodKey,
            FVJsonRpcMethod.ReadAccountPublicKey.methodKey,
            FVJsonRpcMethod.ReadUtxoPublicKey.methodKey,
            FVJsonRpcMethod.ResetWallet.methodKey,
            FVJsonRpcMethod.NewBlockchain.methodKey,
            FVJsonRpcMethod.Sign.methodKey,
            -> {
                //定義好的 json rpc 規格，統一處理
                val nfcManager =
                    activity.getSystemService(Context.NFC_SERVICE) as NfcManager
                nfcAdapter = nfcManager.defaultAdapter
                if (nfcAdapter == null) {
                    //該手機沒有 nfc
                    result.error(
                        FVJsonRpcErrorType.NfcNeedsEnable.code.toString(),
                        FVJsonRpcErrorType.NfcNeedsEnable.message,
                        FVJsonRpcUtil.errorJsonRpc(
                            "1",
                            FVJsonRpcErrorType.NfcNeedsEnable,
                            "hardware not supported."
                        )
                    )
                } else if (!nfcAdapter!!.isEnabled) {
                    //該手機沒有 打開 nfc
                    result.error(
                        FVJsonRpcErrorType.NfcNeedsEnable.code.toString(),
                        FVJsonRpcErrorType.NfcNeedsEnable.message,
                        FVJsonRpcUtil.errorJsonRpc(
                            "1",
                            FVJsonRpcErrorType.NfcNeedsEnable
                        )
                    )
                } else {
                    nfcAdapter?.apply {
                        if (sdk == null) {
                            sdk = FVFlutterSdk()
                            sdk?.starScanAndHandleJsonRpc(rpc, this, activity, result, eventSink)
                        } else {
                            //指令可以連續，如果沒有中止過 scan，則一直重複使用
                            sdk?.handleJsonRpc(rpc, result, eventSink, nfcAdapter, activity)
                        }
                    }
                }
            }

            FVJsonRpcMethod.SettleUtxoTransaction.methodKey -> {
                //特別處理，因為這個不需要 nfc
                if (sdkWithNoNFC == null) {
                    sdkWithNoNFC = FVFlutterSdk()
                }

                sdkWithNoNFC?.handleJsonRpc(rpc, result)
            }

            FVJsonRpcMethod.StopScan.methodKey -> {
                // 處理 StopScan 的邏輯
                sdk = null
                sdkWithNoNFC = null
            }

            else -> {
                //不在定義範圍內的 json rpc , 一律 error
                return result.error(
                    FVJsonRpcErrorType.MethodNotFound.code.toString(),
                    FVJsonRpcErrorType.MethodNotFound.message,
                    FVJsonRpcUtil.errorJsonRpc(
                        "-1", FVJsonRpcErrorType.MethodNotFound, rpc.method
                    )
                )
            }
        }
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel?.setMethodCallHandler(null)
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        eventSink = events
    }

    override fun onCancel(arguments: Any?) {
        eventSink?.endOfStream()
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        val activity = binding.activity
        wActivity = WeakReference(activity)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        sdk = null
        val activity = wActivity.get() ?: return
        nfcAdapter?.stopNFCReader(activity)
        wActivity = WeakReference(null)
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        wActivity = WeakReference(binding.activity)
    }

    override fun onDetachedFromActivity() {
        sdk = null
        val activity = wActivity.get() ?: return
        nfcAdapter?.stopNFCReader(activity)
        wActivity = WeakReference(null)
    }

    private fun optArgument(call: MethodCall, argKey: String): String? {
        if (!call.hasArgument(argKey)) {
            return null
        }

        return call.argument<String>(argKey)
    }
}
