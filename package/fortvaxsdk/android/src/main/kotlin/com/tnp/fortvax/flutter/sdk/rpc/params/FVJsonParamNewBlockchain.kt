package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamNewBlockchain(jsonParams: String) {

    var caPublicKey = ""
        private set
    var types  = arrayListOf<Int>()
        private set
    var pinCode = ""
        private set
    var cardId = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
        cardId = json.getString("cardId")
        pinCode = json.getString("pinCode")

        val mainNetArray = json.getJSONArray("types")
        for (i in 0 until mainNetArray.length()) {
            val item = mainNetArray.getInt(i)
            types.add(item)
        }
    }
}