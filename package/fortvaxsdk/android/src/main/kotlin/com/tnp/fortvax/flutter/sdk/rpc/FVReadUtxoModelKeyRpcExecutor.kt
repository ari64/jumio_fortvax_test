package com.tnp.fortvax.flutter.sdk.rpc

import android.nfc.Tag
import com.tnp.fortvax.core.blockchain.FVCardBlockChainType
import com.tnp.fortvax.core.blockchain.FVECPublicKey
import com.tnp.fortvax.core.nfc.IFVNFCConnectionProtocol
import com.tnp.fortvax.core.nfc.command.protocol.FVWalletInfo
import com.tnp.fortvax.core.nfc.command.types.FVCommandCoinPath
import com.tnp.fortvax.core.nfc.hexStringToByteArray
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.core.nfc.wallet.IFortVaxWalletProtocol
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import com.tnp.fortvax.flutter.sdk.FVJsonRpcUtil
import com.tnp.fortvax.flutter.sdk.blockchainTypeFromRpc

class FVReadUtxoModelKeyRpcExecutor(
    id: String,
    tag: Tag?,
    cid: String,
    caPublicKey: String,
    pinCode: String,
    val type: Int,
    fortVaxWallet: FortVaxWallet
) :
    FVBaseSdkJsonRpcExecutor(id, tag, cid, caPublicKey, pinCode, fortVaxWallet) {

    override fun execute(resultHandler: (Result<String>) -> Unit) {
        val tag = this.tag ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.TagConnectionFail,
            resultHandler
        )

        val pin =
            pinCode ?: return sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "pin null"
            )

        val cid = cardId ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.InternalError,
            resultHandler,
            "cid null"
        )

        if (caPublicKey.isEmpty() || caPublicKey.hexStringToByteArray().isEmpty()) {
            sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "ca key"
            )
            return
        }

        fortVaxWallet.assignNFCTag(tag)
        fortVaxWallet.setPinCode(pin)
        fortVaxWallet.assignCA(caPublicKey.hexStringToByteArray())

        fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {

            override fun walletDidReadInfo(info: ArrayList<FVWalletInfo>) {
                //12 以後的主鏈(含12)，都要從這裡拿 code , 絕對精準
                val chainFromType = type.blockchainTypeFromRpc()
                if (chainFromType == FVCardBlockChainType.DOGE) {
                    val doge = info.first { it.coinPath == FVCommandCoinPath.DOGE }
                    readUTXOKey(fortVaxWallet, cid, doge.coinCode, resultHandler)
                }
            }

            override fun walletDidReadUTXOPublicKey(utxoPublicKeys: ArrayList<FVECPublicKey>) {
                val result = FVJsonRpcResultMaker.makeReadUtxoModelKey(utxoPublicKeys.first())
                resultHandler(Result.success(FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)))
            }

            override fun walletExecuteError(throwable: Throwable) {
                sendJsonErrorResponse(throwable, resultHandler)
            }

            override fun walletExecuting() {
                eventSink?.success(walletExecuteEvent)
            }

            override fun walletDidExecute() {
            }

        }

        //用來安全檢查
        //未來如果有新主鏈，持續往後增加
        val sdkSupportChain = listOf(2, 6, 12)
        if (!sdkSupportChain.contains(type)) {
            sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "type invalid")
            return
        }

        when (type) {
            2, 6 -> {
                readUTXOKey(fortVaxWallet, cid, type.blockchainTypeFromRpc().coinCode.toInt(), resultHandler)
            }

            12 -> {
                fortVaxWallet.readWalletInfo(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {

                    }

                })
            }

            else -> {

            }
        }
    }

    private fun readUTXOKey(
        fortVaxWallet: FortVaxWallet,
        cid: String,
        coinCode: Int,
        resultHandler: (Result<String>) -> Unit
    ) {
        fortVaxWallet.readUTXOPublicKey(
            cid,
            coinCode,
            0,
            2,
            object : IFVNFCConnectionProtocol {
                override fun connectSuccess() {

                }

                override fun connectFail() {
                    eventSink?.success(walletConnectFailEvent)

                    sendJsonErrorResponse(
                        FVJsonRpcErrorType.TagConnectionFail,
                        resultHandler
                    )
                }

            })
    }

}