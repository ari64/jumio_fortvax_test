package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamReadAccountModelKey(jsonParams: String) {

    var caPublicKey = ""
        private set
    var supportedCryptoMainNet = arrayListOf<Int>()
        private set
    var cardId = ""
        private set
    var pinCode = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
        cardId = json.getString("cardId")
        pinCode = json.getString("pinCode")

        val mainNetArray = json.getJSONArray("supportedCryptoMainNet")
        for (i in 0 until mainNetArray.length()) {
            val item = mainNetArray.getInt(i)
            supportedCryptoMainNet.add(item)
        }
    }
}