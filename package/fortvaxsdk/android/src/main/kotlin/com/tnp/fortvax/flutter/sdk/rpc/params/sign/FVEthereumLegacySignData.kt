package com.tnp.fortvax.flutter.sdk.rpc.params.sign

import org.json.JSONObject

class FVEthereumLegacySignData(jsonObject: JSONObject) {

    var nonce: String = ""
        private set
    var gasPrice: String = ""
        private set
    var gasLimit: String = ""
        private set
    var amount: String = ""
        private set
    var toAddress: String = ""
        private set
    var chainId: Long = 0
        private set
    var publicKey: String = ""
        private set

    init {
        nonce = jsonObject.getString("nonce")
        gasPrice = jsonObject.getString("gasPrice")
        gasLimit = jsonObject.getString("gasLimit")
        amount = jsonObject.getString("amount")
        toAddress = jsonObject.getString("toAddress")
        chainId = jsonObject.getLong("chainId")
        publicKey = jsonObject.getString("publicKey").substring(2)
    }
}