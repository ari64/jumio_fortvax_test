package com.tnp.fortvax.flutter.sdk.rpc.params

import com.tnp.fortvax.core.blockchain.btc.FVBtcUtxo
import com.tnp.fortvax.core.blockchain.ltc.FVLtcUtxo
import org.json.JSONObject

class FVJsonParamSettleUtxo(jsonParams: String) {

    var type = 2
        private set
    var feeRate = 2
        private set
    var fromPublicKey = ""
        private set
    var toAddress = ""
        private set
    var candidateUtxo = ArrayList<CandidateUtxo>()
        private set
    var sendAmount = 0L
        private set

    init {
        val json = JSONObject(jsonParams)
        feeRate = json.getInt("feeRate")
        fromPublicKey = json.getString("fromPublicKey")
        toAddress = json.getString("toAddress")
        sendAmount = json.getLong("sendAmount")
        type = json.getInt("type")
        val utxoArray = json.getJSONArray("candidateUtxo")

        for (i in 0 until utxoArray.length()) {
            val item = utxoArray.getJSONObject(i)
            val candidate = CandidateUtxo(item)
            candidateUtxo.add(candidate)
        }
    }

    class CandidateUtxo(json: JSONObject) {
        var tx_hash = ""
            private set
        var tx_input_n = -1
            private set
        var tx_output_n = -1
            private set
        var value = 0L
            private set
        var script = ""
            private set
        var address = ""
            private set
        var confirmations = 0L
            private set

        init {
            tx_hash = json.getString("tx_hash")
            tx_input_n = json.getInt("tx_input_n")
            tx_output_n = json.getInt("tx_output_n")
            value = json.getLong("value")
            script = json.getString("script")
            address = json.getString("address")
            confirmations = json.getLong("confirmations")
        }

        fun transformBtcUtxo(): FVBtcUtxo {
            val btcUtxo = FVBtcUtxo()
            btcUtxo.address = address
            btcUtxo.script = script
            btcUtxo.tx_hash = tx_hash
            btcUtxo.confirmations = confirmations
            btcUtxo.tx_input_n = tx_input_n
            btcUtxo.tx_output_n = tx_output_n
            btcUtxo.value = value
            return btcUtxo
        }

        fun transformLtcUtxo(): FVLtcUtxo {
            val ltcUtxo = FVLtcUtxo()
            ltcUtxo.address = address
            ltcUtxo.script = script
            ltcUtxo.tx_hash = tx_hash
            ltcUtxo.confirmations = confirmations
            ltcUtxo.tx_input_n = tx_input_n
            ltcUtxo.tx_output_n = tx_output_n
            ltcUtxo.value = value
            return ltcUtxo
        }
    }
}