package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamRestoreWallet(jsonParams: String) {

    var caPublicKey = ""
        private set
    var words = ""
        private set
    var createPassword = false
        private set
    var cardId = ""
        private set
    var pinCode = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
        words = json.getString("words")
        createPassword = json.getBoolean("createPassword")
        cardId = json.getString("cardId")
        pinCode = json.getString("pinCode")
    }
}