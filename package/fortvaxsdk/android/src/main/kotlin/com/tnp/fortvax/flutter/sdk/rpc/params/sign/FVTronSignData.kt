package com.tnp.fortvax.flutter.sdk.rpc.params.sign

import com.tnp.fortvax.core.blockchain.tron.FVTronBlock
import com.tnp.fortvax.core.blockchain.tron.FVTronBlockHeader
import com.tnp.fortvax.core.blockchain.tron.FVTronBlockRawData
import org.json.JSONObject

class FVTronSignData(jsonObject: JSONObject) {

    var fromAddress: String = ""
        private set
    var toAddress: String = ""
        private set
    var amount = 0L
        private set
    var publicKey: String = ""
        private set
    var nowBlock: FVTronBlock
        private set

    init {
        amount = jsonObject.getLong("amount")
        toAddress = jsonObject.getString("toAddress")
        publicKey = jsonObject.getString("publicKey").substring(2)
        fromAddress = jsonObject.getString("fromAddress")

        nowBlock = FVTronBlock()
        val nowBlockObject = jsonObject.getJSONObject("nowBlock")
        nowBlock.blockID = nowBlockObject.getString("blockID")
        val blockHeaderObject = nowBlockObject.getJSONObject("block_header")
        val rawDataObject = blockHeaderObject.getJSONObject("raw_data")
        nowBlock.block_header = FVTronBlockHeader()
        nowBlock.block_header.raw_data = FVTronBlockRawData()

        nowBlock.block_header.raw_data.number = rawDataObject.getLong("number")
        nowBlock.block_header.raw_data.txTrieRoot = rawDataObject.getString("txTrieRoot")
        nowBlock.block_header.raw_data.witness_address = rawDataObject.getString("witness_address")
        nowBlock.block_header.raw_data.parentHash = rawDataObject.getString("parentHash")
        nowBlock.block_header.raw_data.version = rawDataObject.getInt("version")
        nowBlock.block_header.raw_data.timestamp = rawDataObject.getLong("timestamp")
    }
}