package com.tnp.fortvax.flutter.sdk.rpc

import android.nfc.Tag
import com.tnp.fortvax.core.blockchain.FVCardBlockChainType
import com.tnp.fortvax.core.blockchain.FVECPublicKey
import com.tnp.fortvax.core.blockchain.btc.BTCAddressType
import com.tnp.fortvax.core.blockchain.btc.BTCUtil
import com.tnp.fortvax.core.blockchain.btc.FVBtcRawTransaction
import com.tnp.fortvax.core.blockchain.doge.DogeUtil
import com.tnp.fortvax.core.blockchain.doge.FVDogeRawTransaction
import com.tnp.fortvax.core.blockchain.eth.EthSignUtil
import com.tnp.fortvax.core.blockchain.eth.FVPrepareVRS
import com.tnp.fortvax.core.blockchain.ltc.FVLtcRawTransaction
import com.tnp.fortvax.core.blockchain.ltc.LTCAddressType
import com.tnp.fortvax.core.blockchain.ltc.LTCUtil
import com.tnp.fortvax.core.nfc.IFVNFCConnectionProtocol
import com.tnp.fortvax.core.nfc.command.protocol.FVTxSign
import com.tnp.fortvax.core.nfc.hexStringToByteArray
import com.tnp.fortvax.core.nfc.toHexString
import com.tnp.fortvax.core.nfc.wallet.FVBitcoinHasher
import com.tnp.fortvax.core.nfc.wallet.FVDogeHasher
import com.tnp.fortvax.core.nfc.wallet.FVErc20EIP1559Hasher
import com.tnp.fortvax.core.nfc.wallet.FVErc20Hasher
import com.tnp.fortvax.core.nfc.wallet.FVEthereumEIP1559Hasher
import com.tnp.fortvax.core.nfc.wallet.FVEthereumHasher
import com.tnp.fortvax.core.nfc.wallet.FVLitecoinHasher
import com.tnp.fortvax.core.nfc.wallet.FVTrc20Hasher
import com.tnp.fortvax.core.nfc.wallet.FVTronHasher
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.core.nfc.wallet.IFVCardHasher
import com.tnp.fortvax.core.nfc.wallet.IFortVaxWalletProtocol
import com.tnp.fortvax.core.web3j.crypto.RawTransaction
import com.tnp.fortvax.core.web3j.crypto.Sign
import com.tnp.fortvax.core.web3j.utils.Numeric
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import com.tnp.fortvax.flutter.sdk.FVJsonRpcUtil
import com.tnp.fortvax.flutter.sdk.blockchainTypeFromRpc
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVEthereum1559Erc20SignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVEthereum1559SignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVEthereumLegacyErc20SignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVEthereumLegacySignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVJsonParamSign
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVTronSignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVTronTrc20SignData
import com.tnp.fortvax.flutter.sdk.rpc.params.sign.FVUtxoSignData
import java.math.BigInteger

class FVSignRpcExecutor(id: String, tag: Tag?, val paramsJson: String, fortVaxWallet: FortVaxWallet) : FVBaseSdkJsonRpcExecutor(id, tag, null, "", null, fortVaxWallet) {

    override fun execute(resultHandler: (Result<String>) -> Unit) {
        val signParam = FVJsonParamSign(paramsJson)

        val cid = signParam.cardId

        val tag = this.tag ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.TagConnectionFail,
            resultHandler
        )

        if (signParam.caPublicKey.isEmpty() || signParam.caPublicKey.hexStringToByteArray().isEmpty()) {
            sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "ca key"
            )
            return
        }

        val pin = signParam.pinCode

        fortVaxWallet.assignNFCTag(tag)
        fortVaxWallet.setPinCode(pin)
        fortVaxWallet.assignCA(signParam.caPublicKey.hexStringToByteArray())

        val fungible = "fungible"
        val native = "native"

        when (signParam.type.blockchainTypeFromRpc()) {
            FVCardBlockChainType.ETH -> {

                val hasher: IFVCardHasher
                val chainId: Long
                if (signParam.evmType == 1 || signParam.evmType == 2) {
                    if (signParam.transactionType == native) {
                        val ethData = FVEthereum1559SignData(signParam.data)
                        chainId = ethData.chainId
                        hasher = FVEthereumEIP1559Hasher(
                            ethData.nonce.toBigInteger(),
                            ethData.gasLimit.toBigInteger(),
                            ethData.amount.toBigInteger(),
                            ethData.toAddress,
                            ethData.chainId,
                            ethData.publicKey,
                            ethData.maxFeePerGas.toBigInteger(),
                            ethData.maxPriorityFeePerGas.toBigInteger()
                        )

                    } else {
                        val ethData = FVEthereum1559Erc20SignData(signParam.data)
                        chainId = ethData.chainId
                        hasher = FVErc20EIP1559Hasher(
                            ethData.contractAddress,
                            ethData.nonce.toBigInteger(),
                            ethData.gasLimit.toBigInteger(),
                            ethData.amount.toBigInteger(),
                            ethData.toAddress,
                            ethData.chainId,
                            ethData.publicKey,
                            ethData.maxFeePerGas.toBigInteger(),
                            ethData.maxPriorityFeePerGas.toBigInteger()
                        )
                    }
                } else if (signParam.evmType == 3) {
                    if (signParam.transactionType == native) {
                        val ethData = FVEthereumLegacySignData(signParam.data)
                        chainId = ethData.chainId
                        hasher = FVEthereumHasher(
                            ethData.nonce.toBigInteger(),
                            ethData.gasPrice.toBigInteger(),
                            ethData.gasLimit.toBigInteger(),
                            ethData.amount.toBigInteger(),
                            ethData.toAddress,
                            ethData.chainId,
                            ethData.publicKey
                        )

                    } else {
                        val ethData = FVEthereumLegacyErc20SignData(signParam.data)
                        chainId = ethData.chainId
                        hasher = FVErc20Hasher(
                            ethData.contractAddress,
                            ethData.nonce.toBigInteger(),
                            ethData.gasPrice.toBigInteger(),
                            ethData.gasLimit.toBigInteger(),
                            ethData.amount.toBigInteger(),
                            ethData.toAddress,
                            ethData.chainId,
                            ethData.publicKey
                        )
                    }
                } else {
                    sendJsonErrorResponse(
                        FVJsonRpcErrorType.InvalidParams,
                        resultHandler,
                        "evm type invalid"
                    )
                    return
                }

                fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
                    override fun walletDidSign(vrs: FVPrepareVRS, derFormat: ByteArray, signIndex: Int, errorMessage: String?) {
                        if (errorMessage != null) {
                            sendJsonErrorResponse(
                                FVJsonRpcErrorType.InternalError,
                                resultHandler,
                                "sign verify fail"
                            )
                        }
                    }

                    override fun walletDidSignAllCompleted(cardSignResults: ArrayList<FVTxSign>, vrs: ArrayList<FVPrepareVRS>, derFormat: ArrayList<ByteArray>) {
                        val signature = cardSignResults.first()
                        val v = vrs.first()
                        val rawTx: RawTransaction
                        when (hasher) {
                            is FVErc20EIP1559Hasher -> {
                                rawTx = hasher.unsignRawTransaction()
                            }

                            is FVEthereumEIP1559Hasher -> {
                                rawTx = hasher.unsignRawTransaction()
                            }

                            is FVErc20Hasher -> {
                                rawTx = hasher.unsignRawTransaction()
                            }

                            else -> {
                                rawTx = (hasher as FVEthereumHasher).unsignRawTransaction()
                            }
                        }
                        val s = Sign.SignatureData(v.v, Numeric.toBytesPadded(signature.r, 32), Numeric.toBytesPadded(signature.s, 32))
                        val result = FVJsonRpcResultMaker.makeSig(EthSignUtil.completeToSignedRawTransaction(rawTx, chainId, s))
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                            )
                        )
                    }

                    override fun walletDidExecute() {

                    }

                    override fun walletExecuteError(throwable: Throwable) {
                        sendJsonErrorResponse(throwable, resultHandler)
                    }

                    override fun walletExecuting() {
                        eventSink?.success(walletExecuteEvent)
                    }
                }

                fortVaxWallet.sign(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {
                    }

                }, hasher)
            }

            FVCardBlockChainType.TRON -> {
                val hasher: IFVCardHasher = if (signParam.transactionType == native) {
                    val tronData = FVTronSignData(signParam.data)
                    FVTronHasher(tronData.fromAddress, tronData.toAddress, tronData.amount, tronData.nowBlock, tronData.publicKey)
                } else {
                    val tronData = FVTronTrc20SignData(signParam.data)
                    FVTrc20Hasher(tronData.contractAddress, tronData.fromAddress, tronData.toAddress, BigInteger.valueOf(tronData.amount), tronData.nowBlock, tronData.publicKey)
                }

                fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
                    override fun walletDidSign(vrs: FVPrepareVRS, derFormat: ByteArray, signIndex: Int, errorMessage: String?) {
                        if (errorMessage != null) {
                            sendJsonErrorResponse(
                                FVJsonRpcErrorType.InternalError,
                                resultHandler,
                                "sign verify fail"
                            )
                        }
                    }

                    override fun walletDidSignAllCompleted(cardSignResults: ArrayList<FVTxSign>, vrs: ArrayList<FVPrepareVRS>, derFormat: ArrayList<ByteArray>) {
                        when (hasher) {
                            is FVTronHasher -> {
                                val result = FVJsonRpcResultMaker.makeSig(hasher.signRawTransaction(vrs).toHexString())
                                resultHandler(
                                    Result.success(
                                        FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                                    )
                                )
                            }

                            is FVTrc20Hasher -> {
                                val result = FVJsonRpcResultMaker.makeSig(hasher.signRawTransaction(vrs).toHexString())
                                resultHandler(
                                    Result.success(
                                        FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                                    )
                                )
                            }

                            else -> {
                                sendJsonErrorResponse(
                                    FVJsonRpcErrorType.InternalError,
                                    resultHandler,
                                    "sign verify fail"
                                )
                            }
                        }
                    }

                    override fun walletDidExecute() {

                    }

                    override fun walletExecuteError(throwable: Throwable) {
                        sendJsonErrorResponse(throwable, resultHandler)
                    }

                    override fun walletExecuting() {
                        eventSink?.success(walletExecuteEvent)
                    }
                }

                fortVaxWallet.sign(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {
                    }

                }, hasher)
            }

            FVCardBlockChainType.BTC -> {
                val params = FVUtxoSignData(signParam.data)

                //BTC
                val rx = FVBtcRawTransaction()
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformBtcUtxo())
                }

                when (BTCUtil.checkAddressType(params.toAddress)) {
                    BTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.P2SH_P2WPKH -> {
                        rx.addNestedSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.P2WPKH -> {
                        rx.addNativeSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.UNDEFINED -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                val estimateFee = rx.estimateNetworkFee(params.feeRate.toLong())
                val finalTx = FVBtcRawTransaction()
                val ecpk = FVECPublicKey()
                ecpk.addressIndex = params.fromPublicKey.addressIndex
                ecpk.uncompressedKey = params.fromPublicKey.uncompressedKey
                ecpk.compressedKey = params.fromPublicKey.compressedKey
                ecpk.change = params.fromPublicKey.change
                ecpk.type = params.fromPublicKey.type.blockchainTypeFromRpc()
                val settlement = rx.settle(ecpk)
                val hasher = FVBitcoinHasher(settlement, arrayListOf(ecpk))

                fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
                    override fun walletDidSign(vrs: FVPrepareVRS, derFormat: ByteArray, signIndex: Int, errorMessage: String?) {
                        if (errorMessage != null) {
                            sendJsonErrorResponse(
                                FVJsonRpcErrorType.InternalError,
                                resultHandler,
                                "sign verify fail"
                            )
                            return
                        }

                        val preSignTxIn = settlement.txInputs[signIndex]

                        val witness = BTCUtil.createWitness(derFormat, hasher.scriptSignPublicKey(signIndex))

                        //all copy
                        val postSignTxIn =
                            FVBtcRawTransaction.FVBTCInput(
                                preSignTxIn.amount,
                                preSignTxIn.refTxHash,
                                preSignTxIn.refTxIndex,
                                BTCUtil.createNestedSegWitScriptSig(hasher.scriptSignPublicKey(signIndex)),
                                preSignTxIn.refPkScript,
                                witness,
                                preSignTxIn.type
                            )
                        finalTx.addSendInput(postSignTxIn)
                    }

                    override fun walletDidSignAllCompleted(cardSignResults: ArrayList<FVTxSign>, vrs: ArrayList<FVPrepareVRS>, derFormat: ArrayList<ByteArray>) {
                        for (txOut in settlement.txOutputs) {
                            finalTx.addSendTxOutput(txOut)
                        }

                        val result = FVJsonRpcResultMaker.makeSig(finalTx.bitcoinSerialize(estimateFee).toHexString())
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                            )
                        )
                    }

                    override fun walletDidExecute() {

                    }

                    override fun walletExecuteError(throwable: Throwable) {
                        sendJsonErrorResponse(throwable, resultHandler)
                    }

                    override fun walletExecuting() {
                        eventSink?.success(walletExecuteEvent)
                    }
                }

                fortVaxWallet.sign(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {
                    }

                }, hasher)
            }

            FVCardBlockChainType.LTC -> {
                val params = FVUtxoSignData(signParam.data)

                //BTC
                val rx = FVLtcRawTransaction()
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformLtcUtxo())
                }

                when (LTCUtil.checkAddressType(params.toAddress)) {
                    LTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.P2SH_P2WPKH -> {
                        rx.addNestedSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.P2WPKH -> {
                        rx.addNativeSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.UNDEFINED -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                val estimateFee = rx.estimateNetworkFee(params.feeRate.toLong())
                val finalTx = FVLtcRawTransaction()
                val ecpk = FVECPublicKey()
                ecpk.addressIndex = params.fromPublicKey.addressIndex
                ecpk.uncompressedKey = params.fromPublicKey.uncompressedKey
                ecpk.compressedKey = params.fromPublicKey.compressedKey
                ecpk.change = params.fromPublicKey.change
                ecpk.type = params.fromPublicKey.type.blockchainTypeFromRpc()
                val settlement = rx.settle(ecpk)
                val hasher = FVLitecoinHasher(settlement, arrayListOf(ecpk))

                fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
                    override fun walletDidSign(vrs: FVPrepareVRS, derFormat: ByteArray, signIndex: Int, errorMessage: String?) {
                        if (errorMessage != null) {
                            sendJsonErrorResponse(
                                FVJsonRpcErrorType.InternalError,
                                resultHandler,
                                "sign verify fail"
                            )
                            return
                        }

                        val preSignTxIn = settlement.txInputs[signIndex]

                        val witness = LTCUtil.createWitness(derFormat, hasher.scriptSignPublicKey(signIndex))

                        //all copy
                        val postSignTxIn =
                            FVLtcRawTransaction.FVLTCInput(
                                preSignTxIn.amount,
                                preSignTxIn.refTxHash,
                                preSignTxIn.refTxIndex,
                                LTCUtil.createNestedSegWitScriptSig(hasher.scriptSignPublicKey(signIndex)),
                                preSignTxIn.refPkScript,
                                witness,
                                preSignTxIn.type
                            )
                        finalTx.addSendInput(postSignTxIn)
                    }

                    override fun walletDidSignAllCompleted(cardSignResults: ArrayList<FVTxSign>, vrs: ArrayList<FVPrepareVRS>, derFormat: ArrayList<ByteArray>) {
                        for (txOut in settlement.txOutputs) {
                            finalTx.addSendTxOutput(txOut)
                        }

                        val result = FVJsonRpcResultMaker.makeSig(finalTx.litecoinSerialize(estimateFee).toHexString())
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                            )
                        )
                    }

                    override fun walletDidExecute() {

                    }

                    override fun walletExecuteError(throwable: Throwable) {
                        sendJsonErrorResponse(throwable, resultHandler)
                    }

                    override fun walletExecuting() {
                        eventSink?.success(walletExecuteEvent)
                    }
                }

                fortVaxWallet.sign(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {
                    }

                }, hasher)
            }

            FVCardBlockChainType.DOGE -> {
                val params = FVUtxoSignData(signParam.data)

                //BTC
                val rx = FVDogeRawTransaction(params.feeRate.toLong())
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformBtcUtxo())
                }

                when (DogeUtil.checkAddressType(params.toAddress)) {
                    BTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    else -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                val coinCode = params.coinCode ?: return sendJsonErrorResponse(
                    FVJsonRpcErrorType.TagConnectionFail,
                    resultHandler,
                    "coinCode null"
                )

                val estimateFee = rx.estimateNetworkFee()
                val finalTx = FVDogeRawTransaction(params.feeRate.toLong())
                val ecpk = FVECPublicKey()
                ecpk.addressIndex = params.fromPublicKey.addressIndex
                ecpk.uncompressedKey = params.fromPublicKey.uncompressedKey
                ecpk.compressedKey = params.fromPublicKey.compressedKey
                ecpk.change = params.fromPublicKey.change
                ecpk.type = params.fromPublicKey.type.blockchainTypeFromRpc()
                val settlement = rx.settle(ecpk)
                val hasher = FVDogeHasher(settlement, arrayListOf(ecpk), coinCode.hexStringToByteArray()[1])

                fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
                    override fun walletDidSign(vrs: FVPrepareVRS, derFormat: ByteArray, signIndex: Int, errorMessage: String?) {
                        if (errorMessage != null) {
                            sendJsonErrorResponse(
                                FVJsonRpcErrorType.InternalError,
                                resultHandler,
                                "sign verify fail"
                            )
                            return
                        }

                        val preSignTxIn = settlement.txInputs[signIndex]

                        val witness = DogeUtil.createWitness(derFormat, hasher.scriptSignPublicKey(signIndex))

                        //all copy
                        val postSignTxIn =
                            FVDogeRawTransaction.FVBTCInput(
                                preSignTxIn.amount,
                                preSignTxIn.refTxHash,
                                preSignTxIn.refTxIndex,
                                witness,
                                preSignTxIn.refPkScript,
                                preSignTxIn.type
                            )
                        finalTx.addSendInput(postSignTxIn)
                    }

                    override fun walletDidSignAllCompleted(cardSignResults: ArrayList<FVTxSign>, vrs: ArrayList<FVPrepareVRS>, derFormat: ArrayList<ByteArray>) {
                        for (txOut in settlement.txOutputs) {
                            finalTx.addSendTxOutput(txOut)
                        }

                        val result = FVJsonRpcResultMaker.makeSig(finalTx.dogeSerialize(estimateFee).toHexString())
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                            )
                        )
                    }

                    override fun walletDidExecute() {

                    }

                    override fun walletExecuteError(throwable: Throwable) {
                        sendJsonErrorResponse(throwable, resultHandler)
                    }

                    override fun walletExecuting() {
                        eventSink?.success(walletExecuteEvent)
                    }
                }

                fortVaxWallet.sign(cid, object : IFVNFCConnectionProtocol {
                    override fun connectFail() {
                        eventSink?.success(walletConnectFailEvent)

                        sendJsonErrorResponse(
                            FVJsonRpcErrorType.TagConnectionFail,
                            resultHandler
                        )
                    }

                    override fun connectSuccess() {
                    }

                }, hasher)
            }

            else -> {
                sendJsonErrorResponse(
                    FVJsonRpcErrorType.InvalidParams,
                    resultHandler,
                    "type invalid"
                )
            }
        }
    }

}