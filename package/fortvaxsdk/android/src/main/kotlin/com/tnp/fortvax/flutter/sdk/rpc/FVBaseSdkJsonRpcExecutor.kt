package com.tnp.fortvax.flutter.sdk.rpc

import android.nfc.Tag
import com.tnp.fortvax.core.nfc.command.types.EFVCommandError
import com.tnp.fortvax.core.nfc.exception.FVNFCException
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import io.flutter.plugin.common.EventChannel

abstract class FVBaseSdkJsonRpcExecutor(
    val jsonRpcId: String,
    val tag: Tag?,
    val cardId: String?,
    val caPublicKey: String,
    var pinCode: String?,
    var fortVaxWallet: FortVaxWallet
) {

    companion object {
        val walletExecuteEvent = "walletExecuteEvent"
        val walletConnectFailEvent = "walletConnectFailEvent"
        val walletPasswordCreatedEvent = "walletPasswordCreatedEvent"
    }

    var eventSink: EventChannel.EventSink? = null

    abstract fun execute(resultHandler: (Result<String>) -> Unit)

    private fun transformError(throwable: Throwable): FVJsonRpcErrorType {
        return when (throwable) {
            is FVNFCException -> {

                when (throwable.type) {
                    EFVCommandError.UNIDENTIFIED -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.CARD_PID_INCORRECT -> FVJsonRpcErrorType.CardIncorrect
                    EFVCommandError.CARD_LOCKED -> FVJsonRpcErrorType.CardLocked
                    EFVCommandError.CARD_SYSTEM_ERROR -> FVJsonRpcErrorType.CardCertStateError
                    EFVCommandError.DATA_VERIFY_FAILED -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.NFC_DISCONNECTED -> FVJsonRpcErrorType.TagLoss
                    EFVCommandError.NFC_ERROR -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.NFC_IS_SENDING -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.NFC_INIT_FAIL -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.DATA_EMPTY -> FVJsonRpcErrorType.HardwareError
                    EFVCommandError.CLA -> FVJsonRpcErrorType.Cla
                    EFVCommandError.INS -> FVJsonRpcErrorType.Ins
                    EFVCommandError.P1_P2 -> FVJsonRpcErrorType.P1P2
                    EFVCommandError.LC_LE -> FVJsonRpcErrorType.Lcle
                    EFVCommandError.UNIDENTIFIED_APPLET -> FVJsonRpcErrorType.UnidentifiedApplet
                    EFVCommandError.CARD_CERT_FAIL -> FVJsonRpcErrorType.CardCertFail
                    EFVCommandError.CARD_CERT_STATE_ERROR -> FVJsonRpcErrorType.CardCertStateError
                    EFVCommandError.PASSWORD_BUILD_FAIL -> FVJsonRpcErrorType.PasswordBuild
                    EFVCommandError.PASSWORD_FORMAT_FAIL -> FVJsonRpcErrorType.PasswordFormat
                    EFVCommandError.PASSWORD_AUTH_FAIL -> FVJsonRpcErrorType.PasswordAuth
                    EFVCommandError.PASSWORD_STATE_ERROR -> FVJsonRpcErrorType.PasswordStateError
                    EFVCommandError.BUILD_SK_AUTH_FAIL -> FVJsonRpcErrorType.BuildSkAuth
                    EFVCommandError.BUILD_SK_EPIN_CERT_RAND_FAIL -> FVJsonRpcErrorType.BuildSkEpinCertRandom
                    EFVCommandError.BUILD_SK_NO_INIT_PASSWORD -> FVJsonRpcErrorType.BuildSkNoInitPassword
                    EFVCommandError.BUILD_SK_PIN_LIMITED_ERROR -> FVJsonRpcErrorType.BuildSkPinLimited
                    EFVCommandError.BUILD_SK_STATE_ERROR -> FVJsonRpcErrorType.BuildSkStateError
                    EFVCommandError.CREATE_WALLET_BUILD_FAIL -> FVJsonRpcErrorType.CreateWalletBuild
                    EFVCommandError.CREATE_WALLET_STATE_ERROR -> FVJsonRpcErrorType.CreateWalletStateError
                    EFVCommandError.CREATE_WALLET_MAC_ERROR -> FVJsonRpcErrorType.CreateWalletMac
                    EFVCommandError.RECOVERY_WALLET_BUILD_FAIL -> FVJsonRpcErrorType.RecoveryWalletBuild
                    EFVCommandError.RECOVERY_WALLET_STATE_ERROR -> FVJsonRpcErrorType.RecoveryWalletStateError
                    EFVCommandError.RECOVERY_WALLET_MAC_ERROR -> FVJsonRpcErrorType.RecoveryWalletMac
                    EFVCommandError.CHANGE_PASSWORD_AUTH_FAIL -> FVJsonRpcErrorType.ChangePasswordAuth
                    EFVCommandError.CHANGE_PASSWORD_STATE_ERROR -> FVJsonRpcErrorType.ChangePasswordStateError
                    EFVCommandError.CHANGE_PASSWORD_MODIFIED_FAIL -> FVJsonRpcErrorType.ChangePasswordModified
                    EFVCommandError.CHANGE_PASSWORD_NOT_INIT -> FVJsonRpcErrorType.ChangePasswordNotInit
                    EFVCommandError.TX_SIGN_AUTH_FAIL -> FVJsonRpcErrorType.TxSignAuth
                    EFVCommandError.TX_SIGN_STATE_ERROR -> FVJsonRpcErrorType.TxSignNoWallet
                    EFVCommandError.TX_SIGN_NO_WALLET -> FVJsonRpcErrorType.TxSignNoWallet
                    EFVCommandError.RETRIEVE_PBK_AUTH_FAIL -> FVJsonRpcErrorType.RetrievePublicAuth
                    EFVCommandError.RETRIEVE_PBK_INDEX_FAIL -> FVJsonRpcErrorType.RetrievePublicIndex
                    EFVCommandError.RETRIEVE_PBK_WALLET_FAIL -> FVJsonRpcErrorType.RetrievePublicWallet
                    EFVCommandError.RETRIEVE_PBK_STATE_ERROR -> FVJsonRpcErrorType.RetrievePublicState
                    EFVCommandError.NEW_BLOCKCHAIN_ALG_ERROR -> FVJsonRpcErrorType.NewBlockchainAlgError
                    EFVCommandError.NEW_BLOCKCHAIN_AUTH_ERROR -> FVJsonRpcErrorType.NewBlockchainAuthError
                    EFVCommandError.NEW_BLOCKCHAIN_CMD_NO_SUPPORT -> FVJsonRpcErrorType.NewBlockchainCommandNotSupport
                    EFVCommandError.NEW_BLOCKCHAIN_CREATE_FAIL -> FVJsonRpcErrorType.NewBlockchainCreateFail
                    EFVCommandError.NEW_BLOCKCHAIN_NO_WALLET -> FVJsonRpcErrorType.NewBlockchainNoWallet
                    EFVCommandError.NEW_BLOCKCHAIN_WALLET_LIMITED -> FVJsonRpcErrorType.NewBlockchainWalletLimited
                    EFVCommandError.NEW_BLOCKCHAIN_STATE_ERROR -> FVJsonRpcErrorType.NewBlockchainStateError
                    EFVCommandError.WALLET_INFO_AUTH_ERROR -> FVJsonRpcErrorType.WalletInfoAuthError
                    EFVCommandError.WALLET_INFO_STATE_ERROR -> FVJsonRpcErrorType.WalletInfoStateError
                    else -> FVJsonRpcErrorType.Undefined
                }
            }

            else -> {
                return FVJsonRpcErrorType.Undefined
            }
        }
    }

    fun sendJsonErrorResponse(throwable: Throwable, resultHandler: (Result<String>) -> Unit) {
        val jsonError = transformError(throwable)
        val e = FVBaseSdkJsonRpcExecutorException(jsonError)

        resultHandler(Result.failure(e))
    }

    fun sendJsonErrorResponse(
        errorType: FVJsonRpcErrorType,
        resultHandler: (Result<String>) -> Unit,
        detail: String? = null
    ) {
        val e = FVBaseSdkJsonRpcExecutorException(errorType, detail)
        resultHandler(Result.failure(e))
    }

    class FVBaseSdkJsonRpcExecutorException(
        val error: FVJsonRpcErrorType,
        val detail: String? = null
    ) : Exception()
}