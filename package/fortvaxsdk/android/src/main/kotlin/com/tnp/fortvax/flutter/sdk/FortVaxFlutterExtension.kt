package com.tnp.fortvax.flutter.sdk

import android.app.Activity
import android.nfc.NfcAdapter
import android.os.Build
import androidx.annotation.RequiresApi
import com.tnp.fortvax.core.blockchain.FVBlockchainPlus
import com.tnp.fortvax.core.blockchain.FVCardBlockChainType
import java.lang.RuntimeException

@RequiresApi(Build.VERSION_CODES.M)
fun NfcAdapter.startNFCReader(activity: Activity, adapterCallBack: NfcAdapter.ReaderCallback) {
    enableReaderMode(activity, adapterCallBack, NfcAdapter.FLAG_READER_NFC_A, null)
}

@RequiresApi(Build.VERSION_CODES.M)
fun NfcAdapter.stopNFCReader(activity: Activity) {
    disableReaderMode(activity)
}

fun Int.blockchainTypeFromRpc(): FVCardBlockChainType {
    when (this) {
        1 -> return FVCardBlockChainType.ETH
        2 -> return FVCardBlockChainType.BTC
        3 -> return FVCardBlockChainType.TRON
        4 -> return FVCardBlockChainType.USDT
        5 -> return FVCardBlockChainType.BNB
        6 -> return FVCardBlockChainType.LTC
        7 -> return FVCardBlockChainType.XRP
        8 -> return FVCardBlockChainType.EOS
        9 -> return FVCardBlockChainType.BCH
        10 -> return FVCardBlockChainType.BCD
        11 -> return FVCardBlockChainType.BCHSV
        12 -> return FVCardBlockChainType.DOGE
    }

    throw RuntimeException()
}

fun FVCardBlockChainType.toRpcType(): Int {
    return when (this) {
        FVCardBlockChainType.ETH -> 1
        FVCardBlockChainType.BTC -> 2
        FVCardBlockChainType.DOGE -> 12
        FVCardBlockChainType.TRON -> 3
        FVCardBlockChainType.USDT -> 4
        FVCardBlockChainType.BNB -> 5
        FVCardBlockChainType.LTC -> 6
        FVCardBlockChainType.XRP -> 7
        FVCardBlockChainType.EOS -> 8
        FVCardBlockChainType.BCH -> 9
        FVCardBlockChainType.BCD -> 10
        FVCardBlockChainType.BCHSV -> 11
    }
}

fun FVBlockchainPlus.toRpcType(): Int {
    when (this.blockchain) {
        FVCardBlockChainType.DOGE -> {
            return 12
        }

        else -> {
            throw RuntimeException()
        }
    }
}