package com.tnp.fortvax.flutter.sdk.rpc.params.sign

import com.tnp.fortvax.core.blockchain.btc.FVBtcUtxo
import com.tnp.fortvax.core.blockchain.ltc.FVLtcUtxo
import org.json.JSONObject

class FVUtxoSignData(jsonObject: JSONObject) {

    var type = 2
        private set
    var feeRate = 2
        private set
    var fromPublicKey: FVWalletPublicKey
        private set
    var toAddress = ""
        private set
    var candidateUtxo = ArrayList<CandidateUtxo>()
        private set
    var sendAmount = 0L
        private set
    var coinCode: String? = null
        private set

    init {
        feeRate = jsonObject.getInt("feeRate")
        toAddress = jsonObject.getString("toAddress")
        sendAmount = jsonObject.getLong("sendAmount")
        type = jsonObject.getInt("type")
        fromPublicKey = FVWalletPublicKey(jsonObject.getJSONObject("fromPublicKey"))

        val utxoArray = jsonObject.getJSONArray("candidateUtxo")

        for (i in 0 until utxoArray.length()) {
            val item = utxoArray.getJSONObject(i)
            val candidate = CandidateUtxo(item)
            candidateUtxo.add(candidate)
        }

        if (!jsonObject.isNull("coinCode")) {
            coinCode = jsonObject.getString("coinCode")
        }
    }

    class FVWalletPublicKey(json: JSONObject) {
        var type = 2
            private set
        var compressedKey = ""
            private set
        var uncompressedKey = ""
            private set
        var change = false
            private set
        var addressIndex = 1
            private set

        init {
            type = json.getInt("type")
            compressedKey = json.getString("compressedKey")
            uncompressedKey = json.getString("uncompressedKey")
            change = json.getBoolean("change")
            addressIndex = json.getInt("addressIndex")
        }
    }

    class CandidateUtxo(json: JSONObject) {
        var tx_hash = ""
            private set
        var tx_input_n = -1
            private set
        var tx_output_n = -1
            private set
        var value = 0L
            private set
        var script = ""
            private set
        var address = ""
            private set
        var confirmations = 0L
            private set

        init {
            tx_hash = json.getString("tx_hash")
            tx_input_n = json.getInt("tx_input_n")
            tx_output_n = json.getInt("tx_output_n")
            value = json.getLong("value")
            script = json.getString("script")
            address = json.getString("address")
            confirmations = json.getLong("confirmations")
        }

        fun transformBtcUtxo(): FVBtcUtxo {
            val btcUtxo = FVBtcUtxo()
            btcUtxo.address = address
            btcUtxo.script = script
            btcUtxo.tx_hash = tx_hash
            btcUtxo.confirmations = confirmations
            btcUtxo.tx_input_n = tx_input_n
            btcUtxo.tx_output_n = tx_output_n
            btcUtxo.value = value
            return btcUtxo
        }

        fun transformLtcUtxo(): FVLtcUtxo {
            val ltcUtxo = FVLtcUtxo()
            ltcUtxo.address = address
            ltcUtxo.script = script
            ltcUtxo.tx_hash = tx_hash
            ltcUtxo.confirmations = confirmations
            ltcUtxo.tx_input_n = tx_input_n
            ltcUtxo.tx_output_n = tx_output_n
            ltcUtxo.value = value
            return ltcUtxo
        }
    }
}