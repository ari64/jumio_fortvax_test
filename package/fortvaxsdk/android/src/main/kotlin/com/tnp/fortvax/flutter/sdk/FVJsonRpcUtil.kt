package com.tnp.fortvax.flutter.sdk

import org.json.JSONObject

object FVJsonRpcUtil {
    fun errorJsonRpc(
        id: String,
        errorType: FVJsonRpcErrorType,
        extraMessage: String = ""
    ): String {
        return FVJsonRpcError(
            id,
            "2.0",
            errorType.code.toString(),
            "${errorType.message} (${extraMessage})"
        ).jsonString()
    }

    fun successJsonRpc(id: String, result: Map<String, Any>): String {
        return FVJsonRpcResult(id, "2.0", result).jsonString()
    }

    //單一字串，單純告知這個 rpc 有成功就好
    fun successJsonRpc(id: String, result: String): String {
        return FVJsonRpcResult(id, "2.0", mapOf("message" to result)).jsonString()
    }
}

data class FVJsonRpcError(
    val id: String, val jsonrpc: String, val errorCode: String, val errorMessage: String
) {
    fun jsonString(): String {
        val jsonObject = JSONObject()
        jsonObject.put("id", id)
        jsonObject.put("jsonrpc", jsonrpc)
        val jsonErrorObject = JSONObject()
        jsonErrorObject.put("code", errorCode)
        jsonErrorObject.put("message", errorMessage)
        jsonObject.put("error", jsonErrorObject)
        return jsonObject.toString()
    }
}

data class FVJsonRpcResult(
    val id: String, val jsonrpc: String, val result: Map<String, Any>
) {
    fun jsonString(): String {
        val jsonObject = JSONObject()
        jsonObject.put("id", id)
        jsonObject.put("jsonrpc", jsonrpc)
        jsonObject.put("result", JSONObject(result))
        return jsonObject.toString()
    }
}

data class FVJsonRpc(
    val id: String, val method: String, val jsonrpc: String, val params: String?
) {

    companion object {
        fun parser(jsonRpc: String): FVJsonRpc? {
            try {
                //強制完整檢查
                val jsonObject = JSONObject(jsonRpc)
                val id = jsonObject.optString("id") ?: return null
                val method = jsonObject.optString("method") ?: return null
                val jsonrpc = jsonObject.optString("jsonrpc") ?: return null

                //只有這個是可選
                val params = jsonObject.optJSONObject("params")?.toString()

                return FVJsonRpc(id, method, jsonrpc, params)
            } catch (e: Exception) {
                return null
            }
        }
    }
}

enum class FVJsonRpcErrorType(val code: Int, val message: String) {
    Undefined(-1, "Undefined"),
    TagNotSupported(-2, "TagNotSupported"),
    NfcNeedsEnable(-3, "NfcNeedsEnable"),
    TagConnectionFail(-4, "TagConnectionFail"),
    HardwareError(-5, "HardwareError"),
    InvalidRequest(-32600, "InvalidRequest"),
    MethodNotFound(-32601, "MethodNotFound"),
    InvalidParams(-32602, "InvalidParams"),
    InternalError(-32603, "InternalError"),
    ParserError(-32700, "ParserError"),
    UnidentifiedApplet(-12001, "unidentifiedApplet"),
    Cla(-12002, "cla"),
    Ins(-12003, "ins"),
    P1P2(-12004, "p1p2"),
    Lcle(-12005, "lcle"),
    CardCertFail(-12100, "cardCertFail"),
    CardCertStateError(-12101, "cardCertStateError"),
    PasswordFormat(-12150, "passwordFormat"),
    PasswordBuild(-12151, "passwordBuild"),
    PasswordAuth(-12152, "passwordAuth"),
    PasswordStateError(-12153, "passwordStateError"),
    BuildSkAuth(-12200, "buildSkAuth"),
    BuildSkEpinCertRandom(-12201, "buildSkEpinCertRandom"),
    BuildSkNoInitPassword(-12202, "buildSkNoInitPassword"),
    BuildSkPinLimited(-12203, "buildSkPinLimited"),
    BuildSkStateError(-12204, "buildSkStateError"),
    MenmonicBuild(-12250, "menmonicBuild"),
    MenmonicStateError(-12251, "menmonicStateError"),
    MenmonicMac(-12252, "menmonicMac"),
    CreateWalletBuild(-12300, "createWalletBuild"),
    CreateWalletStateError(-12301, "createWalletStateError"),
    CreateWalletMac(-12302, "createWalletMac"),
    RecoveryWalletBuild(-12350, "recoveryWalletBuild"),
    RecoveryWalletStateError(-12351, "recoveryWalletStateError"),
    RecoveryWalletMac(-12352, "recoveryWalletMac"),
    RetrievePublicState(-12400, "retrievePublicState"),
    RetrievePublicAuth(-12401, "retrievePublicAuth"),
    RetrievePublicIndex(-12402, "retrievePublicIndex"),
    RetrievePublicWallet(-12403, "retrievePublicWallet"),
    TxSignAuth(-12450, "txSignAuth"),
    TxSignState(-12451, "txSignState"),
    TxSignNoWallet(-12452, "txSignNoWallet"),
    ChangePasswordModified(-12500, "changePasswordModified"),
    ChangePasswordStateError(-12501, "changePasswordStateError"),
    ChangePasswordAuth(-12502, "changePasswordAuth"),
    ChangePasswordNotInit(-12503, "changePasswordNotInit"),
    NewBlockchainAlgError(-12550, "newBlockchainAlgError"),
    NewBlockchainAuthError(-12551, "newBlockchainAuthError"),
    NewBlockchainCommandNotSupport(-12552, "newBlockchainCommandNotSupport"),
    NewBlockchainCreateFail(-12553, "newBlockchainCreateFail"),
    NewBlockchainNoWallet(-12554, "newBlockchainNoWallet"),
    NewBlockchainWalletLimited(-12555, "newBlockchainWalletLimited"),
    NewBlockchainStateError(-12556, "newBlockchainStateError"),
    WalletInfoAuthError(-12600, "walletInfoAuthError"),
    WalletInfoStateError(-12601, "walletInfoStateError"),
    CardIncorrect(-13000, "cardIncorrect"),
    CardLocked(-13001, "cardLocked"),
    TagLoss(-13002, "tagLoss"),
    CaFail(-13003, "caFail")
}

enum class FVJsonRpcMethod(val methodKey: String) {
    ReadCardStatus("read_card_status"),
    CreateWallet("create_wallet"),
    RestoreWallet("restore_wallet"),
    ReadAccountPublicKey("read_account_publicKey"),
    ReadUtxoPublicKey("read_utxo_publicKey"),
    ResetWallet("reset_wallet"),
    NewBlockchain("new_blockchain"),
    Sign("sign"),
    StopScan("stop_scan"),
    SettleUtxoTransaction("settle_utxo_transaction"),
}