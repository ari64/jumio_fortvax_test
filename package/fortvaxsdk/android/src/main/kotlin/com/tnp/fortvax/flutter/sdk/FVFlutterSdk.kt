package com.tnp.fortvax.flutter.sdk

import android.app.Activity
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.Build
import androidx.annotation.RequiresApi
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.flutter.sdk.rpc.FVBaseSdkJsonRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVCreateWalletRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVNewBlockchainRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVReadAccountModelKeyRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVReadCardStatusRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVReadUtxoModelKeyRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVResetWalletRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVRestoreWalletRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVSettleUtxoTransactionRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.FVSignRpcExecutor
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamCreateWallet
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamNewBlockchain
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamReadAccountModelKey
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamReadCardStatus
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamReadUtxoModelKey
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamResetWallet
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamRestoreWallet
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamSettleUtxo
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel.Result
import org.json.JSONException

/**
 * 中間掛一層 flutter 專用，處理各種 json rpc
 */
@RequiresApi(Build.VERSION_CODES.M)
class FVFlutterSdk {

    private var tag: Tag? = null
    private var fortVaxWallet = FortVaxWallet()

    //連線不關閉，持續感應
    fun handleJsonRpc(jsonRpc: FVJsonRpc, result: Result, eventSink: EventChannel.EventSink? = null, nfcAdapter: NfcAdapter? = null, activity: Activity? = null) {

        var executor: FVBaseSdkJsonRpcExecutor? = null
        if (tag?.techList?.contains(IsoDep::class.java.name) != true && jsonRpc.method != FVJsonRpcMethod.SettleUtxoTransaction.methodKey) {
            result.error(
                FVJsonRpcErrorType.InvalidParams.code.toString(),
                FVJsonRpcErrorType.InvalidParams.message,
                FVJsonRpcUtil.errorJsonRpc(
                    jsonRpc.id,
                    FVJsonRpcErrorType.InvalidParams,
                    "Tag"
                )
            )
            return
        }
        try {
            when (jsonRpc.method) {
                FVJsonRpcMethod.ReadCardStatus.methodKey -> {
                    val p = FVJsonParamReadCardStatus(jsonRpc.params ?: "")
                    executor = FVReadCardStatusRpcExecutor(jsonRpc.id, tag, p.caPublicKey, fortVaxWallet)
                }

                FVJsonRpcMethod.CreateWallet.methodKey -> {
                    val p = FVJsonParamCreateWallet(jsonRpc.params ?: "")
                    executor = FVCreateWalletRpcExecutor(
                        jsonRpc.id,
                        tag,
                        p.cardId,
                        p.pinCode,
                        p.caPublicKey,
                        p.wordCount,
                        p.createPassword,
                        fortVaxWallet
                    )
                }

                FVJsonRpcMethod.RestoreWallet.methodKey -> {
                    val p = FVJsonParamRestoreWallet(jsonRpc.params ?: "")
                    executor = FVRestoreWalletRpcExecutor(
                        jsonRpc.id,
                        tag,
                        p.cardId,
                        p.pinCode,
                        p.caPublicKey,
                        p.words,
                        p.createPassword,
                        fortVaxWallet
                    )
                }

                FVJsonRpcMethod.ReadAccountPublicKey.methodKey -> {
                    val p = FVJsonParamReadAccountModelKey(jsonRpc.params ?: "")
                    executor = FVReadAccountModelKeyRpcExecutor(
                        jsonRpc.id,
                        tag,
                        p.cardId,
                        p.caPublicKey,
                        p.pinCode,
                        p.supportedCryptoMainNet,
                        fortVaxWallet
                    )
                }

                FVJsonRpcMethod.ReadUtxoPublicKey.methodKey -> {
                    val p = FVJsonParamReadUtxoModelKey(jsonRpc.params ?: "")
                    executor =
                        FVReadUtxoModelKeyRpcExecutor(jsonRpc.id, tag, p.cardId, p.caPublicKey, p.pinCode, p.type, fortVaxWallet)
                }

                FVJsonRpcMethod.ResetWallet.methodKey -> {
                    val p = FVJsonParamResetWallet(jsonRpc.params ?: "")
                    executor = FVResetWalletRpcExecutor(jsonRpc.id, tag, p.caPublicKey, fortVaxWallet)
                }

                FVJsonRpcMethod.NewBlockchain.methodKey -> {
                    val p = FVJsonParamNewBlockchain(jsonRpc.params ?: "")
                    executor = FVNewBlockchainRpcExecutor(jsonRpc.id, tag, p.cardId, p.pinCode, p.caPublicKey, p.types, fortVaxWallet)
                }

                FVJsonRpcMethod.Sign.methodKey -> {
                    executor = FVSignRpcExecutor(jsonRpc.id, tag, jsonRpc.params ?: "", fortVaxWallet)
                }

                FVJsonRpcMethod.SettleUtxoTransaction.methodKey -> {
                    executor = FVSettleUtxoTransactionRpcExecutor(jsonRpc.id, FVJsonParamSettleUtxo(jsonRpc.params ?: ""), fortVaxWallet)
                }
            }
        } catch (e: JSONException) {
            result.error(
                FVJsonRpcErrorType.InvalidParams.code.toString(),
                FVJsonRpcErrorType.InvalidParams.message,
                FVJsonRpcUtil.errorJsonRpc(
                    jsonRpc.id,
                    FVJsonRpcErrorType.InvalidParams,
                    "Parser error"
                )
            )
            return
        } catch (e: Exception) {
            result.error(
                FVJsonRpcErrorType.InternalError.code.toString(),
                FVJsonRpcErrorType.InternalError.message,
                FVJsonRpcUtil.errorJsonRpc(
                    jsonRpc.id,
                    FVJsonRpcErrorType.InternalError,
                    e.javaClass.name
                )
            )
            return
        }

        val run = executor ?: return result.error(
            FVJsonRpcErrorType.InternalError.code.toString(),
            FVJsonRpcErrorType.InternalError.message,
            FVJsonRpcUtil.errorJsonRpc(
                jsonRpc.id,
                FVJsonRpcErrorType.InternalError
            )
        )

        run.eventSink = eventSink

        run.execute {
            try {
                if (it.isFailure) {
                    when (val t = it.exceptionOrNull()) {
                        is FVBaseSdkJsonRpcExecutor.FVBaseSdkJsonRpcExecutorException -> {
                            result.error(
                                t.error.code.toString(),
                                t.error.message,
                                FVJsonRpcUtil.errorJsonRpc(
                                    jsonRpc.id, t.error, t.detail ?: ""
                                )
                            )
                        }

                        else -> {
                            result.error(
                                FVJsonRpcErrorType.InternalError.code.toString(),
                                FVJsonRpcErrorType.InternalError.message,
                                FVJsonRpcUtil.errorJsonRpc(
                                    jsonRpc.id,
                                    FVJsonRpcErrorType.InternalError
                                )
                            )
                        }
                    }
                } else {
                    result.success(it.getOrThrow())
                }
            } catch (e: Exception) {
                if (activity != null) {
                    nfcAdapter?.stopNFCReader(activity)
                }
            }
        }
    }

    //第一次開啟感應時
    fun starScanAndHandleJsonRpc(
        jsonRpc: FVJsonRpc,
        nfcAdapter: NfcAdapter,
        activity: Activity,
        result: Result,
        eventSink: EventChannel.EventSink? = null
    ) {
        nfcAdapter.startNFCReader(activity) {
            tag = it
            handleJsonRpc(jsonRpc, result, eventSink, nfcAdapter, activity)
        }
    }
}