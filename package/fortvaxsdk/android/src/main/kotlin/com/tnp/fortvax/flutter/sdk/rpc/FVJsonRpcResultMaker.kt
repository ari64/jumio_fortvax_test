package com.tnp.fortvax.flutter.sdk.rpc

import com.tnp.fortvax.core.blockchain.FVBlockchainPlus
import com.tnp.fortvax.core.blockchain.FVECPublicKey
import com.tnp.fortvax.core.nfc.command.protocol.FVCert
import com.tnp.fortvax.core.nfc.toHexString
import com.tnp.fortvax.flutter.sdk.toRpcType

object FVJsonRpcResultMaker {

    fun makeSig(txHexData: String): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        map["sign"] = txHexData
        return map
    }

    fun makeSettleUtxoTransaction(estimateFee: Long): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        map["estimateFee"] = estimateFee
        return map
    }

    fun makeNewBlockchain(plus: ArrayList<FVBlockchainPlus>): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        val data = ArrayList<Map<String, Any>>()
        for (p in plus) {
            val itemMap = mutableMapOf<String, Any>()
            itemMap["type"] = p.blockchain.toRpcType()
            itemMap["coinCode"] = "0x${p.coinCode.uppercase()}"
            data.add(itemMap)
        }

        map["blockchainPlus"] = data

        return map
    }

    fun makeReadAccountModelKey(publicKeys: ArrayList<FVECPublicKey>): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        val accKeys = ArrayList<Map<String, Any>>()
        for (key in publicKeys) {
            try {
                val accountKeyMap = mutableMapOf<String, Any>()
                accountKeyMap["type"] = key.type.toRpcType()
                accountKeyMap["compressedKey"] = key.compressedKey
                accountKeyMap["uncompressedKey"] = key.uncompressedKey
                accountKeyMap["change"] = key.change
                accountKeyMap["addressIndex"] = key.addressIndex
                accountKeyMap["keyToAddress"] = key.parserAddress()
                accKeys.add(accountKeyMap)
            } catch (_: Exception) {

            }
        }

        map["accountModelKeys"] = accKeys
        return map
    }

    fun makeReadUtxoModelKey(publicKey: FVECPublicKey): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        val keyMap = mutableMapOf<String, Any>()

        keyMap["type"] = publicKey.type.toRpcType()
        keyMap["compressedKey"] = publicKey.compressedKey
        keyMap["uncompressedKey"] = publicKey.uncompressedKey
        keyMap["change"] = publicKey.change
        keyMap["addressIndex"] = publicKey.addressIndex
        keyMap["keyToAddress"] = publicKey.parserAddress()

        map["utxoModelKey"] = keyMap

        return map
    }

    fun makeRestoreWallet(
        accountPublicKeys: ArrayList<FVECPublicKey>,
    ): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        val accKeys = ArrayList<Map<String, Any>>()
        for (key in accountPublicKeys) {
            try {
                val accountKeyMap = mutableMapOf<String, Any>()
                accountKeyMap["type"] = key.type.toRpcType()
                accountKeyMap["compressedKey"] = key.compressedKey
                accountKeyMap["uncompressedKey"] = key.uncompressedKey
                accountKeyMap["change"] = key.change
                accountKeyMap["addressIndex"] = key.addressIndex
                accountKeyMap["keyToAddress"] = key.parserAddress()
                accKeys.add(accountKeyMap)
            } catch (_: Exception) {

            }
        }

        map["accountModelKeys"] = accKeys
        return map
    }

    fun makeCreateWallet(
        seedWords: ArrayList<String>,
        accountPublicKeys: ArrayList<FVECPublicKey>,
        btcUtxoPublicKeys: ArrayList<FVECPublicKey>
    ): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        map["seedWords"] = seedWords.joinToString(" ")

        //sdk parserAddress 還沒有完全 全部支援 11 條鏈地址，所以用了會噴錯
        //錯誤就略過

        val accKeys = ArrayList<Map<String, Any>>()
        for (key in accountPublicKeys) {
            try {
                val accountKeyMap = mutableMapOf<String, Any>()
                accountKeyMap["type"] = key.type.toRpcType()
                accountKeyMap["compressedKey"] = key.compressedKey
                accountKeyMap["uncompressedKey"] = key.uncompressedKey
                accountKeyMap["change"] = key.change
                accountKeyMap["addressIndex"] = key.addressIndex
                accountKeyMap["keyToAddress"] = key.parserAddress()
                accKeys.add(accountKeyMap)
            } catch (_: Exception) {

            }
        }

        map["accountModelKeys"] = accKeys

//        val utxoKeys = ArrayList<Map<String, Any>>()
//        for (key in btcUtxoPublicKeys) {
//            try {
//                val accountKeyMap = mutableMapOf<String, Any>()
//                accountKeyMap["type"] = key.type.toRpcType()
//                accountKeyMap["compressedKey"] = key.compressedKey
//                accountKeyMap["uncompressedKey"] = key.uncompressedKey
//                accountKeyMap["keyToAddress"] = key.parserAddress()
//                accountKeyMap["change"] = key.change
//                accountKeyMap["addressIndex"] = key.addressIndex
//                utxoKeys.add(accountKeyMap)
//            } catch (_: Exception) {
//
//            }
//        }
//
//        map["utxoModelKeys"] = utxoKeys

        return map
    }

    fun makeCardStatus(cert: FVCert): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        map["caIndex"] = cert.caIndex
        map["capVer"] = cert.capVer
        map["uid"] = cert.uid.toHexString().lowercase()

        if (cert.isEpinCreated() && !cert.isWalletHasBeenBuild()) {
            map["cardStatus"] = 1
        } else if (cert.isWalletHasBeenBuild() && cert.isEpinCreated()) {
            map["cardStatus"] = 2
        } else {
            map["cardStatus"] = 0
        }

        return map
    }
}