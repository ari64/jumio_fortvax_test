package com.tnp.fortvax.flutter.sdk.rpc.params.sign

import org.json.JSONObject

class FVEthereum1559SignData(jsonObject: JSONObject) {

    var nonce: String = ""
        private set
    var maxFeePerGas: String = ""
        private set
    var maxPriorityFeePerGas: String = ""
        private set
    var gasLimit: String = ""
        private set
    var amount: String = ""
        private set
    var toAddress: String = ""
        private set
    var chainId: Long = 0
        private set
    var publicKey: String = ""
        private set

    init {
        nonce = jsonObject.getString("nonce")
        gasLimit = jsonObject.getString("gasLimit")
        amount = jsonObject.getString("amount")
        toAddress = jsonObject.getString("toAddress")
        chainId = jsonObject.getLong("chainId")
        publicKey = jsonObject.getString("publicKey").substring(2)
        maxFeePerGas = jsonObject.getString("maxFeePerGas")
        maxPriorityFeePerGas = jsonObject.getString("maxPriorityFeePerGas")
    }
}