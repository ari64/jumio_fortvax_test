package com.tnp.fortvax.flutter.sdk.rpc

import com.tnp.fortvax.core.blockchain.btc.BTCAddressType
import com.tnp.fortvax.core.blockchain.btc.BTCUtil
import com.tnp.fortvax.core.blockchain.btc.FVBtcRawTransaction
import com.tnp.fortvax.core.blockchain.doge.DogeUtil
import com.tnp.fortvax.core.blockchain.doge.FVDogeRawTransaction
import com.tnp.fortvax.core.blockchain.ltc.FVLtcRawTransaction
import com.tnp.fortvax.core.blockchain.ltc.LTCAddressType
import com.tnp.fortvax.core.blockchain.ltc.LTCUtil
import com.tnp.fortvax.core.exception.FVBTCUtxoNotCoverException
import com.tnp.fortvax.core.exception.FVLTCUtxoNotCoverException
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import com.tnp.fortvax.flutter.sdk.FVJsonRpcUtil
import com.tnp.fortvax.flutter.sdk.rpc.params.FVJsonParamSettleUtxo

class FVSettleUtxoTransactionRpcExecutor(id: String, val params: FVJsonParamSettleUtxo, fortVaxWallet: FortVaxWallet) :
    FVBaseSdkJsonRpcExecutor(id, null, null, "", null, fortVaxWallet) {

    override fun execute(resultHandler: (Result<String>) -> Unit) {

        //安全檢查
        when (params.type) {
            2 -> {
                //BTC
                val rx = FVBtcRawTransaction()
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformBtcUtxo())
                }

                when (BTCUtil.checkAddressType(params.toAddress)) {
                    BTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.P2SH_P2WPKH -> {
                        rx.addNestedSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.P2WPKH -> {
                        rx.addNativeSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    BTCAddressType.UNDEFINED -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                try {
                    val estimateFee = rx.estimateNetworkFee(params.feeRate.toLong())
                    resultHandler(
                        Result.success(
                            FVJsonRpcUtil.successJsonRpc(
                                jsonRpcId,
                                FVJsonRpcResultMaker.makeSettleUtxoTransaction(estimateFee)
                            )
                        )
                    )
                } catch (e: Exception) {
                    if (e is FVBTCUtxoNotCoverException) {
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(
                                    jsonRpcId,
                                    FVJsonRpcResultMaker.makeSettleUtxoTransaction(e.estimateFee)
                                )
                            )
                        )
                    } else {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "estimateNetworkFee error")
                    }
                }
            }

            6 -> {
                //LTC
                val rx = FVLtcRawTransaction()
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformLtcUtxo())
                }

                when (LTCUtil.checkAddressType(params.toAddress)) {
                    LTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.P2SH_P2WPKH -> {
                        rx.addNestedSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.P2WPKH -> {
                        rx.addNativeSegWitOutput(params.toAddress, params.sendAmount)
                    }

                    LTCAddressType.UNDEFINED -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                try {
                    val estimateFee = rx.estimateNetworkFee(params.feeRate.toLong())
                    resultHandler(
                        Result.success(
                            FVJsonRpcUtil.successJsonRpc(
                                jsonRpcId,
                                FVJsonRpcResultMaker.makeSettleUtxoTransaction(estimateFee)
                            )
                        )
                    )
                } catch (e: Exception) {
                    if (e is FVLTCUtxoNotCoverException) {
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(
                                    jsonRpcId,
                                    FVJsonRpcResultMaker.makeSettleUtxoTransaction(e.estimateFee)
                                )
                            )
                        )
                    } else {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "estimateNetworkFee error")
                    }
                }
            }

            12 -> {
                //DOGE
                val rx = FVDogeRawTransaction(params.feeRate.toLong())
                for (candidate in params.candidateUtxo) {
                    rx.addCandidateUtxo(candidate.transformBtcUtxo())
                }

                when (DogeUtil.checkAddressType(params.toAddress)) {
                    BTCAddressType.P2PKH -> {
                        rx.addP2PKHOutput(params.toAddress, params.sendAmount)
                    }

                    else -> {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "to address invalid")
                        return
                    }
                }

                try {
                    val estimateFee = rx.estimateNetworkFee()
                    resultHandler(
                        Result.success(
                            FVJsonRpcUtil.successJsonRpc(
                                jsonRpcId,
                                FVJsonRpcResultMaker.makeSettleUtxoTransaction(estimateFee)
                            )
                        )
                    )
                } catch (e: Exception) {
                    if (e is FVBTCUtxoNotCoverException) {
                        resultHandler(
                            Result.success(
                                FVJsonRpcUtil.successJsonRpc(
                                    jsonRpcId,
                                    FVJsonRpcResultMaker.makeSettleUtxoTransaction(e.estimateFee)
                                )
                            )
                        )
                    } else {
                        sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "estimateNetworkFee error")
                    }
                }
            }

            else -> {
                //其他都不支援
                sendJsonErrorResponse(FVJsonRpcErrorType.InvalidParams, resultHandler, "type invalid")
                return
            }
        }
    }
}