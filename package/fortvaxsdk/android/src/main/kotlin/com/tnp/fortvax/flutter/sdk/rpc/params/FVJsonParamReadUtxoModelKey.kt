package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamReadUtxoModelKey(jsonParams: String) {

    var caPublicKey = ""
        private set
    var type = 0
        private set
    var cardId = ""
        private set
    var pinCode = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
        cardId = json.getString("cardId")
        pinCode = json.getString("pinCode")
        type = json.getInt("type")
    }
}