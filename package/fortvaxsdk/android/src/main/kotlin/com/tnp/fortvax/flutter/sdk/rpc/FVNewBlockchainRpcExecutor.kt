package com.tnp.fortvax.flutter.sdk.rpc

import android.nfc.Tag
import com.tnp.fortvax.core.blockchain.FVBlockchainPlus
import com.tnp.fortvax.core.blockchain.FVCardBlockChainType
import com.tnp.fortvax.core.blockchain.FVECPublicKey
import com.tnp.fortvax.core.nfc.IFVNFCConnectionProtocol
import com.tnp.fortvax.core.nfc.hexStringToByteArray
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.core.nfc.wallet.IFortVaxWalletProtocol
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import com.tnp.fortvax.flutter.sdk.FVJsonRpcUtil
import com.tnp.fortvax.flutter.sdk.blockchainTypeFromRpc

class FVNewBlockchainRpcExecutor(
    id: String,
    tag: Tag?,
    cardId: String,
    pinCode: String,
    caPublicKey: String,
    val types: ArrayList<Int>,
    fortVaxWallet: FortVaxWallet
) :
    FVBaseSdkJsonRpcExecutor(id, tag, cardId, caPublicKey, pinCode, fortVaxWallet) {

    override fun execute(resultHandler: (Result<String>) -> Unit) {
        val tag = this.tag ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.TagConnectionFail,
            resultHandler
        )

        val pin =
            pinCode ?: return sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "pin null"
            )

        if (caPublicKey.isEmpty() || caPublicKey.hexStringToByteArray().isEmpty()) {
            sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "ca key"
            )
            return
        }

        fortVaxWallet.assignNFCTag(tag)
        fortVaxWallet.setPinCode(pin)
        fortVaxWallet.assignCA(caPublicKey.hexStringToByteArray())

        fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {

            override fun walletDidInitializeBlockchainPlus(plus: ArrayList<FVBlockchainPlus>) {
                resultHandler(
                    Result.success(
                        FVJsonRpcUtil.successJsonRpc(
                            jsonRpcId,
                            FVJsonRpcResultMaker.makeNewBlockchain(plus)
                        )
                    )
                )
            }

            override fun walletDidRestoreChain(accountPublicKeys: ArrayList<FVECPublicKey>) {
                val result = FVJsonRpcResultMaker.makeRestoreWallet(accountPublicKeys)
                resultHandler(
                    Result.success(
                        FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                    )
                )
            }

            override fun walletExecuteError(throwable: Throwable) {
                sendJsonErrorResponse(throwable, resultHandler)
            }

            override fun walletExecuting() {
                eventSink?.success(walletExecuteEvent)
            }

            override fun walletDidExecute() {
            }

        }

        val cid = cardId ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.InternalError,
            resultHandler,
            "cid null"
        )

        val sdkTypes = ArrayList<FVCardBlockChainType>()
        sdkTypes.addAll(types.map { it.blockchainTypeFromRpc() })

        fortVaxWallet.addNewBlockchain(cid, sdkTypes, object : IFVNFCConnectionProtocol {
            override fun connectSuccess() {

            }

            override fun connectFail() {
                eventSink?.success(walletConnectFailEvent)

                sendJsonErrorResponse(
                    FVJsonRpcErrorType.TagConnectionFail,
                    resultHandler
                )
            }

        })
    }


}