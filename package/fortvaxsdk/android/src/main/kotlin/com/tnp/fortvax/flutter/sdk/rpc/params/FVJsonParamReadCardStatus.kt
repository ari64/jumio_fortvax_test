package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamReadCardStatus(jsonParams: String) {

    var caPublicKey = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
    }
}