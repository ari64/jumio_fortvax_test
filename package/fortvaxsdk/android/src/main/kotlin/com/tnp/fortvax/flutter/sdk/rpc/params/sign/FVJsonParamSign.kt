package com.tnp.fortvax.flutter.sdk.rpc.params.sign

import org.json.JSONObject

class FVJsonParamSign(jsonParams: String) {

    var caPublicKey = ""
        private set
    var cardId = ""
        private set
    var pinCode = ""
        private set
    var type = 1
        private set
    var transactionType = ""
        private set
    var evmType: Int? = null
        private set
    var data = JSONObject()
        private set

    init {
        val paramsJson = JSONObject(jsonParams)
        caPublicKey = paramsJson.getString("caPublicKey")
        transactionType = paramsJson.getString("transactionType")
        cardId = paramsJson.getString("cardId")
        pinCode = paramsJson.getString("pinCode")
        type = paramsJson.getInt("type")
        evmType = if (paramsJson.has("evmType")) paramsJson.getInt("evmType") else null
        data = paramsJson.getJSONObject("data")
    }
}