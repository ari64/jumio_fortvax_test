package com.tnp.fortvax.flutter.sdk.rpc.params

import org.json.JSONObject

class FVJsonParamCreateWallet(jsonParams: String) {

    var caPublicKey = ""
        private set
    var wordCount = 0
        private set
    var createPassword = false
        private set
    var cardId = ""
        private set
    var pinCode = ""
        private set

    init {
        val json = JSONObject(jsonParams)
        caPublicKey = json.getString("caPublicKey")
        wordCount = json.getInt("wordCount")
        createPassword = json.getBoolean("createPassword")
        cardId = json.getString("cardId")
        pinCode = json.getString("pinCode")
    }
}