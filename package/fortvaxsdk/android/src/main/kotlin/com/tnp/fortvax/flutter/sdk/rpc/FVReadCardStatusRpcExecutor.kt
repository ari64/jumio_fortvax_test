package com.tnp.fortvax.flutter.sdk.rpc

import android.nfc.Tag
import com.tnp.fortvax.core.nfc.IFVNFCConnectionProtocol
import com.tnp.fortvax.core.nfc.command.protocol.FVCert
import com.tnp.fortvax.core.nfc.hexStringToByteArray
import com.tnp.fortvax.core.nfc.wallet.FortVaxWallet
import com.tnp.fortvax.core.nfc.wallet.IFortVaxWalletProtocol
import com.tnp.fortvax.flutter.sdk.FVJsonRpcErrorType
import com.tnp.fortvax.flutter.sdk.FVJsonRpcUtil

class FVReadCardStatusRpcExecutor(id: String, tag: Tag?, caPublicKey: String, fortVaxWallet: FortVaxWallet) :
    FVBaseSdkJsonRpcExecutor(id, tag, null, caPublicKey, null, fortVaxWallet) {

    override fun execute(resultHandler: (Result<String>) -> Unit) {
        val tag = this.tag ?: return sendJsonErrorResponse(
            FVJsonRpcErrorType.TagConnectionFail,
            resultHandler
        )

        if (caPublicKey.isEmpty() || caPublicKey.hexStringToByteArray().isEmpty()) {
            sendJsonErrorResponse(
                FVJsonRpcErrorType.InternalError,
                resultHandler,
                "ca key"
            )
            return
        }

        fortVaxWallet.assignNFCTag(tag)
        fortVaxWallet.assignCA(caPublicKey.hexStringToByteArray())

        fortVaxWallet.walletProtocol = object : IFortVaxWalletProtocol {
            override fun walletDidReadCertStatus(cert: FVCert) {
                val result = FVJsonRpcResultMaker.makeCardStatus(cert)
                resultHandler(
                    Result.success(
                        FVJsonRpcUtil.successJsonRpc(jsonRpcId, result)
                    )
                )
            }

            override fun walletExecuteError(throwable: Throwable) {
                sendJsonErrorResponse(throwable, resultHandler)
            }

            override fun walletExecuting() {
                eventSink?.success(walletExecuteEvent)
            }

            override fun walletDidExecute() {
            }

        }

        fortVaxWallet.readCardStatus(object : IFVNFCConnectionProtocol {
            override fun connectSuccess() {

            }

            override fun connectFail() {
                eventSink?.success(walletConnectFailEvent)

                sendJsonErrorResponse(
                    FVJsonRpcErrorType.TagConnectionFail,
                    resultHandler
                )
            }

        })
    }


}